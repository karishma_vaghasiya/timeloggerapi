<?php

return [
'error_code' => ['code'=>'#ff7675'],
'sun_code' => ['code'=>'#00b894'],
'sat_code' => ['code'=>'#fdcb6e'],
'leave_type'=>[''=>'Select Leave Type','Casual Leave'=>'Casual Leave','Privilege Leave'=>'Privilege Leave','Sick Leave'=>'Sick Leave'],
'category'=>[''=>'Select Category','Technical'=>'Technical','Catering'=>'Catering']
];