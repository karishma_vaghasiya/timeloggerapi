@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Service Master</h1>
    </div>
    <div>
        <a href="<?= URL::route('services.create') ?>"class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Peroson Name</th>
                            <th>Contact</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')

<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>

<script>
var token = "<?= csrf_token() ?>";
var title = "Are you sure to delete selected record(s)?";
var text = "You will not be able to recover this record";
var type = "warning";
var token = "{{ csrf_token() }}";
var delete_path = "{{ route('services.delete') }}";

$(document).ready(function(){

    $(function()
        {
            var master = $('#employee').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "autoWidth": true,
            "aaSorting": [
                [1, "asc"]
            ],
            "sAjaxSource": "{{ URL::route('services.index')}}",
            "fnServerParams": function ( aoData ) {
                aoData.push({ "name": "act", "value": "fetch" });
                server_params = aoData;
            },
            "aoColumns": [
            { mData: 'id',sWidth: "20%",bSortable:true, bVisible:false},
            { mData: 'person_name',sWidth: "30%",bSortable:true, },
            { mData: 'phone_num',sWidth: "30%",bSortable:true,
                mRender:function(v,t,o) {
                    values = v.split(',');
                    return values[0];
                }
             },
            { mData: 'category',sWidth: "20%",bSortable:true, },
            { mData: null,
                sWidth: "10%",
                bSortable : false,
                mRender:function(v,t,o) {

                    var path = "<?= URL::route('services.edit',array('id'=>':id')) ?>";

                    path     = path.replace(':id',o['id']);

                    var extra_html  =   "<div class='btn-group pr5'>"
                                    +       "<a title='Edit' href='"+path+"'><i class='fa fa-edit'></i></a>|"
                                    +   "<a id='delete' href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" class='fa fa-fw fa-trash-o'></a>  "
                                    +   "</div>";
                    return extra_html;


                }

            }
            ],
            
        });

        
        });
    $.ajaxSetup({
        statusCode: {
          401: function() {
            location.reload();
          }
        }
    });

});
</script>
@include('partials.alert')
@stop
