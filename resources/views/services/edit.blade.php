@extends('layout.layout')
@section('content')
<style type="text/css">
    .avtar{
    background-color: #fff;
    padding:30px 0;
    }
    .avtar button.btn-primary {
    float: none;
    width: 80%;
    margin: 30px auto;
    }
    .avtar form {
    float: none;
    width: 90%;
    margin: auto;
    }
    .profile-detail{
    width:100%;
    background-color: #fff;
    padding:30px 0;
    }
    .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
    margin: 10px 0;
    }
    .profile-detail label {
    float: right;
    font-size: 16px;
    margin: 19px 0;
    }
    .profile-detail .form-control {
    float: left;
    width: 98%;
    margin: 10px 0;
    }
    .profile-detail textarea {
    width: 98%;
    }
    .profile-detail button {
    float: left;
    }
    .checkbox {
    float: left;
    width: 98%;
    text-align: left;
    }
    .checkbox label {
    float: left;
    width: auto;
    margin: 0;
    }
    .checkbox input[type="checkbox"] {
    float: left;
    width: auto;
    margin: 4px 0;
    }
    .dl-horizontal.dd-horizontal > dd {
    width: 100%;
    text-align: left;
    }
    .text-left.check-radio{
        margin-top:30px;
    }
    </style>
<div class="page-title">
    <div>
        <h1>Services Master</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>Edit</li>
        </ul>
    </div>
</div>
<div class="clearix"></div>
<div class="col-md-13">
    <div class="card">
        <h3 class="card-title">Edit</h3>
        <div class="card-body">
            <?= Form::model($service_master,['route'=>['services.update','id'=>$service_master->id],'class' => 'form-horizontal','method'=>'put']) ?>
            <!-- <div class="form-group">
                <label class="control-label col-sm-3" for="inputFirm Name">Firm Name</label>
                <div class="col-sm-8">
                <?= Form::text('firm_name' , old('firm_name') ,['class' => 'form-control','placeholder' => 'Firm Name']); ?>
                </div>
            </div> -->
            <div class="form-group">
                <label class="control-label col-sm-3" for="inputperson Name">Person Name</label>
                <div class="col-sm-8">
                    <?= Form::text('person_name' ,old('person_name') ,['class' => 'form-control','placeholder' => 'Person Name']); ?>
                    <?= $errors->first('person_name',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <!-- <div class="form-group">
                <label class="control-label col-sm-3" for="inputphone Num">Email</label>
                <div class="col-sm-8">
                    <?= Form::text('email' ,old('email') ,['class' => 'form-control','id'=>'email','placeholder' => 'Email']); ?>
                    <?= $errors->first('email',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>  -->
            <div class="form-group">
                <label class="control-label col-sm-3" for="inputcategory Name">Category</label>
                <div class="col-sm-8">
                    <?= Form::select('category',config('project.category'),old('category') ,['class' => 'form-control','id'=>'category']); ?>
                    <?= $errors->first('category',"<span class='text-danger'>:message</span>");?>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-3" for="inputphone Num">Phone Number</label>
                <div class="col-sm-8">
                    <select class="select2_2 form-control" id="phone_num" multiple="multiple" name="phone_num[]" placeholder="Phone Number">
                    </select>
                </div>
            </div> 
            <!-- <div class="form-group">
                <label class="control-label col-sm-3" for="inputphone Num">Phone Number(Alernative)</label>
                <div class="col-sm-8">
                    <?= Form::text('alternative_phone_num' ,old('alternative_phone_num') ,['class' => 'form-control','id'=>'alternative_phone_num','placeholder' => ' Alernative Phone Number']); ?>
                    <?= $errors->first('alternative_phone_num',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="inputFull Name">Address</label>
                <div class="col-sm-8">
                    <?= Form::textarea('address', old('address'), ['class' => 'form-control', 'placeholder' => 'Address']); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="inputphone Num">Account Number</label>
                <div class="col-sm-8">
                    <?= Form::text('account_num' ,old('account_num') ,['class' => 'form-control','id'=>'account_num','placeholder' => 'Account Number']); ?>
                    <?= $errors->first('account_num',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>  -->
            <div class="form-group">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit" class="btn btn-primary" value="save_exit" name="save_button">Save & Exit</button>
                    <button type="submit" class="btn btn-primary" value="save" name="save_button">Save</button>
                    <a href="<?=URL::route('services.index')?>">
                    <button type="button" class="btn btn-white btn-default" value="cancel" id="" name="cancel">Cancel</button>
                    </a>
                    
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
<?= Html::script('asset/js/select2.min.js') ?>

    <script type="text/javascript">
  
      var tag = $('#phone_num').select2({
        placeholder : "select tag",
       
      });
      @if(old('phone_num',$service_master['phone_num']))
        var old_tag = {!! json_encode(old('phone_num',$service_master['phone_num'])) !!};
        if(typeof old_tag == 'string'){
            old_tag = old_tag.split(',');
        }
        $.each(old_tag,function(k,v){
            $("#phone_num").append($('<option>', {value: v, text: v}));
        })
        tag.val(old_tag).trigger('change');
      @endif

    </script>   
    <?= Html::script('asset/js/form.demo.min.js') ?>
@include('partials.alert')
@stop
