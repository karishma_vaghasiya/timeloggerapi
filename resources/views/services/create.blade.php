@extends('layout.layout')
@section('content')
<style type="text/css">
    .avtar{
    background-color: #fff;
    padding:30px 0;
    }
    .avtar button.btn-primary {
    float: none;
    width: 80%;
    margin: 30px auto;
    }
    .avtar form {
    float: none;
    width: 90%;
    margin: auto;
    }
    .profile-detail{
    width:100%;
    background-color: #fff;
    padding:30px 0;
    }
    .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
    margin: 10px 0;
    }
    .profile-detail label {
    float: right;
    font-size: 16px;
    margin: 19px 0;
    }
    .profile-detail .form-control {
    float: left;
    width: 98%;
    margin: 10px 0;
    }
    .profile-detail textarea {
    width: 98%;
    }
    .profile-detail button {
    float: left;
    }
    .checkbox {
    float: left;
    width: 98%;
    text-align: left;
    }
    .checkbox label {
    float: left;
    width: auto;
    margin: 0;
    }
    .checkbox input[type="checkbox"] {
    float: left;
    width: auto;
    margin: 4px 0;
    }
    .dl-horizontal.dd-horizontal > dd {
    width: 100%;
    text-align: left;
    }
    .text-left.check-radio{
        margin-top:30px;
    }
    </style>
<div class="page-title">
    <div>
        <h1>Services Master</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>create</li>
        </ul>
    </div>
</div>
<div class="clearix"></div>
<div class="col-md-13">
    <div class="card">
        <h3 class="card-title">Create</h3>
        <div class="card-body">
            <?= Form::open(array('url' => route('services.store') ,'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="inputperson Name">Person Name</label>
                <div class="col-sm-8">
                    <?= Form::text('person_name' ,old('person_name') ,['class' => 'form-control','placeholder' => 'Person Name']); ?>
                    <?= $errors->first('person_name',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="inputcategory Name">Category</label>
                <div class="col-sm-8">
                    <?= Form::select('category[]',$category,old('category') ,['class' => 'form-control select2_2','id'=>'category','multiple']); ?>
                    <?= $errors->first('category',"<span class='text-danger'>:message</span>");?>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-3" for="inputphone Num">Phone Number</label>
                <div class="col-sm-8">
                    <select class="select2_2 form-control" id="phone_num" multiple="multiple" name="phone_num[]" placeholder="Phone Number">
                    </select>
                </div>
            </div> 
            <div class="form-group">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit" class="btn btn-primary submit" id="submit" name="save_button" value="save_new">Save & New</button>
                    <button type="submit" class="btn btn-primary submit" id="submit1" name="save_button" value="save_exit">Save & Exit</button>
                    <a href="<?= URL::route('services.index') ?>" class="btn btn-white btn-default"> Cancel</a>
                    
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
    <?= Html::script('asset/js/select2.min.js') ?>

    <script type="text/javascript">
        //phone num
      var tag = $('#phone_num').select2({
        placeholder : "select tag",
       
      });
      @if(old('phone_num'))
        var old_tag = {!! json_encode(old('phone_num')) !!};
        $.each(old_tag,function(k,v){
            $("#phone_num").append($('<option>', {value: v, text: v}));
        })
        tag.val(old_tag).trigger('change');
      @endif

      //category
      var cattag = $('#category').select2({
        placeholder : "select tag",
       
      });
      $('#category').change(function(){
        cat_values = $(this).val();
        $.ajax({
            url: '<?= URL::route('category.store') ?>',
            type:'post',
            dataType:'json',
            data:{cat_values:cat_values,_token: '<?= csrf_token()?>'},
            success:function(){
                // iziToast.success({title:'Success!',message: 'Working Days Updated Successfully'});
                // window.setTimeout(function(){location.reload()},3000)
            }
        });
      });
      @if(old('category'))
        var old_tag = {!! json_encode(old('category')) !!};
        $.each(old_tag,function(k,v){
            $("#category").append($('<option>', {value: v, text: v}));
        })
        cattag.val(old_tag).trigger('change');
      @endif

    </script>   
    <?= Html::script('asset/js/form.demo.min.js') ?>

@include('partials.alert')
@stop
