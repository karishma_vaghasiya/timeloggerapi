@extends('layout.layout')
@section('content')
<div class="page-title">
        <div>
          <h1>Permission</h1>
            <ul class="breadcrumb side">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li>Create</li>
           </ul>
        </div>
</div>
<div class="clearix"></div>
    @include('partials.alert')
    <div class="row">
    	<div class="col-md-12">
        	<div class="card">
        		<h3 class="card-title">Create</h3>
     			<div class="card-body">
      				<?= Form::open(array('url' => route('userpermission.store') ,'class' => 'form-horizontal' ,'files' => true)) ?>
          				<div class="form-group">
                			<label class="control-label col-sm-2" for="inputFull Name">User</label>
                			<div class="col-sm-8">
            					<label><?= Form::select('user_id',[''=>'select'] + $users,old('user_id'),['class' => 'form-control','id' => 'userchange']) ?></label>
                                <span class="help-inline text-danger">{{ $errors->first('user_id') }}</span>
                            </div>
    					</div>
    	                <div class="form-group">
                            <label class="control-label col-sm-2" for="inputFull Name">Permission</label>
                            <div class="animated-checkbox" style="margin-bottom:10px;">
                                <label style="margin: 11px 19px;"><input type="checkbox" id="selectall"><span class="label-text">Select All</span></label>
                                <span class="help-inline text-danger">{{ $errors->first('permissions') }}</span>
                            </div>
                            <div class="row">
        					<div class="col-sm-offset-2 col-sm-10">
                                    @foreach($secPermissions as $sectionName => $secByPermission)
                                    <div class="col-sm-12 permission-section">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <a class="collapsed panellink-title" data-toggle="collapse" data-parent="#accordion" href="#<?= $sectionName ?>"><?= ucfirst($sectionName) ?></a>
                                                    <div class="animated-checkbox" style="margin-bottom:0px;float: right;">
                                                        <label><input type="checkbox" id="" class="section_selectall" data="<?= $sectionName?>"><span class="label-text">Select All</span></label>
                                                    </div>
                                                </div>
                                                <div class="panel-collapse collapse" id="<?= $sectionName ?>">
                                                    @foreach($secByPermission as $permission)
                                                        <div class="panel-body">
                                                            <div class="animated-checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="permissions[]" id="<?= $permission['id']; ?>" value="<?= $permission['id']; ?>">
                                                                    <span class="label-text"><?= $permission['description']; ?></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
    						</div>
    					</div>
       	  				<div class="form-group">
          					<div class="col-md-8 col-md-offset-2">
            					<button type="submit" class="btn btn-primary">Submit</button>
          					</div>
          				</div>
      				{!! Form::close() !!}
      			</div>
        	</div>
      	</div>
    </div>
</div>

@stop
@section('style')
    <?= Html::style('asset/css/new_dashboard.css') ?>
@stop
@section('script')
<!-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> -->
<script>
    jQuery(document).ready(function() {
        $('form').submit(function(){
            $(this).find('button:submit').html('<span><i class="fa fa-spinner fa-spin"></i>Please Wait....</span>').prop('disabled', true);
        });
        $('.section_selectall').on('change',function(){
            var isChecked = $(this).is(':checked');
            var id = $(this).attr('data');
            if (isChecked) {
                $('#'+id+' input:checkbox').prop('checked',true);
            } else {
                $('#'+id+' input:checkbox').prop('checked',false);
            }
        });
        $('#selectall').on('change',function(){
            var isChecked = $(this).is(':checked');
            console.log(isChecked);

            if (isChecked) {
                $('input:checkbox').prop('checked',true);
            } else {
                $('input:checkbox').prop('checked',false);
            }
        });

        $('#userchange').on('change',function(){

            var userId = $(this).val();

            if (userId == null && userId == '') {
                return;
            }

            $.ajax({
                url: '<?= route('selectpermission.get') ?>',
                type: 'json',
                method: 'post',
                data: {'user_id': userId,'_token': '<?= csrf_token() ?>' },
                beforeSend: function(){
                    $('input:checkbox').removeAttr('checked');
                },
                complete: function(){

                },
                success: function(resp){
                    if( ! resp.success ) {
                        return;
                    }
                    for (var i = 0; i < resp.user_permissions.length; i++) {
                        var seletedValue = resp.user_permissions[i].permission_id;
                        $('#'+seletedValue).prop('checked',true);
                    }
                }
            })
        });

    });
</script>
@include('partials.alert')
@stop
