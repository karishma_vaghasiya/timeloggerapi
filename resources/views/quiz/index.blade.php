@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
    <h1>Quiz</h1>
    <ul class="breadcrumb side">
        <li><i class="fa fa-home fa-lg"></i></li>
        <li>Questions List</li>
    </ul>
    </div>
    <div>
        <a href="<?=URL::route('quiz.create')?>" class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
           <div class="card-body">
              <table id="employee" class="table table-hover table-bordered" border="1 px">
                 <thead>
                    <tr>
                        <th></th>
                        <th>Question</th>
                        <th>Time</th>
                        <th>Action</th>
                    </tr>
                 </thead>
                 <tbody></tbody>
              </table>
           </div>
        </div>
    </div>
</div>
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>

<script>
    $(document).ready(function()
    {
        $('#employee').DataTable(
        {
            "bProcessing" : true,
            "bServerSide" : true,
            "ajax" :"<?= URL::route('quiz.index') ?>",
            "aaSorting": [
                [ 1, "desc" ]
            ],
            "aoColumns" :[{
                mData: "id",
                bSortable: false,
                bVisible: false
            },
            { mData: 'question' },
            { mData: 'time' },
            {
                mData: null,
                bSortable : false,
                mRender:function(v,t,o)
                {
                    var extra_html = '';

                    var path_del = "<?= route('quiz.delete',['id'=>':id']) ?>";
                    path_del = path_del.replace(':id',o['id']);

                    var path_edit = "<?= route('quiz.edit',['id'=>':id']) ?>";
                    path_edit = path_edit.replace(':id',o['id']);

                    var path_send = "<?= route('quiz.send',['id'=>':id']) ?>";
                    path_send = path_send.replace(':id',o['id']);

                    if(o['is_send'] == 0) {

                        extra_html += '<a href="'+path_edit+'" title="edit" class="fa fa-fw fa-edit"></a> | ';
                        extra_html += '<a href="'+path_del+'" title="delete" class="fa fa-fw fa-trash-o"></a> | ';
                        extra_html += '<a href="'+path_send+'" title="send" class="fa fa-fw fa fa-paper-plane"></a>';

                    } else {

                        extra_html += '<a href="'+path_del+'" class="fa fa-fw fa-trash-o"></a>';
                    }
                    return extra_html;
                }
            },
            ],
        });

    });
</script>
@stop
