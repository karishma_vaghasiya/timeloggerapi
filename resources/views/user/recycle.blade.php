@extends('layout.layout')
@section('content')
<div class="page-title">
   <div>
      <h1>User</h1>
      <ul class="breadcrumb side">
        <li><i class="fa fa-home fa-lg"></i></li>
        <li class="active"><a href="<?= URL::route('user.index') ?>">User Table</a></li>
        <li class="active"><a href="<?=URL::route('user.recyclebin')?>">RecycleBin</a></li>
      </ul>
   </div>
   <div><a href="<?=URL::route('user.create')?>" class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a><a href="#" class="btn btn-info btn-flat"><i class="fa fa-lg fa-refresh"></i></a><a href="#" class="btn btn-warning btn-flat"><i class="fa fa-lg fa-trash"></i></a></div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
</div>
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            <table id="employee" class="table table-hover table-bordered" border="1 px">
               <thead>
                  <tr>
                     <th><input type="checkbox" id="selectall1" name="selectall"></th>
                     <th>Profile_pic</th>
                     <th>Fullname</th>
                     <th>Position</th>
                     <th>Email</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody></tbody>
            </table>
         </div>
      </div>
   </div>
</div>
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/plugins/sweetalert.min.js') ?>
<?= Html::script('asset/js/plugins/select2.min.js') ?>
<script>
   $(document).ready(function()
   {
     $('#employee').DataTable(
     {
       "bProcessing" : true,
       "bServerSide" : true,
       "ajax" :"<?= URL::route('user.recyclebin') ?>",
       "aaSorting": [
                    [ 2, "asc" ]
        ],
       "aoColumns"   :[
                   {
                     mData: 'id' ,
                     bSortable : false,
                     mRender   : function (v, t, o) {
                     return   '<div class="checkboxer checkboxer-indigo form-inline">' +
                                       '<input type="checkbox" id="chk_'+v+'" name="id[]" value="'+v+'">' +
                                       '<label for="chk_'+v+'"></label>' +
                               '</div>';
                     }
                   },
                   { mData: 'profile_pic',
                     bSortable : false,
                     mRender: function(v,t,o)
                     {
                         var path = "<?= asset('upload/"+v+"'); ?>";
                         var img = "<img height='100' width='100' src='"+path+"''>";
                         return img; 
                     }
                   },
                   { mData: 'fullname'
                     //bSortable: true,
                   },
                   { mData: 'position'
                     //bSortable: true,
                   },
                   
                   { mData: 'email',
                     bSortable: false,
                   },
                  
                   { mData: null,
                     bSortable : false,
                     mRender:function(v,t,o)
                     {
                       var path     = "<?= URL::route('user.restore',array('id'=>':id')) ?>";
                       var path_del = "<?= URL::route('user.destroy',array('id'=>':id')) ?>";
                         
                       path     = path.replace(':id',o['id']);
                       path_del = path_del.replace(':id',o['id']);
                       
                        
                       var extra_html = '<a href="'+path+'" class="fa fa-undo"></a> |' +
                       
                                       '<a href="'+path_del+'" class="fa fa-fw fa-trash-o"></a>' 
                                         
                                          
   
                         return extra_html;
                       }
                     },
               ],
   });
      //   $('#demoNotify').click(function(){
      //   $.notify({
      //     title: "Update Complete : ",
      //     message: "Something cool is just updated!",
      //     icon: 'fa fa-check-circle' 
      //   },{
      //     type: "danger"
      //   });
      // });
           $('#employee input:checkbox[name="selectall"]').click(function() 
           {
               var is_checked = $(this).is(':checked');
               $(this).closest('table').find('tbody tr td:first-child input[type=checkbox], thead tr th:first-child input[type=checkbox], tfoot tr th:first-child input[type=checkbox]')
                                       .prop('checked', is_checked);
           });
   });
</script>
@stop