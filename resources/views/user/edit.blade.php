@extends('layout.layout')
@section('content')
<div class="page-title">
    <div style="float: left;width: 100%">
        <div style="float: left">
            <h1>User</h1>
            <ul class="breadcrumb side">
                <li><i class="fa fa-home fa-lg"></i></li>
                <li>Update</li>
            </ul>
        </div>
        @if(!empty($last_logs_date['time']))
            <div style="float: right;background-color: #009688;padding: 10px;color: #fff;font-size: 15px;"><strong>Last Log Date: </strong>{{$last_logs_date['time']}}</div>
       @endif
    </div>
</div>
<div class="clearix"></div>
<div class="row">
    <div class="col-md-3" style="padding: 0">
        <div class="card">
            <div class="card-body">
                @if(isset($users['profile_pic']))
                    <div class=" avtar text-center">
                        <img src="/upload/<?= $users['profile_pic'] ?>" alt="" style="width:150px;height:150px;margin-bottom: 20px">
                        <a href="http://timeloggerapi.local/user/profile/upload-photo/{{$users['id']}}" class="btn btn-primary iframe cboxElement">Update Photo</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
            <h3 class="card-title">Update</h3>
            <div class="card-body">
                <?= Form::model($users,['route'=>['user.update','id'=>$users->id],'files'=> true , 'class' => 'form-horizontal','method'=>'put']) ?>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputGender">Gender</label>
                    <div class="col-sm-9">
                        <?= Form::radio('gender', 'male',old('gender')); ?> Male
                        <?= Form::radio('gender', 'female',old('gender')); ?> Female
                        <?= $errors->first('gender',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="input Fullname">FullName</label>
                    <div class="col-sm-9">
                        <?= Form::text('fullname',old('fullname'),['class'=>'form-control' , 'placeholder' => 'FullName' ]); ?>
                        <?= $errors->first('fullname', "<span class='text-danger'>:message</span>"); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputFull Name">Position</label>
                    <div class="col-sm-9">
                       <?= Form::select('position' ,[''=>'select','Project Manager'=>'Project Manager','Php Developer'=>'Php Developer', 'Android Developer'=>'Anroid Developer', 'Web Developer'=>'Website Desginer' ,'Magento Developer'=>'Magento Developer'], old('position') ,['class' => 'form-control']); ?>
                        <?= $errors->first('position',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputDate of Birth">Date of Birth</label>
                    <div class="col-sm-9">
                        <?= Form::text('birthdate', old('birthdate'), ['class' => 'form-control','id'=>'birth_Date','placeholder' => 'Date of Birth']); ?>
                        <?= $errors->first('birthdate',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputEmail">Email</label>
                    <div class="col-sm-9">
                        <?= Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email']); ?>
                        <?= $errors->first('email',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputContact No">Contact No</label>
                    <div class="col-sm-9">
                        <?= Form::text('contact', old('contact'), ['class' => 'form-control', 'placeholder' => 'Contact No']); ?>
                        <?= $errors->first('contact',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputEmergency No">Emergency Contact No</label>
                    <div class="col-sm-9">
                        <?= Form::text('emergency_contact',old('emergency_contact'), ['class' => 'form-control', 'placeholder' => 'Emergency Contact No']); ?>
                        <?= $errors->first('emergency_contact',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputEmergency No">Address</label>
                    <div class="col-sm-9">
                        <?= Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => 'Address']); ?>
                        <?= $errors->first('address',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputJoiningDate">Joining Date</label>
                    <div class="col-sm-9">
                        <?= Form::text('joining_date',old('joining_date'), ['class' => 'form-control', 'placeholder' => 'JoiningDate','id'=>'joining_date']); ?>
                        <?= $errors->first('joining_date',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputJoiningDate">Resign Date</label>
                    <div class="col-sm-9">
                        <?= Form::text('resign_date',old('resign_date'), ['class' => 'form-control', 'placeholder' => 'Resign Date','id'=>'resign_date']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputJoiningDate">Last Serving Date</label>
                    <div class="col-sm-9">
                        <?= Form::text('serving_date',old('serving_date'), ['class' => 'form-control', 'placeholder' => 'Last Serving Date','id'=>'serving_date']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputJoiningDate">Facebook</label>
                    <div class="col-sm-9">
                        <?= Form::text('facebook',old('facebook'), ['class' => 'form-control', 'placeholder' => 'Facebook','id'=>'facebook']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputJoiningDate">Twitter</label>
                    <div class="col-sm-9">
                        <?= Form::text('twitter',old('twitter'), ['class' => 'form-control', 'placeholder' => 'Twitter','id'=>'twitter']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="inputJoiningDate">Linkedin</label>
                    <div class="col-sm-9">
                        <?= Form::text('linkedin',old('linkedin'), ['class' => 'form-control', 'placeholder' => 'Linkedin','id'=>'linkedin']); ?>
                        <?= $errors->first('linkedin',"<span class='text-danger'>:message</span>");?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary" value="save_exit" name="save_button">Save & Exit</button>
                    <button type="submit" class="btn btn-primary" value="save" name="save_button">Save</button>
                    <a href="<?=URL::route('user.index',['type'=>'currentemp'])?>">
                    <button type="submit" class="btn btn-white btn-default" value="cancel" id="" name="cancel">Cancel</button>
                    </a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('style')
<?= Html::style('css/colorbox.css') ?>
<?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
@stop
@section('script')
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
<?= Html::script('js/jquery.colorbox-min.js') ?>
<script type="text/javascript">
    $( document ).ready(function(){
        var currentDate = new Date();  
        //birth date
        $('#birth_Date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
            endDate:currentDate,
        });
        
        //joining date
        var joining_date = "{{ old('joining_date',$users['joining_date'])}}";
        $('#joining_date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
            endDate:currentDate,
        });

        // resign date
        var resign_date = "{{ old('resign_date',$users['resign_date'])}}";
        
        $('#resign_date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
        });
        $("#resign_date").datepicker("setDate",resign_date);

        //serving date
        var serving_date = "{{ old('serving_date',$users['serving_date'])}}";
        $('#serving_date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
        });
        $("#serving_date").datepicker("setDate",serving_date);

        parent.$.fn.colorbox.close();

        $(".iframe").colorbox({
            iframe   : true,
            width    : "70%",
            height   : "80%",
            onClosed : function(){ location.reload(true); }
        });
    });    
</script>
@include('partials.alert')
@stop