@extends('layout.layouts')
@section('content')

<div class="clearix"></div>
<div class="col-md-12">
    <div class="card">
    <h3 class="card-title">Create</h3>
    <div class="card-body">
        @include('partials.alert')
        <?= Form::open(array('url' => route('user.store') ,'class' => 'form-horizontal' ,'files' => true)) ?>
        {{ session('msg') }}
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputProfile">Profile</label>
        <div class="col-sm-8">
            <?= Form::file('profile_pic', ['class' => 'form-control']); ?>
            <?= $errors->first('profile_pic',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputGender">Gender</label>
        <div class="col-sm-8">
            <?= Form::radio('gender', 'male'); ?> Male
            <?= Form::radio('gender', 'female'); ?> Female
            <?= $errors->first('gender',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>         
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputFull Name">Full Name</label>
        <div class="col-sm-8">
            <?= Form::text('fullname', null, ['class' => 'form-control', 'placeholder' => 'Full Name']); ?>
            <?= $errors->first('fullname',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputPosition">Position</label>
        <div class="col-sm-8">
            <?= Form::select('position', [''=>'select','php Developer'=>'Php Developer', 'android'=>'Anroid Developer', 'web'=>'Website Desginer' ,'Magento Developer'], null,['class' => 'form-control']); ?>
            <?= $errors->first('position',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputDate of Birth">Date of Birth</label>
        <div class="col-sm-8">
            <?= Form::date('birthdate', null, ['class' => 'form-control', 'placeholder' => 'Date of Birth']); ?>
            <?= $errors->first('birthdate',"<span class='text-danger'>:message</span>");?>
        </div>
    </div> 
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputEmail">Email</label>
        <div class="col-sm-8">
            <?= Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']); ?>
            <?= $errors->first('email',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>    
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputPassword">Password</label>
        <div class="col-sm-8">
            <?= Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']); ?>
            <?= $errors->first('password',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>  
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputConfirmPassword">Confirm Password</label>
        <div class="col-sm-8">
            <?= Form::password('password_confirm', ['class' => 'form-control', 'placeholder' => 'Password']); ?>
            <?= $errors->first('password_confirm',"<span class='text-danger'>:message</span>");?>
        </div>
    </div> 
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputContact No">Contact No</label>
        <div class="col-sm-8">
            <?= Form::text('contact', null, ['class' => 'form-control', 'placeholder' => 'Contact No']); ?>
            <?= $errors->first('contact',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputEmergency No">Emergency Contact No</label>
        <div class="col-sm-8">
            <?= Form::text('emergency_contact', null, ['class' => 'form-control', 'placeholder' => 'Emergency Contact No']); ?>
            <?= $errors->first('emergencyno',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputEmergency No">Address</label>
        <div class="col-sm-8">
            <?= Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address']); ?>
            <?= $errors->first('address',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="inputPosition">Role</label>
        <div class="col-sm-8">
            <?= Form::select('role', [''=>'Select','SuperAdmin'=>'SuperAdmin', 'Admin'=>'Admin'], null,['class' => 'form-control']); ?>
            <?= $errors->first('role',"<span class='text-danger'>:message</span>");?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-8 col-md-offset-3">
            <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
    </div>
</div>


@stop
