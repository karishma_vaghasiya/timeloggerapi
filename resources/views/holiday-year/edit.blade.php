@extends('layout.layout')
@section('content')
<style type="text/css">
    .avtar{
    background-color: #fff;
    padding:20px 0;
    }
    .avtar button.btn-primary {
    float: none;
    width: 80%;
    margin: 20px auto;
    }
    .avtar form {
    float: none;
    width: 90%;
    margin: auto;
    }
    .profile-detail{
    width:100%;
    background-color: #fff;
    padding:20px 0;
    }
    .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
    margin: 10px 0;
    }
    .profile-detail label {
    float: right;
    font-size: 16px;
    margin: 19px 0;
    }
    .profile-detail .form-control {
    float: left;
    width: 98%;
    margin: 10px 0;
    }
    .profile-detail textarea {
    width: 98%;
    }
    .profile-detail button {
    float: left;
    }
    .checkbox {
    float: left;
    width: 98%;
    text-align: left;
    }
    .checkbox label {
    float: left;
    width: auto;
    margin: 0;
    }
    .checkbox input[type="checkbox"] {
    float: left;
    width: auto;
    margin: 4px 0;
    }
    .dl-horizontal.dd-horizontal > dd {
    width: 100%;
    text-align: left;
    }
    .text-left.check-radio{
        margin-top:20px;
    }
    </style>
<div class="page-title">
    <div>
        <h1>Holiday Master</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>Edit</li>
        </ul>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div>
<div class="clearix"></div>
<div class="col-md-12">
    <div class="card">
        <h3 class="card-title">Edit</h3>
        <div class="card-body">
            @include('partials.alert')
           <?= Form::model($holiday_year,['route'=>['holiday-year.update','id'=>$holiday_year->id], 'class' => 'form-horizontal','method'=>'put']) ?>
            {{ session('msg') }}
            
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Day</label>
                <div class="col-sm-8">
                    <?= Form::text('day', old('day'), ['class' => 'form-control', 'placeholder' => 'DayName','id'=>'day','readonly'=>true]); ?>
                    <?= $errors->first('day',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
             <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Holiday</label>
                <div class="col-sm-8">
            <?= Form::select('holiday_id',$holiday_master, null ,['class' => 'form-control']); ?>
                    <?= $errors->first('holiday_id',"<span class='text-danger'>:message</span>");?>
                </div>
              </div>
             <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Date</label>
                <div class="col-sm-8">
                    <?= Form::text('holiday_date', null, ['class' => 'form-control', 'placeholder' => 'Date','id'=>'date1']); ?>
                    <?= $errors->first('holiday_date',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>      
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-primary" value="save_exit" name="save_button">Save & Exit</button>
                    <button type="submit" class="btn btn-primary" value="save" name="save_button">Save</button>
                    <a href="<?= URL::route('holiday-year.index') ?>" class="btn btn-white btn-default"> Cancel</a>
                    
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('style')
    <?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
@stop
@section('script')
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
    <script type="text/javascript">
    jQuery(document).ready(function() {
        $('#date1').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
        });
        var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        $('#date1').change(function(){
            var olddate = $(this).val();
            var dates = olddate.split("-").reverse().join("-");
            var newdate = new Date(dates);
            var dayname = weekday[newdate.getDay()];
            $('#day').val(dayname);
        });
        // $('form').submit(function(){
        //     $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin" value="Please Wait"></i>').prop('disabled', true);
        // });

    });
    </script>
@include('partials.alert') 
@stop