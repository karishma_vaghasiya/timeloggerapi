@extends('layout.layout')
@section('content')
<div class="page-title">
        <div>
          <h1 style="float: left;width: auto;margin-right: 10px;line-height: 40px;">Leaves</h1>
          @if(auth()->user()->is_admin)
            <?=Form::select('user', $user, null, array('class' => 'form-control select', 'id' => 'user','style'=>'float: left;width: 170px;'))?>
            <?=Form::select('leavetype',config('project.leave_type'), null, array('class' => 'form-control select', 'id' => 'leavetype','style'=>'float: left;width: 170px;margin-left: 10px;display: inline-block;'))?>
          @endif
          </div>
          <a href="javascript:void(0)" class="btn btn-primary btn-flat approve_leaves">Approve</a>
          <a href="javascript:void(0)" class="btn btn-primary btn-flat disapprove_leaves">Disapprove</a>
          <!-- <div><a href="<?=URL::route('leaves.create')?>" class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a> --><!-- <a href="#" class="btn btn-warning btn-flat"><i class="fa fa-lg fa-trash"></i></a> </div>-->
</div>
<div class="row" id="leavesummary">
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
</div>
<div class="row">
  <div class="col-md-12">
    <div class="modal fade" id="leavestatus" role="dialog" tabindex="-1" aria-labelledby="leave-detailsLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Leave Approve</h3>
          </div>
          <div class="modal-body" id="leave_body">
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
      <table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
      <thead>
        <tr>
        <th style="width: 45px;text-align: center;"><div class="animated-checkbox"><label><input type="checkbox" id="selectall" class="select_check_box mt-1" ><span class="label-text"></span></label></div></th>
        <th>Date</th>
        @if(auth()->user()->is_admin)
        <th>Employee</th>
        @endif
        <th>Leave Type</th>
        <th>Leave Time</th>
        <th>Leave Purpose</th>
        <th>From Hours</th>
        <th>To Hours</th>
        <th>Status</th>
        <th>Action</th>
        </tr>
      </thead> 
      </table>
      </div>
    </div>
  </div>
</div>
        
@stop
@section('script')

<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/delete_script.js') ?>
<?= Html::script('asset/js/leaveapproval.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/sweetalert.min.js') ?>
<script>
var title = "Are you sure to delete selected record(s)?";
var text = "You will not be able to recover this record";
var type = "warning";
var delete_path = "<?=URL::route('leaves.delete') ?>";
var token = "{{ csrf_token() }}";

//approve
var approve_title = "Are you sure to approve selected record(s)?";
var approve_text = "You will not be able to recover this record";
var approve_type = "warning";
var approve_delete_path = "<?=URL::route('leave.status') ?>";
var approve_confirmButtonText = "Yes, approve it!";

//disapprove
var disapprove_title = "Are you sure to disapprove selected record(s)?";
var disapprove_text = "You will not be able to recover this record";
var disapprove_type = "warning";
var disapprove_delete_path = "<?=URL::route('leave.status') ?>";
var disapprove_confirmButtonText = "Yes, disapprove it!";

$(document).ready(function()
{
  $('.approve_leaves').click(function(){
      var leave_id = $('#employee tbody input[type=checkbox]:checked');
      approve_leave_checkLength(leave_id,approve_title,approve_text,approve_type,approve_delete_path,approve_confirmButtonText);
  });
  $('.disapprove_leaves').click(function(){
      var leave_id = $('#employee tbody input[type=checkbox]:checked');
      disapprove_leave_checkLength(leave_id,disapprove_title,disapprove_text,disapprove_type,disapprove_delete_path,disapprove_confirmButtonText);
  });
  $(function()
  {
    var master = $('#employee').dataTable({
    "bProcessing": false,
    "bServerSide": true,
    "autoWidth": true,
    "aaSorting": [
        [1, "desc"]
    ],
    "sAjaxSource": "{{ URL::route('leaves.index')}}",
    "fnServerParams": function ( aoData ) {
        aoData.push({ "name": "act", "value": "fetch" });
        aoData.push({ "name": "user", "value": $('#user').val() });
        aoData.push({ "name": "leavetype", "value": $('#leavetype').val() });
        server_params = aoData;
    },
    "aoColumns"   :[
    {
        mData: "id",
        bSortable: false,
        bVisible: true,
        sWidth: "2%",
        mRender: function(v, t, o) 
        {
            return '<div class="animated-checkbox">'
            +'<label><input type="checkbox" class="select_check_box mt-1" id="chk_'+v+'" name="id[]" value="'+v+'"/><span class="label-text"></span></label>'
            +'</div>';
        }
    },
      { mData: 'date',sWidth: "12%",},
      @if(auth()->user()->is_admin)
        { mData: 'name',sWidth: "10%"},
      @endif
      { mData: 'leave_type',sWidth: "7%",},
      { mData: 'leave_time',sWidth: "4%",},
      { mData: 'leave_purpose',sWidth: "20%",},
      { mData: 'from_hour',sWidth: "7%",},
      { mData: 'to_hour',sWidth: "7%"},
      {
          "mData": "status",
          sClass: "text-center",
          sWidth: "7%",
          mRender:function(v,t,o)
          {
              if (v == 'a') {
                  return '<span class="badge bg-green">Approve</span>'
              } else if(v == 'd'){
                  return '<span class="badge bg-red">Dis Approve</span>'
              } else {
                  return '<span class="badge bg-yellow">Pending</span>'
              }
          }
      },
      
      { mData: null,
        bSortable:false,
        sWidth: "12%",
        mRender:function(v,t,o) {

          if (o['status'] == 'p' || o['status'] == 'd') {
            @if(auth()->user()->is_admin)
              if(o['status'] == 'p'){
                var extra_html = "<a href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" style='font-size: 18px' class='fa fa-fw fa-trash'></a><a href='javascript:void(0)' onclick=\"leaveApproval('"+approve_delete_path+"','"+approve_title+"','"+approve_text+"','"+token+"','"+approve_type+"',"+o['id']+",'"+approve_confirmButtonText+"','approve')\" style='font-size: 18px' class='fa fa-fw fa-check'></a><a href='javascript:void(0)' onclick=\"leaveApproval('"+disapprove_delete_path+"','"+disapprove_title+"','"+disapprove_text+"','"+token+"','"+disapprove_type+"','"+o['id']+"','"+approve_confirmButtonText+"','disapprove')\" style='font-size: 18px' class='fa fa-fw fa-close'></a>";
              }
              else{
                  var extra_html = "<a href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\" style='font-size: 18px' class='fa fa-fw fa-trash'></a><a href='javascript:void(0)' onclick=\"leaveApproval('"+approve_delete_path+"','"+approve_title+"','"+approve_text+"','"+token+"','"+approve_type+"',"+o['id']+",'"+approve_confirmButtonText+"','approve')\" style='font-size: 18px' class='fa fa-fw fa-check'></a>";
              }
            @else
              var extra_html = "<a href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+"','"+approve_confirmButtonText+"')\" style='font-size: 18px' class='fa fa-fw fa-trash'></a>";
            @endif
            return extra_html;
          }else{
            return '';
            
          }
       }
      },
    ],
      fnDrawCallback: function(oSettings) {
        res_html = oSettings.json.res_html;
        $('#leavesummary').html(res_html);
      }
    });     
  });
  $('#user').change(function(){
    $('.dataTable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    });
  });
  $('#leavetype').change(function(){
    $('.dataTable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    });
  });
  $(".select_check_box").on('click', function(){
    var is_checked = $(this).is(':checked');
    console.log($(this).closest('table').find('tbody tr td:first-child input[type=checkbox]'));
    $(this).closest('table').find('tbody tr td:first-child input[type=checkbox]').prop('checked',is_checked);
    $(".select_check_box").prop('checked',is_checked);
  });
});

</script>
@include('partials.alert')
@stop