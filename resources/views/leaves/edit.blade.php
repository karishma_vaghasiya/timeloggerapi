@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Leaves</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>Update</li>
        </ul>
    </div>
</div>
<div class="clearix"></div>
<div class="col-md-12">
    <div class="card">
        <h3 class="card-title">Update</h3>
        <div class="card-body">
        @include('partials.alert')
        <?= Form::model($leaves,['route'=>['leaves.update','id'=>$leaves->id],'class' => 'form-horizontal', 'method'=>'put']) ?>
        <div class="form-group">
            <label class="control-label col-sm-2">Date</label>
            <div class="col-sm-8">
                <?= Form::date('date', old('date'), ['class' => 'form-control', 'placeholder' => 'From_Date']); ?>
                <?= $errors->first('date',"<span class='text-danger'>:message</span>");?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">LeaveType</label>
            <div class="col-sm-8">
                <?= Form::select('leave_type',[''=>'select','Sick Leave'=>'Sick Leave', 'Casual Leave'=>'Casual Leave','Privilege Leave'=>'Privilege Leave'] ,old('leave_type'), ['class' => 'form-control']); ?>
                <?= $errors->first('leave_type',"<span class='text-danger'>:message</span>");?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">LeaveTime</label>
            <div class="col-sm-8">
                <?= Form::select('leave_time', [''=>'select','full day'=>'Full Day', 'half day'=>'Half Day'], old('leave_time'),['class' => 'form-control','id' => 'leave_time']); ?>
                <?= $errors->first('leave_time',"<span class='text-danger'>:message</span>");?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">LeavePurpose</label>
            <div class="col-sm-8">
                <?= Form::text('leave_purpose', old('leave_purpose'), ['class' => 'form-control', 'placeholder' => 'LeavePurpose']); ?>
                <?= $errors->first('leave_purpose',"<span class='text-danger'>:message</span>");?>
            </div>
        </div>
        <div class="form-group" id="from_hour_div" >
            <label class="control-label col-sm-2">From hours</label>
            <div class="col-sm-8">
                <?= Form::text('from_hour', old('from_hour'), ['class' => 'form-control', 'placeholder' => 'From hours']); ?>
                <?= $errors->first('contact',"<span class='text-danger'>:message</span>");?>
            </div>
        </div>
		 <div class="form-group" id="to_hour_div">
            <label class="control-label col-sm-2">To hours</label>
            <div class="col-sm-8">
                <?= Form::text('to_hour', old('to_hour'), ['class' => 'form-control', 'placeholder' => 'To hours']); ?>
                <?= $errors->first('contact',"<span class='text-danger'>:message</span>");?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="<?= URL::route('leaves.index') ?>" class="btn btn-cyan btn-default"> Cancel</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    </div>
</div>
@stop
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('form').submit(function(){
        $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
    });

    var leaveTime = $('#leave_time');

    fromAndToHoursShowHide();

    leaveTime.on('change',function(){
        fromAndToHoursShowHide();
    });

    function fromAndToHoursShowHide() {

        leaveTimeVal = leaveTime.val();

        if (leaveTimeVal == 'half day') {
            $('#from_hour_div').show();
            $('#to_hour_div').show();
        } else {
            $('#from_hour_div').hide();
            $('#to_hour_div').hide();
        }
    }
});
</script>
@stop
