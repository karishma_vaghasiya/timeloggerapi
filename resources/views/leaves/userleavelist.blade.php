<div class="bs-component well">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#step1" data-toggle="tab">Step 1</a></li>
    <li><a href="#step2" data-toggle="tab">Step 2</a></li>
    <li><a href="#step3" data-toggle="tab">Step 3</a></li>
  </ul>
  <div class="tab-content mt-20" id="myTabContent">
    <div class="tab-pane fade active in" id="step1">
      <div class="panel panel-primary" id="step3data">
        <div class="panel-heading">
          <h3 class="panel-title" id="date-panel-second">Leave Summary</h3>
        </div>
        <div class="panel-body">
            <div class="button-section col-xs-3 pl-0"><a style="width:100px" class="btn btn-danger" href="javascript:;">Casual</a></div>
            <div class="leave-used col-xs-3 text-center"><strong class="text-muted">USED</strong><br><span>{{ $casual_leave}}</span></div>
            <div class="avail col-xs-3 text-center"><strong class="text-muted">AVAIL</strong><br><span><?= 6-$casual_leave ?></span></div>
            <div class="avail col-xs-3 text-center"><strong class="text-muted">TOTAL</strong><br><span>6</span></div>

            <div class="button-section col-xs-3 pl-0"><a style="width:100px" class="btn btn-danger" href="javascript:;"> Sick</a></div>
            <div class="leave-used col-xs-3 text-center"><strong class="text-muted">USED</strong><br><span>{{ $sick_leave}}</span></div>
            <div class="avail col-xs-3 text-center"><strong class="text-muted">AVAIL</strong><br><span><?= 6-$sick_leave ?></span></div>
            <div class="avail col-xs-3 text-center"><strong class="text-muted">TOTAL</strong><br><span>6</span></div>
          
            <div class="button-section col-xs-3 pl-0"><a style="width:100px" class="btn btn-danger" href="javascript:;">
                  Privilege</a></div>
            <div class="leave-used col-xs-3 text-center"><strong class="text-muted">USED</strong><br><span>{{ $privilege_leave}}</span></div>
            <div class="avail col-xs-3 text-center"><strong class="text-muted">AVAIL</strong><br><span><?= 6-$privilege_leave ?></span></div>
            <div class="avail col-xs-3 text-center"><strong class="text-muted">TOTAL</strong><br><span>6</span></div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="step2">
      <h3>Leave History</h3>
        @foreach($leave_list as $key=>$value)
          <div class="panel panel-primary" id="step3data">
            <div class="panel-heading">
              <h3 class="panel-title" id="date-panel-second">{{$value['date']}} ({{$value['leave_type']}})</h3>
            </div>
            <div class="panel-body">
              <div id="formwholeday"><strong>{{ucwords($value['leave_time'])}}</strong></div>
              <div id="formpurpose">{{$value['leave_purpose']}}</div>
            </div>
          </div>
        @endforeach
    </div>
    <div class="tab-pane fade" id="step3">
      <div class="panel panel-primary" id="step3data">
        <div class="panel-heading">
          <h3 class="panel-title" id="date-panel-second">{{$leaves['date']}} ({{$leaves['leave_type']}})</h3>
        </div>
        <div class="panel-body">
          <div id="formwholeday"><strong>{{ucwords($leaves['leave_time'])}}</strong></div>
          <div id="formpurpose">{{$leaves['leave_purpose']}}</div>
        </div>
      </div>
      <div class="row">
      <?= Form::open(array('url' => route('leave.status') ,'class' => 'form-horizontal','name'=>'firstajaxform')) ?>
        <input type="hidden" name="id" value="{{$leaves['id']}}">
        <div class="col-md-6">
          <button class="btn btn-primary" name="approve" value="approve" type="submit">Approve</button>
        </div>
        @if($leaves['status'] == 'a' || $leaves['status'] == 'p')
          <div class="col-md-6 text-right">
            <button class="btn btn-primary" name="approve" value="disapprove" type="submit">DisApprove</button>
          </div>
        @endif
      <?= Form::close()?>  
      </div>
    </div>
  </div>
</div>