@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Weekly Logs</h1>
    </div>
</div>
<?php $date_arr=[]; $name_arr=[];?>

@foreach ($log_data as $product=>$value)
	<?php $date_arr[] = $value['Date'];
		$name_arr[]	= $value['fullname'];
	?>
@endforeach
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                	<div class="table-responsive">
	                    <table id="employee" class="table table-hover table-bordered" border="1 px" style="width: 100%;">
	                        <thead>
						      	<tr>
							      <th width="15%" style="padding: 5px;">Name</th>
							      <th width="10%">In/Out</th>
							      @foreach (array_unique($date_arr) as $key=>$value)
							      	<?php $dayname = date('D', strtotime($value));
							      	$value = date('d-m-Y', strtotime($value));?>
							      	<th style="width: 10px;padding-left:3px;padding-right: 3px">{{$dayname}}<br/>{{$value}}</th>
							      @endforeach
						      	</tr>
						    </thead>
						    <tbody>
							    @foreach (array_unique($name_arr) as $key=>$value)
							    	
								    	<tr>
											<th rowspan="3" style="padding: 5px;width: 30px">{{$value}}
											</th>
										    <th style="width: 10px">In</th>
										    <?php $c=0;?>
									    	@foreach ($log_data as $data_product=>$data_value)
													@if($data_value['fullname'] == $value)
														<th style="width: 10px;padding-left:3px;padding-right: 3px">{{substr($data_value['check_in'],0,5)}}</th>
														<?php $c++;?>
													@endif
											@endforeach
											<?php $count=count(array_unique($date_arr))-$c;?>
											@for($i=0;$i<$count;$i++)
												<th></th>
											@endfor
								      	</tr>
								      	<tr>
									    	<th style="width: 10px">Out</th>
									    	@foreach ($log_data as $data_product=>$data_value)
												@if($data_value['fullname'] == $value)
													<th style="width:10px;padding-left:3px;padding-right: 3px">{{substr($data_value['check_out'],0,5)}}</th>
												@endif
											@endforeach	
											<?php $count=count(array_unique($date_arr))-$c;?>
											@for($i=0;$i<$count;$i++)
												<th></th>
											@endfor
								      	</tr>
								      	<tr>
									    	<th style="width: 10px">Hours</th>
									    	@foreach ($log_data as $data_product=>$data_value)

												@if($data_value['fullname'] == $value)
													<th @if(($data_value['time_diff']) < '09:00:00')style="background-color:{{config('project.error_code.code')}};width:10px;padding-left:3px;padding-right:3px;" @else style="width:10px;padding-left:3px;padding-right: 3px;"@endif>{{substr($data_value['time_diff'],0,5)}}</th>
												@endif
											@endforeach	
											<?php $count=count(array_unique($date_arr))-$c;?>
											@for($i=0;$i<$count;$i++)
												<th></th>
											@endfor

								      	</tr>
							    @endforeach
						    </tbody>
	                    </table>
                	</div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')

@stop
