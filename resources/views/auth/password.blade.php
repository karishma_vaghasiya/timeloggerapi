@extends('partials.login-page')
@section('content')
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="login-content">
<div class="logo"><img src="/asset/images/logo.png" class="img-responsive"></div>
<div class="login-box">
    <?= Form::open(array('url' => url('/password/email') ,'class' => 'login-form','method'=>'POST')) ?>
    <?= session('msg'); ?>
    <div class="form-group">
        <label for="inputEmail">EMAIL</label>
        <?= Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'EMAIL']); ?>
        <?= $errors->first('email',"<span class='text-danger'>:message</span>");?>
    </div>
    <div class="form-group">
        <div class="btn-group">
            <button class="btn btn-primary">Password Reset</button>
        </div>
    </div>
    
</div>
@stop
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

 <!-- <?= Html::script('js/jquery-dynamic-form.js') ?> -->

<script>
    jQuery(document).ready(function() {
        $('form').submit(function(){
            $(this).find('button:submit').html('<span><i class="fa fa-spinner fa-spin">Please wait..</span></i>').prop('disabled', true);
        });

    });
    
</script>