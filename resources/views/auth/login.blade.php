@extends('partials.login-page')
@section('content')
<!--  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="login-content">
<div class="logo"><img src="asset/images/logo.png" class="img-responsive"></div>
<div class="login-box">
<?= Form::open(array('url' => route('user.login') ,'class' => 'login-form','id' => 'btn_submit')) ?>
<?= session('msg'); ?>
<h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>LOGIN </h3>
<div class="form-group">
    <label for="inputEmail">EMAIL</label>
    <?= Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'EMAIL']); ?>
    <?= $errors->first('email',"<span class='text-danger'>:message</span>");?>
</div>
<div class="form-group">
    <label>PASSWORD</label>
    <?= Form::password('password', ['class' => 'form-control', 'placeholder' => 'PASSWORD']); ?>
    <?= $errors->first('password',"<span class='text-danger'>:message</span>");?>
</div>
<div class="form-group">
    <div class="btn-group">
        <button class="btn btn-primary btn-lg btn-block ladda-button">Login</button>
    </div>
</div>
<div class="form-group">
    <div class="utility">
        <div class="animated-checkbox">
            <label class="semibold-text">
            <input type="checkbox" id="rememberme" name="remember" value="1"><span class="label-text">Remember me</span>
            </label>
        </div>
        <p class="semibold-text mb-0"><a id="toFlip" href="{{url('/password/email')}}">Forgot Password ?</a></p>
    </div>
</div>
<?= Form::close() ?>
@stop
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

 <!-- <?= Html::script('js/jquery-dynamic-form.js') ?> -->

<script>
    jQuery(document).ready(function() {
        $('form').submit(function(){
            $(this).find('button:submit').html('<span><i class="fa fa-spinner fa-spin"></i>Please Wait..</span>').prop('disabled', true);
        });

    });

</script>
