<!DOCTYPE html>
<html>
<head>
	<title></title>
    <?= Html::style('front/css/style.css') ?>
    <?= Html::style('front/css/color.css') ?>
	<?= Html::style('admin/globals/plugins/Jcrop/css/jquery.Jcrop.min.css') ?>
	<?= Html::script('front/js/jquery.min.js') ?>
	<?= Html::script('vendorlib/jquery/jquery_ui/jquery-ui.min.js') ?>
	<?= Html::script('front/js/bootstrap.min.js') ?>
	<?= Html::script('admin/globals/plugins/Jcrop/js/jquery.Jcrop.min.js') ?>
    <!-- <?= Html::style('/main.css') ?> -->
</head>
<body>
    <div class="container" align="center">
        <div class="row">
            <div class="col-md-12">
                <h2>Crop and adjust Your Profile Photo</h2>
                    <div id="profile-photo-handler">
	                   <img src="{{ ('/upload/' . $profile->temp_photo) }}" class="img-responsive mn pn">
                    </div>
                    
	                <form id="frm_crop_photo" action="<?= URL::route('profile.photo.crop.post') ?>" method="post">
                        <div class="form-group">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <input type="hidden" id="x" name="x" />
                            <input type="hidden" id="y" name="y" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" /></br>
                            <!-- <input name="submit" type="submit" class="btn btn-complete" value="Crop & Save" /> -->
                            <button type="submit" class="btn btn-primary">Crop & Save</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    var token = "<?= csrf_token() ?>";

    $('#profile-photo-handler').Jcrop({
        aspectRatio: 1,
        setSelect:   [50, 0, 300,300],
		allowSelect: false,
        allowResize: false,
        onSelect    : updateCoords,
        onChange: updateCoords
    });

    function updateCoords(c)
    {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };

    // $('#frm_crop_photo').submit(function(){
    //     parent.$.fn.colorbox.close();
    // });
</script>
</html>
