
@extends('layout.layout')
@section('content')
<style type="text/css">
    .avtar{
    background-color: #fff;
    padding:20px 0;
    }
    .avtar button.btn-primary {
    float: none;
    width: 80%;
    margin: 20px auto;
    }
    .avtar form {
    float: none;
    width: 90%;
    margin: auto;
    }
    .profile-detail{
    width:100%;
    background-color: #fff;
    padding:20px 0;
    }
    .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
    margin: 10px 0;
    }
    .profile-detail label {
    float: right;
    font-size: 16px;
    margin: 19px 0;
    }
    .profile-detail .form-control {
    float: left;
    width: 98%;
    margin: 10px 0;
    }
    .profile-detail textarea {
    width: 98%;
    }
    .profile-detail button {
    float: left;
    }
    .checkbox {
    float: left;
    width: 98%;
    text-align: left;
    }
    .checkbox label {
    float: left;
    width: auto;
    margin: 0;
    }
    .checkbox input[type="checkbox"] {
    float: left;
    width: auto;
    margin: 4px 0;
    }
    .dl-horizontal.dd-horizontal > dd {
    width: 100%;
    text-align: left;
    }
    .text-left.check-radio{
        margin-top:20px;
    }
    </style>
<div class="page-title">
    <div>
        <h1>Assets Master</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>Edit</li>
        </ul>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div>
<div class="clearix"></div>
<div class="col-md-12">
    <div class="card">
        <h3 class="card-title">Edit</h3>
        <div class="card-body">
            @include('partials.alert')
            <?= Form::model($assets_master,['route'=>['assets.master.update','id'=>$assets_master->id], 'class' => 'form-horizontal','method'=>'put','files'=>true]) ?>
            {{ session('msg') }}
            
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Assets</label>
                <div class="col-sm-8">
                    <?= Form::select('assets_id' , [''=>'select Asset' ] +$assets, old('assets_id') ,['class' => 'form-control']); ?>
                    <?= $errors->first('assets_id',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Users</label>
                <div class="col-sm-8">
                    <?= Form::select('user_id' ,[''=>'select User' ] + $user, old('user_id') ,['class' => 'form-control']); ?>
                    <?= $errors->first('user_id',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Start Date</label>
                <div class="col-sm-8">
                    <?= Form::text('start_date' ,old('start_date') ,['class' => 'form-control','id'=>'start_date','placeholder' => 'Start Date']); ?>
                    <?= $errors->first('start_date',"<span class='text-danger'>:message</span>");?>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">End Date</label>
                <div class="col-sm-8">
                    <?= Form::text('end_date' ,old('end_date') ,['class' => 'form-control','id'=>'end_date','placeholder' => 'End Date']); ?>
                    <?= $errors->first('end_date',"<span class='text-danger'>:message</span>");?>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Notes</label>
                <div class="col-sm-8">
                    <?= Form::textarea('notes', old('notes'), ['class' => 'form-control', 'placeholder' => 'Enter Description']); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-primary" value="save_exit" name="save_button">Save & Exit</button>
                    <button type="submit" class="btn btn-primary" value="save" name="save_button">Save</button>
                    <a href="<?= URL::route('assets.master.index') ?>" class="btn btn-white btn-default"> Cancel</a>
                    
                    
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
@section('style')
    <?= Html::style('css/bootstrap-fileupload.css') ?>
    <?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
@stop
@section('script')
    <?= Html::script('asset/js/bootstrap-datepicker.min.js') ?> 
    <?= Html::script('js/bootstrap-fileupload.js') ?>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            var start_date = "{{ old('start_date')}}";
            var currentDate = new Date();  
            $('#start_date').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                todayHighlight: true,
                endDate:currentDate,
            });

            var end_date = "{{ old('end_date')}}"; 
            $('#end_date').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                todayHighlight: true,
            });
        });
    </script>
@include('partials.alert')
@stop
