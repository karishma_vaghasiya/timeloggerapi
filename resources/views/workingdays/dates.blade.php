@foreach($dates as $key=>$value)
    <tr @if($ids != null) @foreach($ids as $ids_key=>$ids_value) @if($ids_key == $key) id="{{$ids_value}}" @endif @endforeach @endif
      @if($value['dayname'] == "Sunday") 
        style="background-color:{{config('project.sun_code.code')}}" 
      @elseif(($value['dayname'] == "Saturday")) 
        style="background-color:{{config('project.sat_code.code')}}" 
      @endif>
      <td style="width: 45px;text-align: center;">
        <div class="animated-checkbox">
          <label>
            <input type="checkbox" name="dates" id="dates" value="{{$key}},@if($hours != null)@foreach($hours as $hrs_key=>$hrs_value)@if($hrs_key == $key){{$hrs_value}} @endif @endforeach @endif" @if($working_hrs != null) @foreach($working_hrs as $hrs_key=>$hrs_value) @if($hrs_value == $key) checked @endif @endforeach @endif>
              <span class="label-text"></span>
            </label>
          </div>  
      </td>
      <td>{{$key}}</td>
      <td>
        @foreach($week_name as $week_key=>$week_value)
          @foreach($week_value as $weekname_key=>$weekname_value)
            @if(date("d-m-Y",strtotime($weekname_value)) == $key)
              {{$week_key}}
            @endif
          @endforeach
        @endforeach
      </td>
      <td>{{$value['dayname']}} 
        @if(isset($value['odd_sat_date'])) 
          ({{$value['odd_sat_date']}} - Odd) 
        @elseif(isset($value['even_sat_date']))
          ({{$value['even_sat_date']}} - Even) 
        @elseif(isset($value['odd_sun_date'])) 
          ({{$value['odd_sun_date']}} - Odd) 
        @elseif(isset($value['even_sun_date']))
          ({{$value['even_sun_date']}} - Even) 
        @endif
      </td>
      <td style="width: 200px;">@if($hours != null) @foreach($hours as $hrs_key=>$hrs_value)  @if($hrs_key == $key) <div id="is_active" name="{{$hrs_value}}">{{$hrs_value}} </div>@endif @endforeach @endif</td>
      <td style="width:50px;">
      @if(($value['holiday_name']) !="")
        	<b>{{$value['holiday_name']}}</b>
      @endif  	
      </td>
    </tr>
@endforeach
<?= Html::script('js/jquery-2.1.4.min.js') ?>
<?= Html::script('asset/js/jquery.jeditable.min.js') ?>
<script type="text/javascript">
$(document).ready(function(){

    $('#employee tbody td div#is_active').editable('<?=route('admin.workingdays.hours')?>', {
        onblur:'submit',
        "token":'<?= csrf_token()?>',
        submitdata: function () {
          return {
          "id": $(this).closest('tr').attr('id'),
          };
        },
        callback: function( value, settings ) {
          iziToast.success({
          title:'Ok',
          message: "Working Hours Updated Successfully!",
          });
         window.setTimeout(function(){location.reload()},3000)
        },
    "width":"70px"
  });
});
</script>