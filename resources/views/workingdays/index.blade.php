@extends('layout.layout')
@section('content')
<div class="page-title">
	<div>
    	<h1>Working Days </h1>
      	<!-- <ul class="breadcrumb side">
        	<li><i class="fa fa-home fa-lg"></i></li>
            <?= Form::selectMonth('month',date('m'),['id'=>'month','class' => 'form-control breadcrumb-select input-sm']); ?>
            <?= Form::selectRange('year','2014','2030',date('Y'),['id'=>'year','class' => 'form-control breadcrumb-select input-sm']); ?>
            <input type="hidden" name="month" value="<?= date('m')?>">
            </li>
        </ul> -->
    </div>
    <div>
      <a href="javascript:void(0)" id="working_days" class="btn btn-primary btn-flat working_days">Save</a>
    </div>    
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
              <ul class="breadcrumb side">
                  <b>Month:</b> <?= Form::selectMonth('month',date('m'),['id'=>'month','class' => 'form-control breadcrumb-select input-sm']); ?>
                  <b>Year:</b> <?= Form::selectRange('year','2014','2030',date('Y'),['id'=>'year','class' => 'form-control breadcrumb-select input-sm']); ?>
                  <input type="hidden" name="month" value="<?= date('m')?>">
                  </li>
              </ul>
            	<table id="employee" class="table table-hover table-bordered" border="1 px">
               		<thead>
                  	<tr>
                      <th style="width: 45px;text-align: center;"><div class="animated-checkbox"><label><input type="checkbox" id="selectall" class="select_check_box mt-1" ><span class="label-text"></span></label></div></th>
                      <th>Date</th>
                      <th>Week Name</th>
                      <th>Day Name</th>
                      <th>Hours</th>
		                  <th>Holiday</th>
		                </tr>
               		</thead>
                    <tbody id="date_body">@include('workingdays.dates')</tbody>
               	</table>
         	</div>
      	</div>
   	</div>
</div>
@stop
@section('style')
<style type="text/css">
  .animated-checkbox input[type="checkbox"]:checked+.label-text:before{
    color: #222d32
  }
</style>
@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/plugins/sweetalert.min.js') ?>
<?= Html::script('asset/js/plugins/select2.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/jquery.jeditable.min.js') ?>
<?= Html::script('asset/js/jquery.jeditable.time.js') ?>
<script type="text/javascript">
      $('.working_days').click(function(){
        var day_id = $('#employee tbody input[type=checkbox]:checked');
        checkLength(day_id);
      });
      //Give Error when no data is selected
      function EmptyData()
      {
          swal({
             title: "Please select a record(s)",
             type:"error",
             timer: 2000,
             showConfirmButton: true
          });
      }
      function checkLength(day_id)
      {
        var selected_length = day_id.length;
        var year = $('#year').val();
        var month = $('#month').val();
        if(0 == selected_length){
            EmptyData();
        }else{
            var id = [];
            $.each(day_id, function(i, ele){
                id.push($(ele).val());
            });
            $.ajax({
              url: '<?= URL::route('admin.store.workingdays') ?>',
              type:'post',
              dataType:'json',
              data:{id:id,month:month,year:year,_token: '<?= csrf_token()?>'},
              success:function(){
                 iziToast.success({title:'Success!',message: 'Working Days Updated Successfully'});
                  window.setTimeout(function(){location.reload()},3000)
              }
          });
        }
      }
    $('#month').change(function(){
        $('#date_body').empty();
        year = $('#year').val();
        month = $(this).val();
        $.ajax({
            url: '<?= URL::route('admin.workingdays') ?>',
            type: 'post',
            data: {'month' : month, 'year':year,'_token' : '<?= csrf_token()?>'},
            success: function(resp){
                $('#date_body').html(resp.response_html);
            }
        });
    });
    $('#year').change(function(){
      $('#date_body').empty();
      month = $('#month').val();
      year = $(this).val();
      $.ajax({
          url: '<?= URL::route('admin.workingdays') ?>',
          type: 'post',
          data: {'month' : month, 'year':year,'_token' : '<?= csrf_token()?>'},
          success: function(resp){
              $('#date_body').html(resp.response_html);
          }
      });
    });
    $(".select_check_box").on('click', function(){
      var is_checked = $(this).is(':checked');
      console.log($(this).closest('table').find('tbody tr td:first-child input[type=checkbox]'));
      $(this).closest('table').find('tbody tr td:first-child input[type=checkbox]').prop('checked',is_checked);
      $(".select_check_box").prop('checked',is_checked);
    });
    console.log($('#is_active').text());
    $('#employee tbody td div#is_active').editable('<?=route('admin.workingdays.hours')?>', {
            // onblur:'submit',
            type:"time",
            submit:'ok',
            cancel:'cancel',
            "token":'<?= csrf_token()?>',
            submitdata: function () {
              return {
              "id": $(this).closest('tr').attr('id'),
              };
            },
            callback: function( value, settings ) {
              iziToast.success({
              title:'Ok',
              message: "Working Hours Updated Successfully!",
              });
             window.setTimeout(function(){location.reload()},3000)
            },
        "width":"70px"
      });
</script>
@stop
