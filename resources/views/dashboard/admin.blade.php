@extends('layout.layout')
@section('content')
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
        <p>Bootstrap3 supported admin template</p>
      </div>
      <div>

        <ul class="breadcrumb">
          <li><i class="fa fa-home fa-lg"></i></li>
          <li><a href="#">Dashboard</a></li>
        </ul>
      </div>
    </div>
    <div id="toastrmessage"></div>
    <div class="row mt-20">
        <div class="col-md-6">
            <div class="card" style="height: 278px;">
                <h3 class="card-title">Missing Logs / Leaves</h3>
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
              <div class="card-title-w-btn">
                <h3 class="title">Leave</h3>
                <p><a class="btn btn-primary" href="" data-toggle="modal" data-target="#leave-details">Leave Details</a>&nbsp;<a class="btn btn-primary " href="javascript:;" data-toggle="modal" data-target="#leave-request">Leave Request</a></p>
              </div>
              <!-- leave-details Modal-->
                <div class="modal fade" id="leave-details" role="dialog" tabindex="-1" aria-labelledby="leave-detailsLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="card">
                        <div class="card-title-w-btn">
                          <h3 class="title">leaves</h3>
                          <p>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          </p>
                        </div>
                        <div class="card-body">
                          <div class="bs-component">
                            <div class="list-group">
                                @foreach($total_leaves as $key => $leaves)
                                <a class="list-group-item">
                                <h4 class="list-group-item-heading date_leavetype"> <?= $leaves['date']?> (<?= $leaves['leave_type']?>)</h4>
                                <p class="list-group-item-text reason_leave"><?= $leaves['leave_purpose'];?></p>
                                <p class="list-group-item-text haft-full-day"><strong><?= $leaves['leave_time']?></strong></p></a>
                                @endforeach
                               </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              <!-- leave-request Modal-->
                <div class="modal fade" id="leave-request" role="dialog" tabindex="-1" aria-labelledby="leave-requestLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="card">
                        <div class="card-title-w-btn">
                          <h3 class="title">leave Request</h3>
                          <p>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          </p>
                        </div>
                        <div class="card-body">
                          <div class="bs-component well">
                            <ul class="nav nav-tabs activation-tabs">
                              <li class="active" id="previoustab"><a href="javascript:;" data-toggle="tab">Step 1</a></li>
                              <li class="step2" id="middletab"><a href="javascript:;" data-toggle="tab">Step 2</a></li>
                              <li class="step3" id="nexttab"><a href="javascript:;" data-toggle="tab">Step 3</a></li>
                            </ul>
                            <div class="tab-content mt-20" id="myTabContent">
                              <div class="tab-pane fade active in" id="step1">
                               
                                <?= Form::open(array('url' => route('dashboard.leaves.store') ,'class' => 'form-horizontal ajaxformsubmit','id'=>'firstajaxform','name'=>'firstajaxform')) ?>
                                 
                                  <div class="form-group" id="datesingle">
                                    <label class="col-lg-3 control-label" for="leaveselectdate">Select Date</label>
                                    <div class="col-lg-9">
                                      <?= Form::text('date',old('date'),['class'=>'form-control pull-right','id'=>'datepicker','data-date-format'=>'yyyy-mm-dd','placeholder'=>'select date'])?>
                                       <p class="help-block" style="color:red" id="date_error" ></p> 
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-lg-3 control-label" for="leaveselecttype">Select Leave type</label>
                                    <div class="col-lg-9">
                                      <select class="form-control" name="leave_type">
                                        <option value="Casual Leave">Casual Leave</option>
                                        <option value="Sick Leave">Sick Leave</option>
                                        <option value="Privilege Leave">Privilege Leave</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group @if ($errors->has('leave_purpose')) has-error @endif">
                                    <label class="col-lg-3 control-label" for="leave-purpose">leave-purpose</label>
                                    <div class="col-lg-9">
                                      <textarea class="form-control" type="text" value="" placeholder="leave-purpose" name="leave_purpose" rows="4"></textarea>
                                      <p class="help-block" style="color:red" id="error_f_leavepurpose" ></p> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-3">
                                      <button type="submit" class="btn btn-primary">Next</button>
                                    
                                      <!-- <a href="#step2" class="btn btn-primary" data-toggle="tab" id="firstpage">Next</a> -->
                                    </div>
                                  </div>
                                 {!! Form::close() !!}
                              </div>
                              <div class="tab-pane fade" id="step2">
                                <div class="bs-component">
                                  <div class="panel panel-primary" >
                                    <div class="panel-heading leave-app">
                                    <h3 class="panel-title modal-heading collapsed panellink-title" data-toggle="collapse" data-parent="#accordion" href="#collapse1" >Leave Request</h3>
                                      <!-- <button class="close" type="button">&times;</button> -->
                                    </div>
                                    <div class="panel-body collapse" id="collapse1">
                                    <?= Form::open(array('url' => route('dashboard.leaves.second.store') ,'class' => 'form-horizontal secondajaxform','id'=>'secondajaxform','name'=>'secondajaxform')) ?>
                                    <div id="error_msg" style="color: red;"></div>
                                    <div id="append_modal">
                                    </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      
                                      <a href="#step1" class="btn btn-primary" data-toggle="tab" id="tofirst">Previous</a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                       {!! Form::submit('Save & Next',['class'=>'btn btn-primary'])!!}
                                       <!-- <a href="#step3" data-toggle="tab" class="btn btn-primary" id="tothird"></a> -->
                                    </div>

                                  </div>
                                  {!! Form::close() !!}
                                </div>
                              </div>
                           
                              <div class="tab-pane fade" id="step3">
                                <div class="bs-component">
                                  <h3 class="modal-heading" id="message"></h3>
                                  <div class="panel panel-primary" id="step3data">
                                    <div class="panel-heading">
                                      <h3 class="panel-title" id="date-panel-second"></h3>
                                    </div>
                                    <div class="panel-body">
                                      <div id="formwholeday">Whole Day</div>
                                      <div id="hours_data"></div>
                                      <div id="formpurpose">Purpose : hiii</div>
                                      <div id="formleave_type">Casual leave</div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <a href="#step2" class="btn btn-primary" data-toggle="tab" id="tosecond">Previous</a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                      <button class="btn btn-primary" id="submitleave" onclick="confirmLeave()">Confirm</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-leavebox row">
                      <div class="leavesubsection button-section col-xs-5 pl-0"><a class="btn btn-danger" href="javascript:;">
                            Casual</a></div>
                      <div class="leavesubsection leave-used col-xs-3 text-center"><strong class="text-muted">USED</strong><br><span>{{ $casual_leaves}}</span></div>
                      <div class="leavesubsection avail col-xs-3 text-center"><strong class="text-muted">AVAIL</strong><br><span><?= 6-$casual_leaves ?></span></div>
                    </div>
                    <div class="card-leavebox row">
                      <div class="leavesubsection button-section col-xs-5 pl-0"><a class="btn btn-danger" href="javascript:;"> Sick</a></div>
                      <div class="leavesubsection leave-used col-xs-3 text-center"><strong class="text-muted">USED</strong><br><span>{{ $sick_leaves}}</span></div>
                      <div class="leavesubsection avail col-xs-3 text-center"><strong class="text-muted">AVAIL</strong><br><span><?= 6-$sick_leaves ?></span></div>
                    </div>
                    <div class="card-leavebox row">
                      <div class="leavesubsection button-section col-xs-5 pl-0"><a class="btn btn-danger" href="javascript:;">
                            Privilege</a></div>
                      <div class="leavesubsection leave-used col-xs-3 text-center"><strong class="text-muted">USED</strong><br><span>{{ $priviledge_leaves}}</span></div>
                      <div class="leavesubsection avail col-xs-3 text-center"><strong class="text-muted">AVAIL</strong><br><span><?= 6-$priviledge_leaves ?></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(auth()->user()->is_admin)
    <div class="row mt-20">
        <div class="col-md-12">
            <div class="card" style="height: 520px;" id="firstlevel">
                <div class="card-title-w-btn">
                    <h3 class="title" id="graph_title">Leaves</h3>
                    <p><button id="back" type="button" class="btn btn-primary" onclick="back()" style="display: inline-block;">Back</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo1"></canvas>
                </div>
            </div>
            <div class="card" style="height: 520px;" id="secondlevel">
                <div class="card-title-w-btn">
                    <h3 class="title" id="second_graph_title">Leaves</h3>
                    <p><button id="back" type="button" class="btn btn-primary" onclick="back()" style="display: inline-block;">Back Home</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo2"></canvas>
                </div>
            </div>
            <div class="card" style="height: 520px;" id="thirdlevel">
                <div class="card-title-w-btn">
                    <h3 class="title" id="third_graph_title">Leaves</h3>
                    <p><button id="back1" type="button" class="btn btn-primary" onclick="back1()" style="display: inline-block;position: relative;z-index: 1">Back</button><button id="back" type="button" class="btn btn-primary" onclick="back()" style="display: inline-block;position: relative;z-index: 1;margin: 0px 7px;">Back Home</button></p>
                </div>   
                <div class="embed-responsive embed-responsive-16by9" style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;display: block;">
                    <canvas class="embed-responsive-item" id="barChartDemo3"></canvas>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row mt-20">
        
        @if(!empty($birthday_members))
        <div class="col-md-6">
            <div class="card div-height">
              <h3 class="card-title">Upcoming birthday</h3>
              <div class="product-list">
                @foreach($birthday_members as $key => $birthday)
                <div class="product"><img class="img-circle" src="<?= IMG_PATH.'/upload/'.$birthday['profile_pic'];  ?>" height="50" width="50">
                  <div class="item-desc">
                    <h4> <a href="#"><?= $birthday['fullname']?></a></h4>
                    <p><?= $birthday['birthdate'];?></p>
                  </div>
                </div>
                @endforeach
                
              </div>
            </div>
        </div>
      @endif
      
      
      @if(!empty($holiday))
            <div class="col-md-4">
                <div class="card div-height">
                  <h3 class="card-title">Upcoming Holiday</h3>
                  <div class="card-body">
                    <div class="card-holidaysection">
                      <ul class="list-unstyled">
                        @foreach($holiday as $key => $future_holiday)
                        <li><strong><?= $future_holiday['title'];?></strong><span class="date ml-10"> <?= $future_holiday['holiday_date'];?></span></li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
            </div>
        @endif
      <div class="col-md-4">
        <div class="card">
          <h3 class="card-title"><?php echo date("F",strtotime(date('Y-m-d')));echo date('Y') ?> Working Hours</h3>
          <div class="product-list">
              <div style="margin: 0;display:inline-block;font-size: 20px">Attended Hours:</div>
              <div style="margin: 0 0 0 10px;display:inline-block;font-size: 20px;font-weight: bold;">{{$exp_work_hours[0]}}/<small style="color: #333;font-weight: normal;">{{$exp_total_hours[0]}}</small></div><!-- 
               <input type="text" class="form-control" name="working_hours" id="total_work_hours" disabled="" value="{{$exp_work_hours[0]}}"> -->
           <!-- <div class="form-group row">
             <label class="control-label col-md-3">Total Hours:</label>
             <div class="col-lg-9">
               <h2>{{$exp_total_hours[0]}}</h2>
                <input type="text" name="total_hours" class="form-control " id="total_working_hours" disabled=""  value="{{$exp_total_hours[0]}}">
              </div>
           </div>  -->    
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <h3 class="card-title">Attendance</h3>
          <div class="product-list">
            <div class="form-group">
            @if($checkin_empty)
               <input type="button" name="" class="btn btn-success  btn btn-block checkintime" value="Checkout">
            @else
              <input type="button" name="" class="btn btn-success  btn btn-block checkintime" value="Checkin">
            @endif
          </div>
            <div class="form-group row">
             <label class="control-label col-md-3">CheckIn Time:</label>
             <div class="col-lg-9">
               <input type="text" class="form-control  " name="checkin" id="checkin_data" disabled="" value="@if(!empty($checkin_time['check_in'])){{ $checkin_time['check_in'] }}@endif">
             </div>
           </div>
           <div class="form-group row">
             <label class="control-label col-md-3">Chekout Time:</label>
             <div class="col-lg-9">
                <input type="text" name="checkin" class="form-control " id="checkout_data" disabled=""  value="@if(!empty($checkin_time['check_out'])){{ $checkin_time['check_out'] }}@endif">
              </div>
           </div>
          <div class="form-group row">
             <label class="control-label col-md-3">Total Hours:</label>
             <div class="col-lg-9">
                <input type="text" class="form-control " id="total_hours" disabled=""  value="@if(!empty($time_differance[0]['time_difference'])) {{ $time_differance[0]['time_difference']}} @endif">
              </div>
           </div>     
          <div class="form-group row">
             <label class="control-label col-md-3">Total Checkin</label>
             <div class="col-lg-9">
                <input type="text" class="form-control " id="total_time_checkin" disabled=""  value="{{$count_log_row}}">
              </div>
           </div>      
          </div>
        </div>
      </div>
      @if(!empty($pending_leave))
       <div class="col-md-6" >
        <div class="card div-height">
         
          <h3 class="card-title">Pending Leaves</h3>
           <div class="list-group">
                        @foreach($pending_leave as $key => $leaves)
                        <h4 class="list-group-item-heading date_leavetype"> <?= $leaves['date']?> (<?= $leaves['leave_type']?>)</h4>
                        <p class="list-group-item-text reason_leave"><?= $leaves['leave_purpose'];?></p>
                        <p class="list-group-item-text haft-full-day"><strong><?= $leaves['leave_time']?></strong></p>
                        @endforeach
          </div>
        </div>
      </div>
      @endif
      
@stop
@section('style')
    <?= Html::style('asset/css/bootstrap-timepicker.min.css') ?>
    <?= Html::style('asset/css/new_dashboard.css') ?>
    <?= Html::style('asset/css/bootstrap-datepicker.css') ?>
    <?= Html::style('asset/css/teal.css') ?>
    <style type="text/css">
        #radioBtn .notActive{
            color: #3276b1;
            background-color: #fff;
        }
        
    </style>
@stop
@section('script') 
    <?= Html::script('asset/js/bootstrap-timepicker.min.js') ?>  
     <?= Html::script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
    <?= Html::script('asset/js/bootstrap-datepicker.min.js') ?> 
    <?= Html::script('asset/js/jquery.validate.min.js') ?>      
    <?= Html::script('asset/js/Chart.bundle.min.js') ?>     
    <script type="text/javascript">
        //Graph
        var month = [<?= $month ?>];
        var count_logs = [<?= $count_logs?>];
        var count_leaves = [<?= $count_leaves?>];
        var barChartData = {
            labels: month,
            datasets: [{
                label: 'Missing Logs',
                backgroundColor: "<?= config('project.error_code.code')?>",
                data: count_logs
            },
            {
                label: 'Leaves',
                backgroundColor: "#009688",
                data: count_leaves
                }
            ]
        }

        var context = $("#barChartDemo").get(0).getContext("2d");
        $('#secondlevel').hide();
        $('#thirdlevel').hide();
        var myBar = new Chart(context, {
            type: 'bar',

            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: ""
                },
                tooltips: {
                    mode: 'label'
                },
                scales: {
                    yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepSize:1,
                                labelString: 'text'
                            }
                        }]
                },
                responsive: true,
            }
        });
        @if(auth()->user()->is_admin)
        
        //all user missing logs
        var leaves_count_logs = [<?= $leaves_count_logs ?>];
        var username = [<?= $username?>];
        var fullnames = <?= json_encode($fullname)?>;
        $('#back').hide();
        $('#back1').hide();
        
        function back(){
            location.reload();
            $('#back').hide();
            $('#graph_title').text('Leaves');
        }
        //chart1
        var barChartData1 = {
            labels: username,
            datasets: [{
                label: 'Leaves',
                backgroundColor: "<?= config('project.error_code.code')?>",
                data: leaves_count_logs,
                
            }
            ]
        }
        var context = $("#barChartDemo1").get(0).getContext("2d");

        var myBar = new Chart(context, {
            type: 'bar',

            data: barChartData1,
            options: {
                title: {
                    display: true,
                    text: ""
                },
                tooltips: {
                    enabled:true,
                    mode: 'label',
                    callbacks: {
                    title: function(fullname, data) {
                        var index = fullname[0].index;
                        return fullnames[index];
                    },
                }

                },
                scales: {
                    yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepSize:5,
                                labelString: 'text',
                            },   
                        }],
                        xAxes: [{
                            ticks: {
                                callback: function(value) {
                                    return value.substr(0, 2);//truncate
                                },
                            }
                        }],
                },
                responsive: true,
            }
        });

        //chart2
        var canvas = document.getElementById("barChartDemo1");
        var chartdepth = 0;
        var second_myBar;
        var barChartData2;
        canvas.onclick = function(evt) {
            var activePoints = myBar.getElementsAtEvent(evt);
            
            if(activePoints.length != 0){
                $('#back').show();
                var chartData = activePoints[0]['_chart'].config.data;
                var idx = activePoints[0]['_index'];
                var label = chartData.labels[idx];
                var number = label.match(/\d+/);
                if(number.index != 0){  
                    $('#secondlevel').show();
                    $('#firstlevel').hide();
                    sessionStorage.setItem("userid",number[0]);
                    $.ajax({
                        type: "POST",
                        url: '<?= URL::route('dashboard.leaves.chart') ?>',
                        data: { "_token" :'<?=csrf_token()?>','id': number[0]},

                        success: function(data) {
                            var count_leaves = [data.count_leaves];
                            var year = [data.year];
                            barChartData2 = {
                                labels: '',
                                datasets: [{
                                    label: 'Leaves',
                                    backgroundColor: "<?= config('project.error_code.code')?>",
                                    data: '',
                                    
                                }
                                ]
                            }

                            barChartData2.datasets[0].data = count_leaves[0];
                            barChartData2.labels = year[0];
                            var context1 = $("#barChartDemo2").get(0).getContext("2d");

                            second_myBar = new Chart(context1, {
                                type: 'bar',

                                data: barChartData2,
                                options: {
                                    title: {
                                        display: true,
                                        text: ""
                                    },
                                    tooltips: {
                                        enabled:true,
                                        mode: 'label',

                                    },
                                    scales: {
                                        yAxes: [{
                                                display: true,
                                                ticks: {
                                                    beginAtZero: true,
                                                    steps: 1,
                                                    stepSize:1,
                                                    labelString: 'text',
                                                },   
                                            }],
                                            xAxes: [{
                                                ticks: {
                                                    callback: function(value) {
                                                        return value;//truncate
                                                    },
                                                }
                                            }],
                                    },
                                    responsive: true,
                                }
                            });
                            if(data.fullname != null){
                                $('#second_graph_title').text('Leaves ' + data.fullname);
                            }
                        }
                    });
                }
            }
        }

        function back1(){
          $('#thirdlevel').hide();
          $('#firstlevel').hide();
          $('#secondlevel').show();
        }

        //chart 3
        var canvas1 = document.getElementById("barChartDemo2");
        var chartdepth = 0;
        canvas1.onclick = function(evt) {

            var activePoints = second_myBar.getElementsAtEvent(evt);
            // var year = evt;
            if(activePoints.length != 0){
              var year = activePoints[0]['_model']['label'];
            // }
            // sessionStorage.setItem("current_year",year);
            // year = sessionStorage.getItem("current_year");
            // console.log(activePoints);
                $('#back1').show();
                $('#back').show();
                $('#thirdlevel').show();
                $('#firstlevel').hide();
                $('#secondlevel').hide();
                // var year = activePoints[0]['_model']['label'];
                var user_id = sessionStorage.getItem("userid");
                $.ajax({
                    type: "POST",
                    url: '<?= URL::route('dashboard.leaves.secondchart') ?>',
                    data: { "_token" :'<?=csrf_token()?>','id': user_id,'year':year},

                    success: function(data) {
                      var count_leaves = [data.count_leaves];
                      var month = [data.month];
                      var fullname = data.fullname;
                      var barChartData3 = {
                          labels: '',
                          datasets: [{
                              label: 'Leaves',
                              backgroundColor: "<?= config('project.error_code.code')?>",
                              data: '',
                              
                          }
                          ]
                      }

                      barChartData3.datasets[0].data = count_leaves[0];
                      barChartData3.labels = month[0];
                      var context2 = $("#barChartDemo3").get(0).getContext("2d");

                      third_myBar = new Chart(context2, {
                          type: 'bar',

                          data: barChartData3,
                          options: {
                              title: {
                                  display: true,
                                  text: ""
                              },
                              tooltips: {
                                  enabled:true,
                                  mode: 'label',

                              },
                              scales: {
                                  yAxes: [{
                                          display: true,
                                          ticks: {
                                              beginAtZero: true,
                                              steps: 1,
                                              stepSize:1,
                                              labelString: 'text',
                                          },   
                                      }],
                                      xAxes: [{
                                          ticks: {
                                              callback: function(value) {
                                                  return value;//truncate
                                              },
                                          }
                                      }],
                              },
                              responsive: true,
                          }
                      });

                      third_myBar.destroy();
                        $('#third_graph_title').text('Leaves ' + fullname +'-'+ year);
                    }
                });
            }
        }
         @endif
        var token = "<?= csrf_token()?>";
        $('#firstajaxform').on('submit', function(event) {
            if ($('input[name=date]').val() == "") {
                $('#date_error').text('Please Select Date');
                return false;
            } else {
                $('#date_error').text('');

            }
            if ($('textarea[name=leave_purpose]').val() == "") {
                $('#error_f_leavepurpose').text("plese fill leave purpose field");
                return false;
            } else {
                $('#error_f_leavepurpose').text('');

            }
            event.preventDefault();


            var formData = {
                date: $('input[name=date]').val(),
                leave_type: $('select[name=leave_type]').val(),
                leave_purpose: $('textarea[name=leave_purpose').val(),

            }
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: {
                    formData: formData,
                    _token: token
                },
                cache: false,

                success: function(data) {

                    $('#append_modal').empty();

                    $('#append_modal').html(data.html_content);
                    $('#step3data').empty();
                    $.each(data.date_of_leave, function(k, v) {
                        $('#step3data').append(
                            "<div class='panel-heading'>" +
                            "<h3 class='panel-title' id='date-panel-second'>" + v + "</h3>" +
                            "</div>" +
                            "<div class='panel-body'>" +
                            "<div id='formwholeday'>Whole Day</div>" +
                            "<div id='hours_data'></div>" +
                            "<div id='formpurpose'>" + data.firststep_store.leave_purpose + "</div>" +
                            "<div id='formleave_type'>" + data.firststep_store.leave_type + "</div>"
                        );

                    });
                    $('#previoustab a').attr('aria-expanded', false);
                    $('#previoustab').attr('class', '');
                    $('#middletab a').attr('aria-expanded', true);
                    $('#middletab').addClass('active');
                    $('#step1').removeClass('active in');
                    $('#step2').addClass('active in');
                },
                error: function(errors) {
                    console.log('hi');
                }
            });
        });

        $('.secondajaxform').on('submit', function(event) {
            event.preventDefault();
            $.ajax({

                type: "POST",
                url: '<?= URL::route('dashboard.leaves.second.store') ?>',
                data: $(this).serialize(),
                dataType: 'json',

                success: function(data) {



                    if ($.isEmptyObject(data.error)) {
                        $('#middletab a').attr('aria-expanded', false);
                        $('#middletab').attr('class', '');
                        $('#nexttab a').attr('aria-expanded', true);
                        $('#nexttab').addClass('active');
                        $('#step2').removeClass('active in');
                        $('#step3').addClass('active in');

                        $('#step3data').empty();
                        $.each(data.leave_detail, function(k, v) {
                        
                            if (v.from_hour == "10:00 AM" && v.to_hour == "07:00 PM") {
                    
                                $('#step3data').append(
                                    "<div class='panel-heading'>" +
                                    "<h3 class='panel-title' id='date-panel-second'>" + k + "</h3>" +
                                    "</div>" +
                                    "<div class='panel-body'>" +
                                    "<div id='formwholeday'>Whole Day</div>" +
                                    "<div id='formpurpose'>" + v.leave_purpose + "</div>" +
                                    "<div id='formleave_type'>" + v.leave_type + "</div>"
                                );

                            } else {
                        
                                $('#step3data').append(
                                    "<div class='panel-heading'>" +
                                    "<h3 class='panel-title' id='date-panel-second'>" + k + "</h3>" +
                                    "</div>" +
                                    "<div class='panel-body'>" +
                                    "<div id='formwholeday'>Half Day</div>" +

                                    "<div id='hours_data'>" + v.from_hour + "-" + v.to_hour + "</div>" +

                                    "<div id='formpurpose'>" + v.leave_purpose + "</div>" +
                                    "<div id='formleave_type'>" + v.leave_type + "</div>"
                                );

                            }

                        });
                    } else {
                        printErrorMsg(data.error);
                    }

                }



            });
        });

        function printErrorMsg(msg) {
            $('#error_msg').empty();
            $.each(msg, function(key, value) {
                $('#error_msg').append('<li>' + value + '</li>');
            });
        }
        $('#tofirst').on('click', function() {
            $('#previoustab').addClass('active');
            $('#middletab').attr('class', '');

        });
        $('#tosecond').on('click', function() {
            $('#middletab').addClass('active');
            $('#nexttab').attr('class', '');

        });
    </script>
    <script type="text/javascript">
        function confirmLeave() {
            $.ajax({
                type: "POST",
                url: "<?=URL::route('dashboard.leaves.third.store')?>",
                data: {
                    _token: token
                },
                cache: false,

                success: function(data) {

                    // message
                    $('#message').addClass('modaldate');
                    $("#message").html("Your leave request sent successfully");
                    $('#tosecond').attr('disabled', 'disabled');
                    $('#submitleave').attr('disabled', 'disabled');

                    window.setTimeout(function() {
                        location.reload()
                    }, 3000)
                }
            });
        }

        $(document).ready(function() {
            $(document).on('click', '.timepicker1', function() {
                $(this).timepicker();
            });
            $(document).on('click', '.timepicker2', function() {
                $(this).timepicker();
            });

            $(document).on('change', 'input:checkbox[class=fulldaycheckboxclass]:checked', function() {
                var hide = $(this).parents('.parentdate').children().closest('#hour').hide();
                $(this).attr('value', 'fullday');
            });
            $(document).on('change', 'input:checkbox[class=fulldaycheckboxclass]:unchecked', function() {
                var show = $(this).parents('.parentdate').children().closest('#hour').show();
                $(this).attr('value', 'halfday');
            });
            var selected_date = new Date();
            $('#datepicker').datepicker({
                beforeShowDay: function(date) {
                    var day = date.getDay();
                    if (day == 0) {
                        return false;
                    } else {
                        return true;
                    }
                },
                multidate: true,
                startDate: selected_date,
                format: 'dd-mm-yyyy'
            });
        });
    </script>
    <script type="text/javascript">
        var token = "<?= csrf_token()?>";
        $('.checkintime').click(function() {


            $.ajax({
                url: "<?= URL::route('user.checkin') ?>",
                type: "post",
                data: {
                    _token: token
                },
                dataType: 'JSON',

                success: function(resp) {
                    
                    if (resp.check_in) {
                        if (resp.checkout_time_count) {
                            $('#total_hours').attr('value', resp.checkout_time_count[0].time_difference)
                        }
                        $('.checkintime').attr('value', 'Checkout');
                        $('#checkin_data').attr('value', resp.check_in.check_in);
                        $('#total_time_checkin').attr('value', resp.count_row);

                    } else if (resp.check_out) {
                        $('#total_work_hours').attr('value',resp.exp_work_hours[0]);
                        $('#total_hours').attr('value', resp.checkout_time_count[0].time_difference);
                        $('.checkintime').attr('value', 'CheckIn');
                        $('#checkout_data').attr('value', resp.check_out.check_out)
                        $('#total_time_checkin').attr('value', resp.count_row);
                    }
                }
            });
        });
    </script>
    @stop 