
@foreach($date_of_leave as $key => $form)

<div class="parentdate">

    {{csrf_field()}}
    <h3 class="modal-heading modaldate">{{ $form}}  {{ date('l',strtotime($form))}}</h3>
    <input type="hidden" value="{{$form}}" name="hidden_field">
    <div class="form-group">
      <div class="animated-checkbox col-md-12">
        <label>
          <input type="checkbox" id="fulldaycheckbox" name="leave_detail[{{$form}}][day_type]" value="fullday" class="fulldaycheckboxclass" checked="checked" ><span class="label-text">Whole day</span>
        </label>
      </div>
    </div>
    <div class="form-group hours" id="hour" style="display: none;">
     
        <label class="control-label col-lg-3" for="leaveselecttype">Hours:</label>
      <div class="col-md-9">
          <div class="row">
            <div class="input-group col-md-6 bootstrap-timepicker timepicker">
              <input class="form-control timepicker1"  placeholder="From hour" name="leave_detail[{{$form}}][from_hour]" value="10:00 AM" type="text">
            </div>
            <div class="input-group col-md-6 bootstrap-timepicker timepicker">
              <input class="form-control timepicker2" placeholder="To hour" name="leave_detail[{{$form}}][to_hour]" value="07:00 PM" type="text">
            </div> 
          </div>
      </div>
     
      <p class="help-block" style="color:red" id="error_hours"></p>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label" for="leaveselecttype">Select Leave type</label>
      <div class="col-lg-9">
        <select class="form-control" name="leave_detail[{{$form}}][leave_type]" id="field_leave_detail" >
          <option value="Casual Leave" id="c_leave" @if($get_leavetype == 'Casual Leave') selected="selected" @endif >Casual Leave</option>
          <option value="Sick Leave" class="s_leave" @if($get_leavetype == 'Sick Leave') selected="selected" @endif >Sick Leave</option>
          <option value="Privilege Leave" class="p_leave" @if($get_leavetype == 'Privilege Leave') selected="selected" @endif>Privilege Leave</option>
        </select>
        <p class="help-block" style="color:red" id="error_leave_detail{{$form}}"></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label" for="leave-purpose">leave-purpose</label>
      <div class="col-lg-9">
        <textarea class="form-control leave_pupose_form2" name="leave_detail[{{$form}}][leave_purpose]" type="text" placeholder="leave-purpose" rows="4">{{$get_leavepurpose}}</textarea>
        <p class="help-block" style="color:red" id="error_s_leavepurpose"></p>
      </div>
    </div>
</div>     
@endforeach
