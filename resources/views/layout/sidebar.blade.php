<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <?php if(Auth::user()->profile_pic == NULL AND Auth::user()->profile_pic == '') { ?>
                    <img src="/images/user.png">
                <?php } else { ?>
                    <img src="/upload/<?=Auth::user()->profile_pic ?>" alt="User Image" class="img-circle">
                <?php } ?>
            </div>
            <div class="pull-left info">
                <p><?= Auth::user()->fullname ?></p>
                <p class="designation"><?= Auth::user()->position ?></p>
            </div>
        </div>
        <!-- Sidebar Menu-->
        <ul class="sidebar-menu">
            <li class="@if( 'dashboard' == Request::segment(1)) active @endif" ><a href="<?= url('dashboard')?>"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>

            <li class="@if( 'profile' == Request::segment(1)) active @endif" ><a href="<?=URL::route('show.user')?>"><i class="fa fa-user"></i><span>Profile</span></a></li>
            
            @if(CheckPermission::isPermitted('user.index'))
                <li class="@if( 'user' == Request::segment(1)) active @endif"><a href="<?= URL::route('user.index',['type'=>'currentemp']) ?>"><i class="fa fa-user"></i><span>User</span></a></li>
            @endif

            @if(CheckPermission::isPermitted('leaves.index'))

                <li class="@if( 'leaves' == Request::segment(1)) active @endif"><a href="<?= URL::route('leaves.index') ?>"><i class="fa fa-calendar-minus-o"></i><span>Leaves</span></a></li>
            @endif
            @if(CheckPermission::isPermitted('logs.index'))
            <li class="@if( 'logs' == Request::segment(1)) active @endif"><a href="<?= URL::route('logs.index') ?>"><i class="fa fa-clock-o"></i><span>Logs</span></a></li>
            @endif
            @if(auth()->user()->is_admin)
            <li class="@if( 'weeklylogs' == Request::segment(1)) active @endif"><a href="<?= URL::route('admin.weeklylogs') ?>"><i class="fa fa-clock-o"></i><span>Weekly Logs</span></a></li>
            @endif
            @if(auth()->user()->is_admin)
                <li class="treeview @if('holiday-year' == Request::segment(1) || 'holiday-master' == Request::segment(1)) active @endif"><a href="<?= URL::route('holiday-year.index') ?>"><i class="fa fa-plane"></i><span>Holidays</span><i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu" @if('holiday-year' == Request::segment(1) || 'holiday-master' == Request::segment(1)) style="display: block;" @else style="display: none;" @endif>
                        <li class="@if('holiday-year' == Request::segment(1)) active @endif" style="margin-bottom: 5px;margin-left: 21px;"><a href="<?= URL::route('holiday-year.index') ?>"><i class="fa fa-futbol-o"></i><span>Holiday</span></a></li> 
                        <li class="@if('holiday-master' == Request::segment(1)) active @endif" style="margin-left:21px;"><a href="<?= URL::route('holiday-master.index') ?>"><i class="fa fa-bus"></i><span>Holiday Master</span></a></li> 
                    </ul>
                </li>

                <li class="treeview @if( 'assets' == Request::segment(1)) active @endif"><a href=""><i class="fa fa-desktop"></i><span>Assets</span><i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu" @if( 'assets' == Request::segment(1)) style="display: block;" @else style="display: none;" @endif>
                        <li class="@if( 'assets' == Request::segment(1) && 'master' != Request::segment(2)) active @endif" style="margin-bottom: 5px;margin-left: 21px;"><a href="<?= URL::route('assets.index') ?>"><i class="fa fa-laptop"></i><span>Assets</span></a></li>
                        <li class="@if( 'assets' == Request::segment(1) && 'master' == Request::segment(2)) active @endif" style="margin-left: 21px;"><a href="<?= URL::route('assets.master.index') ?>"><i class="fa fa-keyboard-o"></i><span>Assets Master</span></a></li>
                    </ul>
                </li>
            @endif
            <!-- @if(CheckPermission::isPermitted('holiday.index'))
                <li class="@if( 'holiday' == Request::segment(1)) active @endif"><a href="<?= URL::route('holiday.index') ?>"><i class="fa fa-plane"></i>Holidays</a></li>
            @endif -->
            
            @if(auth()->user()->is_admin)
                <li><a href="<?= URL::route('admin.workingdays') ?>"><i class="fa fa-calendar-check-o"></i><span>Working Days</span></a></li>
            @endif
            @if(auth()->user()->is_admin)
                <li><a href="<?= URL::route('services.index') ?>"><i class="fa fa-briefcase"></i><span>Services</span></a></li>
            @endif
            

            @if(auth()->user()->is_admin)
                <li class="@if( 'userpermission' == Request::segment(1)) active @endif"><a href="<?= URL::route('userpermission.create') ?>"><i class="fa fa-lock"></i><span> User Permission</span></a></li>
            @endif
            @if(CheckPermission::isPermitted('quiz.index'))
                <li class="@if( 'quiz' == Request::segment(1)) active @endif"><a href="<?= URL::route('quiz.index') ?>"><i class="fa fa-question-circle"></i> <span>Quiz</span></a></li>
            @endif
            <li><a href="\logout"><i class="fa fa-sign-out fa-lg"></i><span>Logout</span></a></li>
        </ul>
    </section>
</aside>
