<header class="main-header">
    <a href="#" class="logo">thinkTANKER</a>
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button--><a href="#" data-toggle="offcanvas" class="sidebar-toggle"></a>
        <!-- Navbar Right Menu-->
        <div class="navbar-custom-menu">
            <ul class="top-nav">
                <li class="dropdown notification-menu countsection"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">@if($notifications['notification_count'])
                <span class="count-section">{{$notifications['notification_count']}}</span>
                @endif
                <i class="fa fa-bell-o fa-lg"></i></a>
                    <ul class="dropdown-menu" style="max-height: 400px;overflow-y: auto;min-width: 280px;">
                        @if($notifications['notification_count'])
                            <li class="not-head">You have {{$notifications['notification_count']}} new notifications.
                            </li>
                        @else
                        <li class="not-head">
                            @if($notifications['notification_view_count'])
                                You have {{$notifications['notification_view_count']}} notifications.
                            @else
                                You have no notifications.
                            @endif
                        </li>
                        @endif
                        @foreach($notifications['notification_view'] as $key=>$value)
                            <li onclick="view({{$value['id']}})"><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                              <div class="media-body"><span class="block"><?= $value['title']?></span></div></a>
                            </li>
                        @endforeach    
                      <!-- <li class="not-footer"><a href="#">See all notifications.</a></li> -->
                    </ul>
                </li>
                <!--Notification Menu-->
                <!--  -->
                <!-- User Menu-->
                <!-- <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-user fa-lg"></i></a>
                    <ul class="dropdown-menu settings-menu">
                       <li><a href="<?=URL::route('show.user')?>"><i class="fa fa-user fa-lg"></i> Profile</a></li> 
                       <li><a href="\logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                    </ul>
                </li> -->
            </ul>
        </div>
    </nav>
</header>
