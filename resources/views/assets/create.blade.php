@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Assets</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>create</li>
        </ul>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div>
<div class="clearix"></div>
<div class="col-md-12">
    <div class="card">
        <h3 class="card-title">Create</h3>
        <div class="card-body">
            @include('partials.alert')
            <?= Form::open(array('url' => route('assets.store') ,'class' => 'form-horizontal')) ?>
            {{ session('msg') }}
            <input type="hidden" name="main_title">
            <div class="form-group">
                <label class="control-label col-sm-2" for="title Name">Title</label>
                <div class="col-sm-8">
                    <?= Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => 'Enter Title']); ?>
                    <?= $errors->first('title',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="device_type Name">Device Type</label>
                <div class="col-sm-8">
                     <?=Form::select('device_type', ['' => 'Select Device type'] + Config::get('device_type'), old('device_type'), ['class' => 'form-control select2_1 iconSelect'])?>
                    <?= $errors->first('device_type',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="serial_number Name">Serial Number</label>
                <div class="col-sm-8">
                    <?= Form::text('serial_number', old('serial_number'), ['class' => 'form-control', 'placeholder' => 'Enter Serial Number']); ?>
                    <?= $errors->first('serial_number',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Notes</label>
                <div class="col-sm-8">
                    <?= Form::textarea('notes', old('notes'), ['class' => 'form-control', 'placeholder' => 'Enter Description']); ?>
                    <?= $errors->first('notes',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
          
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-primary submit" id="submit" name="save_button" value="save_new">Save & New</button>
                    <button type="submit" class="btn btn-primary submit" id="submit1" name="save_button" value="save_exit">Save & Exit</button>
                    <a href="<?= URL::route('assets.index') ?>" class="btn btn-white btn-default"> Cancel</a>
                    
                    
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
@section('style')
<?= Html::style('asset/css/select2.min.css') ?>
@stop
@section('script')
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<?= Html::script('asset/js/select2.min.js') ?>
<!-- <script>
    jQuery(document).ready(function() {
        $('form').submit(function(){
            $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin" value="Please Wait"></i>').prop('disabled', true);
        });

    });
</script> -->
@include('partials.alert')
@stop


