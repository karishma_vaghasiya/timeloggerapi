@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Holidays</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>Update</li>
        </ul>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div>
<div class="clearix"></div>
<div class="col-md-12">
    <div class="card">
        <h3 class="card-title">Update</h3>
        <div class="card-body">
            @include('partials.alert')
            <?= Form::model($holidays,['route'=>['holiday.update','id'=>$holidays->id], 'class' => 'form-horizontal','method'=>'put']) ?>
            {{ session('msg') }}
            <div class="form-group">
                <label class="control-label col-md-3" for="inputFull Name">Day</label>
                <div class="col-sm-8">
                    <?= Form::text('day', old('day'), ['class' => 'form-control', 'placeholder' => 'DayName','id'=>'day','readonly'=>true]); ?>
                    <?= $errors->first('day',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="inputFull Name">Date</label>
                <div class="col-sm-8">
                    <?= Form::text('date',$holidays['date'], ['class' => 'form-control', 'placeholder' => 'Date','id'=>'date1']); ?>
                    <?= $errors->first('date',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="inputFull Name">Holiday</label>
                <div class="col-sm-8">
                    <?= Form::text('holiday', null, ['class' => 'form-control', 'placeholder' => 'Holiday']); ?>
                    <?= $errors->first('holiday',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">Update</button>
                    
                    <a href="<?= URL::route('holiday.index') ?>" class="btn btn-cyan btn-default"> Cancel</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('style')
<?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
@stop
@section('script')
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
<script>
    jQuery(document).ready(function() {
        $('#date1').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
        });
        var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        $('#date1').change(function(){
            var olddate = $(this).val();
            var dates = olddate.split("-").reverse().join("-");
            var newdate = new Date(dates);
            var dayname = weekday[newdate.getDay()];
            $('#day').val(dayname);

        });
        $('form').submit(function(){
            $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
        });

    });
</script>
@include('partials.alert')
@stop