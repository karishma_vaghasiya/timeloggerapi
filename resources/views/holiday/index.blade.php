@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Holidays</h1>

    </div>
    <div>
        @if(CheckPermission::isPermitted('holiday.create'))
            <a href="<?= URL::route('holiday.create') ?>"class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a>
        @endif

       <!--  <a href="<?=URL::route('holiday.create')?>" class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a> -->
        <!-- <a href="#" class="btn btn-info btn-flat"><i class="fa fa-lg fa-refresh"></i></a>
        <a id="delete" href="javascript:void(0)" class="btn btn-warning btn-flat"><i class="fa fa-lg fa-trash"></i></a> -->
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
    @endforeach
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="employee" class="table table-hover table-bordered" border="1 px">
                        <thead>
                            <tr>
                                <th>Day</th>
                                <th>Date</th>
                                <th>Holiday</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')

<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<script>
var token = "<?= csrf_token() ?>";

$(document).ready(function(){

    $('#employee').DataTable({

        "bProcessing" : true,
        "bServerSide" : true,
        "ajax"        :"<?= URL::route('holiday.index') ?>",
        "aaSorting": [
                       [1, "asc"]
                    ],
        "bPaginate": false,
        "aoColumns" : [
            { mData: 'day' },
            { mData: 'date' },
            { mData: 'holiday' },
            { mData: null,
                bSortable : false,
                mRender:function(v,t,o) {

                    var path = "<?= URL::route('holiday.edit',array('id'=>':id')) ?>";
                    var path_del = "<?= URL::route('holidays.delete',array('id'=>':id')) ?>";

                    path     = path.replace(':id',o['id']);
                    path_del = path_del.replace(':id',o['id']);

                    var extra_html  =   "<div class='btn-group pr5'>"
                                    +       "@if(CheckPermission::isPermitted('holiday.edit')) <a title='Edit' href='"+path+"'><i class='fa fa-edit'></i>|</a> @endif"
                                    +       "@if(CheckPermission::isPermitted('holidays.destroy'))<a id='delete' href='"+path_del+"' class='fa fa-fw fa-trash-o'></a>  @endif"
                                    +   "</div>";
                    return extra_html;


                }

            }
        ],
    });

    $.ajaxSetup({
        statusCode: {
          401: function() {
            location.reload();
          }
        }
    });

});
</script>
@stop
