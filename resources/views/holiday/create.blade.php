@extends('layout.layout')
@section('content')
<div class="page-title">
    <div>
        <h1>Holidays</h1>
        <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li>create</li>
        </ul>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div>
<div class="clearix"></div>
<div class="col-md-12">
    <div class="card">
        <h3 class="card-title">Create</h3>
        <div class="card-body">
            @include('partials.alert')
            <?= Form::open(array('url' => route('holiday.store') ,'class' => 'form-horizontal' ,'files' => true)) ?>
            {{ session('msg') }}
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Day</label>
                <div class="col-sm-8">
                    <?= Form::text('day', old('day'), ['class' => 'form-control', 'placeholder' => 'DayName','id'=>'day','readonly'=>true]); ?>
                    <?= $errors->first('day',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Date</label>
                <div class="col-sm-8">
                    <?= Form::text('date', null, ['class' => 'form-control', 'placeholder' => 'Date','id'=>'date1']); ?>
                    <?= $errors->first('date',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputFull Name">Holiday</label>
                <div class="col-sm-8">
                    <?= Form::text('holiday', null, ['class' => 'form-control', 'placeholder' => 'Holiday']); ?>
                    <?= $errors->first('holiday',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="<?= URL::route('holiday.index') ?>" class="btn btn-white btn-default"> Back</a>
                    
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('style')
<?= Html::style('asset/css/bootstrap-datepicker.css') ?> 
@stop
@section('script')
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        $('#date1').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
        });
        var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        $('#date1').change(function(){
            var olddate = $(this).val();
            var dates = olddate.split("-").reverse().join("-");
            var newdate = new Date(dates);
            var dayname = weekday[newdate.getDay()];
            $('#day').val(dayname);
        });
        $('form').submit(function(){
            $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin" value="Please Wait"></i>').prop('disabled', true);
        });

    });
</script>
@include('partials.alert')
@stop

