<!doctype html>
<html>
<head>
    <title>Email-Box</title>
    <style type="text/css">
        /* all css reset here */
        body,div,aside,span,p,h1,h2,h3,h4,h5,h6{margin:0;padding:0;}
        nav{padding:10px;}
        .container{padding:0 !important;}
        .navbar{margin:0 !important;border:none !important;}
        .navbar-default{background-color:#222 !important;background-image: none !important;}
        .navbar-brand {color:#fff !important;font-weight:bold;font-size:20px !important;margin:0 !important;}
        .contant {
            background-color: #f1c633;
            float: left;
            min-height: 100%;
            position: absolute;
            top: 0;
            width: 100%;
            padding:10px;
        }
        @media screen only and (max-width:1169px;) {
            .container{width:95% !important;}
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
    <div class="navbar-header page-scroll">
        <a class="navbar-brand page-scroll" href="#">thinkTANKER</a>
    </div>
</div>
</nav>

<!-- contant start here -->
<div class="contant">
    <div class="container">
        <span style="float:left;width:100%;color:#666;margin-top:100px;text-align:left;font-weight:bold;font-family: 'Montserrat', sans-serif;">Dear, Sir/medam</span>
        <p style="float:left;color:#666;font-size:14px;margin:10px 0;text-align:left;font-family: 'Montserrat', sans-serif;">Please follow the link to verify your email address.</p>
        <a class="btn btn-success text-uppercase" href="{{ url('register/verify/' . $confirm_code) }}" style="font-family: 'Montserrat', sans-serif;">verify</a>
    </div>
</div>
</body>
</html>
