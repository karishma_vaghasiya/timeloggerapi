@extends('layout.layout')
@section('content')
<div class="page-title">
	<div>
    	<h1>{{ ucwords($name['fullname'])}} User Logs </h1>
      	<ul class="breadcrumb side">
        	<li><i class="fa fa-home fa-lg"></i></li>
        	<li class="active"><a href="<?= URL::route('show.logs',['id'=>':id']) ?>">User Logs</a></li>
            <li>
            <?= Form::selectMonth('month',date('m'),['id'=>'month','class' => 'form-control breadcrumb-select input-sm']); ?>
            <?= Form::selectRange('year','2016','2025',date('Y'),['id'=>'year','class' => 'form-control breadcrumb-select input-sm']); ?>
            <input type="hidden" name="month" value="<?= date('m')?>">
            </li>
        </ul>
    </div>
    <div>
        <a href="<?= url('logs/alluserexportlogs/'.$id.'/'.date("m").'/'.date("Y")) ?>" taget="_self" id="allexportlogs" class="btn btn-primary btn-flat">Export All User Logs
        </a>
        <a href="<?= url('logs/exportlogs/'.$id.'/'.date("m").'/'.date("Y")) ?>" taget="_self" id="exportlogs" class="btn btn-primary btn-flat">Export Logs
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            	<table id="employee" class="table table-hover table-bordered" border="1 px">
               		<thead>
                  		<tr>
		                    <th>Date</th>
		                    <th>TimeIn</th>
                            <th>TimeOut</th>
		                    <th>Hours</th>
		                </tr>
               		</thead>
               	</table>
         	</div>
      	</div>
   	</div>
</div>
@stop

@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/plugins/sweetalert.min.js') ?>
<?= Html::script('asset/js/plugins/select2.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>

<script type="text/javascript">
var token = "<?= csrf_token() ?>";
var path = '<?= URL::route('show.logs',['id'=>':id']) ?>';
path = path.replace(':id','<?= $id ?>');

$( document ).ready(function(){
    $('#employee').DataTable(
    {
        "bProcessing" : true,
        "bServerSide" : true,  
        'bPaginate'   : false,
        "pageLength"  : 50,
        "ajax": {
            'url':path,
            'data':function(d){
                d.month=$('#month').val()
                d.year=$('#year').val();
            }
        },
        "aaSorting"   : [
            [2,"asc"]
        ],
        "aoColumns"   :[
           
            { mData : 'time'},
            { mData : 'check_in'},
            { mData : 'check_out'},
            { mData : 'time_diff'}
        ],
        rowCallback: function( row, data, index ) {
            var  $node = this.api().row(row).nodes().to$();
            if (data.time_diff < '09')
            {
               $node.css('background-color','{{config('project.error_code.code')}}');
            }
        }
    });

    $('#month').change(function(){
        //for export logs
        url = $('#exportlogs').attr('href');
        main_url = url.split('/');
        main_url[6] = ("0" + $('#month').val()).slice(-2);
        month_url = main_url.join('/');
        $('#exportlogs').attr('href',month_url);

        //for all user export logs
        user_url = $('#allexportlogs').attr('href');
        user_main_url = user_url.split('/');
        user_main_url[6] = ("0" + $('#month').val()).slice(-2);
        user_month_url = user_main_url.join('/');
        $('#allexportlogs').attr('href',user_month_url);

        $('.dataTable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    });

    $('#year').change(function(){
        //for export logs
        url = $('#exportlogs').attr('href');
        main_url = url.split('/');
        main_url[7] = $('#year').val();
        year_url = main_url.join('/');
        $('#exportlogs').attr('href',year_url);
        
        //for all user export logs
        user_url = $('#allexportlogs').attr('href');
        user_main_url = user_url.split('/');
        user_main_url[7] = $('#year').val();
        user_year_url = user_main_url.join('/');
        $('#allexportlogs').attr('href',user_year_url);

        $('.dataTable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    });

});
</script>
@stop
