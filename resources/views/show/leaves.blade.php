@extends('layout.layout')
@section('content')
<div class="page-title">
      <div>
        <h1>{{$username['fullname']}} Leaves</h1>
          <ul class="breadcrumb side">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li class="active"><a href="#">Leaves Table</a></li>
          </ul>
      </div>
      <div>
        <a href="<?= route('user.leaves.create',$id)?>" class="btn btn-primary btn-flat"><i class="fa fa-lg fa-plus"></i></a>
    </div>
</div>
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
</div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
              <table id="employee" class="table table-hover table-bordered" border="1 px">
              <thead>
                <tr>
                <th>Date</th>
                <th>Leave Type</th>
                <th>Leave Time</th>
                <th>Leave Purpose</th>
                <th>From Hour</th>
				<th>To Hour</th>
				<th>Status</th>
                <th>Action</th>
                </tr>
              </thead> 
              </table>
              </div>
            </div>
          </div>
        </div>
@stop
@section('script')

<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<script>
var token = "<?= csrf_token() ?>";
var path = '<?= URL::route('show.leaves',['id'=>':id']) ?>';
path = path.replace(':id','<?= $id ?>');

function btn_delete(id){
    //alert('hi');
    $.ajax(
        {
        url : "<?=URL::route('leaves.delete') ?>",
        type : "post",
        datatype : "JSON",
        data : { id: id,_token: token },
        success : function(resp){
            //console.log(resp);
            $('#delete input:checkbox[name="selectall"]').prop('checked', false);
        }
        })
} 
$(document).ready(function()
{
  $('#employee').DataTable(
  {
    "bProcessing" : true,
    "bServerside" : true,
    "ajax"        :{
        'url': path
    },
    "aaSorting": [
                   [2, "asc"]
               ],
    "aoColumns"   :[
        { mData: 'date',sWidth: "10%",},
        { mData: 'leave_type',sWidth: "10%",},
        { mData: 'leave_time',sWidth: "5%",},
        { mData: 'leave_purpose',sWidth: "35%",},
        { mData: 'from_hour',sWidth: "10%",},
        { mData: 'to_hour',sWidth: "10%"},
        {
            "mData": "status",
            sClass: "text-center",
            sWidth: "10%",
            mRender:function(v,t,o)
            {
                if (v == 'a') {
                    return '<span class="badge bg-green">Approve</span>';
                } else if(v == 'd'){
                    return '<span class="badge bg-red">Dis Approve</span>';
                } else {
                    return '<span class="badge bg-yellow">Pending</span>';
                }
            }
        },
        { mData: null,
          bSortable:false,
          sWidth: "10%",
          mRender:function(v,t,o)
          {
            var path     = "<?= URL::route('leaves.edit',array('id'=>':id')) ?>";
              
            path     = path.replace(':id',o['id']);
            
             
            var extra_html = '<a href="'+path+'" class="fa fa-fw fa-edit" style="font-size: 18px"></a>' +
                             '<a id="delete" class="fa fa-fw fa-trash-o" href="javascript:void(0)" style="font-size: 18px" onclick="btn_delete('+o['id']+')"></a>' 
            
            return extra_html;
            }

          },
                
    ],
});
  
    $('#delete').click(function(){
        //alert('hi');
        var deleted_id = $('#employee tbody input[type=checkbox]:checked');

        if(deleted_id.length > 0)   
        {
            var id = [];
                $.each(deleted_id,function(i,ele){
                    id.push($(ele).val());
                });
        }
    });
     $.ajaxSetup({
              statusCode: {
                401: function() {
                  location.reload();
                }
              }
        });      

});

</script>

@stop