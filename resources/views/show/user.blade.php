@extends('layout.layout')
@section('content')
<!-- <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
    </div> -->
<style type="text/css">
    .avtar{
    background-color: #fff;
    padding:20px 0;
    }
    .avtar button.btn-primary {
    float: none;
    width: 80%;
    margin: 20px auto;
    }
    .avtar form {
    float: none;
    width: 90%;
    margin: auto;
    }
    .profile-detail{
    width:100%;
    background-color: #fff;
    padding:20px 0;
    }
    .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
    margin: 10px 0;
    }
    .profile-detail label {
    float: right;
    font-size: 16px;
    margin: 19px 0;
    }
    .profile-detail input {
    float: left;
    width: 98%;
    margin: 10px 0;
    }
    .profile-detail textarea {
    width: 98%;
    }
    .profile-detail button {
    float: left;
    }
    .checkbox {
    float: left;
    width: 98%;
    text-align: left;
    }
    .checkbox label {
    float: left;
    width: auto;
    margin: 0;
    }
    .checkbox input[type="checkbox"] {
    float: left;
    width: auto;
    margin: 4px 0;
    }
    .dl-horizontal.dd-horizontal > dd {
    width: 100%;
    text-align: left;
    }
</style>
<div class="page-title">
    <div>
        <h1> User Profile</h1>
    </div>
</div>
<div class="row">
    @if(Auth::user()->profile_pic != '' AND Auth::user()->profile_pic != NULL)
    <div class="col-md-3 avtar text-center">
        <img src="/upload/<?= Auth::user()->profile_pic ?>" alt="" style="width:150px;height:150px;">
    </div>
    @endif
    <div class="col-md-9">
        <div class="profile-detail">
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Gender:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?=Auth::user()->gender?>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal ">
                        <dt>Name:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?=Auth::user()->fullname?>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Position:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?=Auth::user()->position?>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>BirthDate:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?= date('d/m/Y', strtotime(Auth::user()->birthdate)) ?>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal dd-horizontal">
                        <dt>Email:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?=Auth::user()->email?>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Contact:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal">
                        <?=Auth::user()->contact?>
                    </dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Emergency Contact:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal"><?=Auth::user()->emergency_contact ?></dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <dl class="dl-horizontal">
                        <dt>Address:</dt>
                    </dl>
                </div>
                <div class="col-sm-9">
                    <dd class="dd-horizontal"><?=Auth::user()->address ?></dd>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-3 text-left">
                    <a href="<?= URL::current().'/edit' ?>" class="btn btn-primary">Edit</a> 
                    <a href="<?= URL::route('dashboard.admin') ?>" class="btn btn-white btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop