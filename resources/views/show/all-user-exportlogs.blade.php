<!DOCTYPE html>
<html>
<head>
		<?= Html::style('/main.css')?>
</head>
	<body style="background-color: #fff; margin-left: -1cm;margin-top: 0cm;font-size: 20px;">
		<?php $date_arr=[]; $name_arr=[];?>
		@foreach ($pdf_data as $product=>$value)
			<?php $date_arr[] = $value['Date'];
				$name_arr[]	= $value['fullname'];
			?>
		@endforeach
		<div class="container" style="padding: 0;margin: 0 5px">
		  <div class="table-responsive">         
			  <table class="table table table-bordered">
			    <thead>
			      <tr>
				      <th style="padding: 5px;width: 30px">Name</th>
				      <th style="width: 10px;">In/<br/>Out</th>
				      @foreach (array_unique($date_arr) as $key=>$value)
				      	<?php $dayname = date('D', strtotime($value));
				      	$value = date('d', strtotime($value));?>
				      	<th style="width: 10px;padding-left:3px;padding-right: 3px">{{$dayname}}<br/>{{$value}}</th>
				      @endforeach
				      	<th style="width: 10px;padding-left:3px;padding-right: 3px">Hrs</th>
			      </tr>
			    </thead>
			    <tbody>
				    @foreach (array_unique($name_arr) as $key=>$value)
				    	<tr>
							<th rowspan="3" style="padding: 5px;width: 30px">
							    <?php 
							    $names = explode(" ", $value); 
							    foreach($names as $i){
	    							echo $i . "<br />";
								}?>
							</th>
						    <th style="width: 10px">In</th>
						    <?php $c=0;?>
					    	@foreach ($pdf_data as $pdf_data_product=>$pdf_data_value)
								@if($pdf_data_value['fullname'] == $value)
									<th style="width: 10px;padding-left:3px;padding-right: 3px">{{substr($pdf_data_value['check_in'],0,5)}}</th>
									<?php $c++;?>
								@endif
							@endforeach
							<?php $count=count(array_unique($date_arr))-$c;?>
							@for($i=0;$i<$count;$i++)
								<th></th>
							@endfor
							<th rowspan="3">
								@foreach ($work_hours as $work_hours_key=>$work_hours_value)
									@foreach ($pdf_data as $pdf_data_product=>$pdf_data_value)
										@if($pdf_data_value['fullname'] == $value)
											@if($pdf_data_value['user_id'] == $work_hours_value['user_id'])
												<?php $works_hours = $work_hours_value['work_hour']?>
											@endif
										@endif
									@endforeach
								@endforeach
								<?php $work_hrs = explode(':',$works_hours);
										$hrs = $work_hrs[0].':'.$work_hrs[1];
								?>
								{{$hrs}}
							</th>
				      	</tr>
				      	<tr>
					    	<th style="width: 10px">Out</th>
					    	@foreach ($pdf_data as $pdf_data_product=>$pdf_data_value)
								@if($pdf_data_value['fullname'] == $value)
									<th style="width:10px;padding-left:3px;padding-right: 3px">{{substr($pdf_data_value['check_out'],0,5)}}</th>
								@endif
							@endforeach	
							<?php $count=count(array_unique($date_arr))-$c;?>
							@for($i=0;$i<$count;$i++)
								<th></th>
							@endfor
				      	</tr>
				      	<tr>
					    	<th style="width: 10px">Hours</th>
					    	@foreach ($pdf_data as $pdf_data_product=>$pdf_data_value)

								@if($pdf_data_value['fullname'] == $value)
									<th @if(($pdf_data_value['time_diff']) < '09:00:00')style="background-color:{{config('project.error_code.code')}};width:10px;padding-left:3px;padding-right:3px;" @else style="width:10px;padding-left:3px;padding-right: 3px;"@endif>{{substr($pdf_data_value['time_diff'],0,5)}}</th>
								@endif
							@endforeach	
							<?php $count=count(array_unique($date_arr))-$c;?>
							@for($i=0;$i<$count;$i++)
								<th></th>
							@endfor

				      	</tr>
				    @endforeach
			    </tbody>
			  </table>
		  </div>
		</div>
		
	</body>
	
</html>