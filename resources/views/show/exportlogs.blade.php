<!DOCTYPE html>
<html>
<head>
		<?= Html::style('/main.css')?>
</head>
	<body style="background-color: #fff">
		<div class="container" style="margin-top:-1cm; ">
		  <div style="border: 1px solid #ddd;margin-bottom: 15px;float: left;width: 100%">
		  	<div style="background-color: #f5f5f5;border-bottom: 1px solid #ddd;color: #333;padding: 10px;font-weight: bold;">Summary</div>
		  	<table style="width: 100%;float: left;">
		  		<tbody>
		  			<tr>
		  				<td style="float: left;width: 50%;padding: 10px">
		  					<span><b>Improper Logs:</b></span>
		  					<span> {{$improper_count}}</span>
		  				</td>
		  				<td style="float: right;width: 50%;padding: 10px;">
		  					<span><b>Work Hours:</b></span>
		  					<span>{{$works_hours}}/ {{$total_working_hours[0]['working_hours']}}</span>
		  				</td>
		  			</tr>	
		  		</tbody>
		  	</table>
		  	<!-- <div style="padding: 10px;font-size: 14px;">
			  	<span style="margin-bottom: 5px;float: left;width: 100%">Improper Logs <b>{{$improper_count}}</b></span>
			  	<br/>
			  	<span>Work Hours: <b>{{$works_hours}}</b> / 117</span>
		  	</div> -->
		  </div>
		  <div class="table-responsive" style="float: left;width: 100%">         
			  <table class="table table table-bordered" style="font-size: 12px;">
			    <thead>
			      <tr>
			        <th>Date</th>
			        <th>Time In</th>
			        <th>Time Out</th>
			        <th>Hours</th>
			      </tr>
			    </thead>
			    <tbody>
			        @foreach ($pdf_data as $product=>$value)
			        <tr>
			            <td @if(($value['time_diff']) < '09:00:00')style="background-color:{{config('project.error_code.code')}}" @endif>{{ $value['time'] }}</td>
			            <td @if(($value['time_diff']) < '09:00:00')style="background-color:{{config('project.error_code.code')}}" @endif>{{ $value['check_in'] }}</td>
			            <td @if(($value['time_diff']) < '09:00:00')style="background-color:{{config('project.error_code.code')}}" @endif>{{ $value['check_out'] }}</td>
			            <td @if(($value['time_diff']) < '09:00:00')style="background-color:{{config('project.error_code.code')}}" @endif>{{ $value['time_diff'] }}</td>
			        </tr>
			        @endforeach
			    </tbody>
			  </table>
			</div>  
		</div>
	</body>
</html>