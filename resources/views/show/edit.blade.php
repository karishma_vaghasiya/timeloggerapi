@extends('layout.layout')
@section('content')
<style type="text/css">
    .avtar{
    background-color: #fff;
    padding:20px 0;
    }
    .avtar button.btn-primary {
    float: none;
    width: 80%;
    margin: 20px auto;
    }
    .avtar form {
    float: none;
    width: 90%;
    margin: auto;
    }
    .profile-detail{
    width:100%;
    background-color: #fff;
    padding:20px 0;
    }
    .profile-detail .dl-horizontal, .profile-detail .dd-horizontal {
    margin: 10px 0;
    }
    .profile-detail label {
    float: right;
    font-size: 16px;
    margin: 19px 0;
    }
    .profile-detail .form-control {
    float: left;
    width: 98%;
    margin: 10px 0;
    }
    .profile-detail textarea {
    width: 98%;
    }
    .profile-detail button {
    float: left;
    }
    .checkbox {
    float: left;
    width: 98%;
    text-align: left;
    }
    .checkbox label {
    float: left;
    width: auto;
    margin: 0;
    }
    .checkbox input[type="checkbox"] {
    float: left;
    width: auto;
    margin: 4px 0;
    }
    .dl-horizontal.dd-horizontal > dd {
    width: 100%;
    text-align: left;
    }
    .text-left.check-radio{
        margin-top:20px;
    }
</style>
<div class="page-title">
    <div>
        <h1>Edit Profile</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-3 avtar text-center">
        @if(Auth::user()->profile_pic != null)
        <img src="/upload/<?= Auth::user()->profile_pic ?>" alt="" style="width:150px;height:150px;margin-bottom:20px;">
        @endif
        <a href="<?= URL::route('profile.upload.get') ?>" class="btn btn-primary iframe">Update Photo</a>
    </div>
    <div class="col-md-9">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
            @endforeach
        </div>
        <div class="profile-detail text-center">
            @include('partials.alert')
            <?= Form::model($user,['route'=>['show.update'],'files'=> true , 'class' => 'form-horizontal','method'=>'post']) ?>
            <div class="row">
                <div class="col-sm-3">
                    <label>Gender</label>
                </div>
                <div class="col-sm-9 text-left check-radio">
                    <?= Form::radio('gender','male'); ?> Male
                    <?= Form::radio('gender','female'); ?> Female
                    <?= $errors->first('gender', "<span class='text-danger'>:message</span>"); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <label>FullName</label>
                </div>
                <div class="col-sm-9">
                    <?= Form::text('fullname',old('fullname'),['class'=>'form-control' , 'placeholder' => 'FullName' ]); ?>
                    <?= $errors->first('fullname', "<span class='text-danger'>:message</span>"); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label>Position</label>
                </div>
                <div class="col-sm-9">
                    <?= Form::select('position' ,[''=>'select','Project Manager'=>'Project Manager','Php Developer'=>'Php Developer', 'Android Developer'=>'Anroid Developer', 'Web Developer'=>'Website Desginer' ,'Magento Developer'=>'Magento Developer'], old('position') ,['class' => 'form-control']); ?>
                    <?= $errors->first('position',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label>Date Of Birth</label>
                </div>
                <div class="col-sm-9">
                    <?= Form::text('birthdate', old('birthdate'), ['class' => 'form-control', 'placeholder' => 'Date of Birth','id'=>'birth_Date']); ?>
                    <?= $errors->first('birthdate',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label>Contact No</label>
                </div>
                <div class="col-sm-9">
                    <?= Form::text('contact', old('contact'), ['class' => 'form-control', 'placeholder' => 'Contact No']); ?>
                    <?= $errors->first('contact',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label>Address</label>
                </div>
                <div class="col-sm-9">
                    <?= Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => 'Address']); ?>
                    <?= $errors->first('address',"<span class='text-danger'>:message</span>");?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-3 col-sm-4">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="<?= URL::route('show.user') ?>" class="btn btn-white btn-default">Cancel</a>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('style')
    <?= Html::style('css/colorbox.css') ?>
    <?= Html::style('admin/globals/plugins/selectize/dist/css/selectize.bootstrap3.css') ?>
    <?= Html::style('front/css/user.css') ?>
    <?= Html::style('asset/css/bootstrap-datepicker.css') ?>     
@stop
@section('script')
    <?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>
    <?= Html::script('js/jquery.colorbox-min.js') ?>
    <?= Html::script('admin/globals/plugins/selectize/dist/js/standalone/selectize.min.js') ?>

    <script type="text/javascript">

        parent.$.fn.colorbox.close();

        $(".iframe").colorbox({
            iframe   : true,
            width    : "70%",
            height   : "80%",
            onClosed : function(){ location.reload(true); }
        });

        jQuery(document).ready(function() {
            var currentDate = new Date();  
            //birth date
            $('#birth_Date').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                todayHighlight: true,
                endDate:currentDate,
            });
        $('form').submit(function(){
            $(this).find('button:submit').html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
        });

    });
    </script>
@include('partials.alert')
@stop
