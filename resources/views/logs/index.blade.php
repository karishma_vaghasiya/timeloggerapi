@extends('layout.layout')
@section('content')
<div class="page-title">
	<div>
    	<h1>Employee Logs</h1>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                                
                                @if(auth()->user()->is_admin)
                                    <label for="email">Select Date</label>
                                    <input id="log_Date" type="text" placeholder="Select Date" class="form-control input-sm">
                                @else
                                    <b>Month: </b><?= Form::selectMonth('month',date('m'),['id'=>'month','class' => 'form-control breadcrumb-select input-sm']); ?>
                                    <b>Year: </b><?= Form::selectRange('year','2016','2025',date('Y'),['id'=>'year','class' => 'form-control breadcrumb-select input-sm']); ?>
                                    <input type="hidden" name="month" value="<?= date('m')?>">    
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
                <br/>
            	<table id="employee" class="table table-hover table-bordered" border="1 px" style="width:100%;">
               		<thead>
                  		<tr>
                    		<!-- <th><input type="checkbox" id="selectall1" name="selectall"></th> -->
                            <th>id</th>
		                    <th>Date</th>
		                    <th>TimeIn</th>
                            <th>TimeOut</th>
                            <th>Hours</th>
                            @if(auth()->user()->is_admin)
		                    <th>Employee Name</th>
                            @endif
		                </tr>
               		</thead>
               	</table>
         	</div>
      	</div>
   	</div>
</div>
@stop
@section('style')
<?= Html::script('asset/css/bootstrap-datepicker.css') ?> 

@stop
@section('script')
<?= Html::script('asset/DataTables/media/js/jquery.dataTables.js') ?>
<?= Html::script('asset/DataTables/media/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('asset/js/plugins/sweetalert.min.js') ?>
<?= Html::script('asset/js/plugins/select2.min.js') ?>
<?= Html::script('asset/js/fnStandingRedraw.js') ?>
<?= Html::script('asset/js/bootstrap-datepicker.min.js') ?>

<script>
var token = "<?= csrf_token() ?>";
$( document ).ready(function(){

    var currentDate = new Date();  
    $('#log_Date').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        todayHighlight: true,
        // endDate:currentDate,
    });

    $("#log_Date").datepicker("setDate",currentDate);
        
    $('#employee').dataTable({
        language:{
            "search": "Search By Date",
        },
        "bProcessing" : true,
        "bServerSide" : true,
        "searching"   : false,
        'bPaginate'   : false,
        "pageLength"  : 50, 
        "sAjaxSource": "{{ URL::route('logs.index')}}",
            "fnServerParams": function ( aoData ) {
                @if(auth()->user()->is_admin)
                    var date = $('#log_Date').val();
                    var newdate = date.split("-").reverse().join("-");
                    aoData.push({ "name": "date", "value": newdate });
                @else
                    aoData.push({ "name": "month", "value": $('#month').val() });
                    aoData.push({ "name": "year", "value": $('#year').val()});

                @endif
                server_params = aoData;
        },
        
        "aaSorting"   : [
            [2,"asc"]
        ],
        "aoColumns"   :[
            { mData : 'id',bVisible:false, },
            { mData : 'time' },
            { mData : 'check_in' },
            { mData : 'check_out' },
            { mData : 'time_diff' },
            @if(auth()->user()->is_admin)
            { mData : 'fullname' },
            @endif
        ],
        rowCallback: function( row, data, index ) {
            var  $node = this.api().row(row).nodes().to$();
            if (data.time_diff < '09')
            {
               $node.css('background-color','#ffc0cb');
            }
        }
    });
    $('#log_Date').change(function(){
        $('.dataTable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    });

    $('#month').change(function(){
        $('.dataTable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    });

    $('#year').change(function(){
        $('.dataTable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    });
});
</script>
@stop
