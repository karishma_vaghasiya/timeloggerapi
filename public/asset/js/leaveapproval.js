//this function is used to call confirmation
function leaveApproval(leave_path,title,text,token,type,id,confirmbtntext,leaveapprove)
{
    swal({
        title: title,
        type: type,
        showCancelButton: true,
        confirmButtonText: confirmbtntext,
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            leaveApprovalRequest(leave_path,id,token,leaveapprove);
        }
    });
}
//this function  is used to check coming data is array or not
function approve_leave_checkLength(leave_id,approve_title,approve_text,approve_type,approve_delete_path,approve_confirmButtonText)
{
    var selected_length = leave_id.length;

    if(0 == selected_length){
        EmptyData();
    }else{
        var id = [];
        $.each(leave_id, function(i, ele){
            id.push($(ele).val());
        });
        leaveApproval(approve_delete_path,approve_title,approve_text,token,approve_type,id,approve_confirmButtonText,'approve')
    }
}
//this function  is used to check coming data is array or not
function disapprove_leave_checkLength(leave_id,disapprove_title,disapprove_text,disapprove_type,disapprove_delete_path,disapprove_confirmButtonText)
{
    var selected_length = leave_id.length;

    if(0 == selected_length){
        EmptyData();
    }else{
        var id = [];
        $.each(leave_id, function(i, ele){
            id.push($(ele).val());
        });
        leaveApproval(disapprove_delete_path,disapprove_title,disapprove_text,token,disapprove_type,id,disapprove_confirmButtonText,'disapprove')
    }
}
// console.log('<?php echo "hi" ?>');
//this function  is used to call delete record
function leaveApprovalRequest(leave_path,id,token,leaveapprove)
{
    $.ajax({
        url: leave_path,
        type:'post',
        dataType:'json',
        data:{id:id,_token: token,leaveapprove:leaveapprove},
        beforeSend:function(){
            $('#spin').show();
        },
        complete:function(){
            $('#spin').hide();
            var redrawtable = $('#employee').dataTable();
            redrawtable.fnStandingRedraw();
            var is_checked = $('.select_check_box').is(':checked');
            if(is_checked == true)
            {
                $('.select_check_box').prop('checked',false);
            }

            if(leaveapprove == "approve"){
                // window.location.href = '<?=URL::route('user.index',['type'=>'currentemp'])?>';
                iziToast.success({
                title:'Success!',
                message: "Leave successfully approved",
                });
                
            }else{
                // window.setTimeout(function(){window.location.href = "<?=URL::route('leave.index')?>";},3000)
                iziToast.success({
                title:'Success!',
                message: "Leave successfully disapproved",
                });
            }
        }
    });
}
//Give Error when no data is selected
function EmptyData()
{
    swal({
       title: "Please select a record for leaveApproval",
       type:"error",
       timer: 2000,
       showConfirmButton: false
    });
}
