<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Notification;
use View,Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view)
        {
            if (Auth::check()) {
                $user_id = auth()->user()->id;
                $notification = Notification::where('user_id',$user_id)->where('is_read','0')->orderby('id','desc')->get();
                $notification_count = $notification->count();
                $notification = $notification->toArray();

                $notification_view = Notification::where('user_id',$user_id)->where('is_view','0')->orderby('id','desc')->get();
                $notification_view_count = $notification_view->count();
                $notification_view = $notification_view->toArray();
                View::share('notifications', ['notification_count'=>$notification_count,'notification'=>$notification,'notification_view'=>$notification_view,'notification_view_count'=>$notification_view_count]);
            }else {
                $view->with('currentUser', null);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
