<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\HolidayMastersEvent' => [
            'App\Listeners\HolidayMasterListener',
        ],
        'App\Events\HolidayYearEvent' => [
            'App\Listeners\HolidayYearListener',
        ],
        'App\Events\AssetsEvent' => [
            'App\Listeners\AssetsListener',
        ],
        'App\Events\AssetsMasterEvent' => [
            'App\Listeners\AssetsMasterListener',
        ],
        'App\Events\ServicesEvent' => [
            'App\Listeners\ServicesListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
