<?php

namespace App\Listeners;

use App\Events\ServicesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\ServicesJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ServicesListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServicesEvent  $event
     * @return void
     */
    public function handle(ServicesEvent $event)
    {
        $services = $event->services;
        
        $save_data = $this->dispatch(new ServicesJob($services));
    }
}
