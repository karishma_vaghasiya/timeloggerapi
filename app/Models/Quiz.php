<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{

	public $table = 'quizzes';

	public $fillable = ['question','time'];

	public function getCreatedAtAttribute($value)
	{
		return date('d-M h:i A',strtotime($value));
	}

	public function getImageAttribute($value)
	{
		if ($value != NULL AND $value != '') {
			return asset('upload/quiz/').'/'.$value;
		}
		return NULL;
	}

}
