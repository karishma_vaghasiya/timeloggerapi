<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class configuration extends Model
{
	public $timestamps = false;

	public $table = 'configuration';

	public $fillable = ['device_id','name'];
}
