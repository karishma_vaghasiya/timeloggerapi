<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationDetails extends Model{

    public $table = 'location_details';

    public $fillable = ['device_id','latitude','longitude','time_mili'];
}
