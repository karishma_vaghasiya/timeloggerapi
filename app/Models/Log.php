<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
	public $table = 'logs';

	public $fillable = ['lat','long','user_id','time_diff','time','check_in','check_out'];

	public $timestamps = false;

	public function getHoliday(){
		return $this->hasMAny('App\Models\Holiday','date','time');
	}
	public function getTimeAttribute($value){
		if (!empty($value)) {
			return date('d-m-Y', strtotime($value));
		}
	}
}
