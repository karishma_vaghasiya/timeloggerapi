<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingInfo extends Model{

	public $table = 'settings';
	
	public $fillable = ['lat','long','wifi_info'];
	
}