<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HolidayYear extends Model
{
    public $table = 'holiday_year';

	public $fillable = ['id','day','holiday_id','holiday_date'];
	
	public function getHolidayDateAttribute($value)
    {
        return date('d-m-Y', strtotime($value));

    }
}
