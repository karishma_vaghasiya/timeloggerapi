<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    public $table = 'services';

	public $fillable = ['id','firm_name','person_name','category','address','phone_num','alternative_phone_num','email','account_num'];
}
