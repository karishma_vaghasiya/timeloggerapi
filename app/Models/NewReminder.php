<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewReminder extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'new_reminders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','is_wifi','check_in_start','check_out_start','check_in_end','check_out_end',];
}
