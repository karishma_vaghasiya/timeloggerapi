<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetsMaster extends Model
{
  public $table = 'assets_master';
  public $fillable = ['user_id','assets_id','start_date','end_date','notes'];
	public function getStartdateAttribute($value)
	{
	 	return date("d-m-Y", strtotime($value));
	}
	public function setStartdateAttribute($value)
	{
    	$this->attributes['start_date'] = date('Y-m-d',strtotime($value));
	}
	public function getEndDateAttribute($value)
  {
    return (!empty($value)) ? date("d-m-Y", strtotime($value)) : ""; 
  }
  public function setEndDateAttribute($value)
  {
    if (!empty($value)) {
      $this->attributes['end_date'] = date('Y-m-d',strtotime($value));
    }else{
      $this->attributes['end_date'] = NULL;
    }
  }
}
