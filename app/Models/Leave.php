<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model{

    public $table = 'leaves';

    public $fillable = ['user_id','date','leave_type','leave_time','leave_purpose','status','from_hour','to_hour','token'];

    // protected $dates = ['date'];

    // public function setDateAttribute($value)
    // {
    //     $this->attributes['date'] = strtotime('d-m-Y',$value);
    // }

    protected $date = ['date'];

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = date('Y-m-d',strtotime($date));
    }

    public function getDateAttribute($date)
    {
        return date('d-m-Y',strtotime($date));
    }

}
