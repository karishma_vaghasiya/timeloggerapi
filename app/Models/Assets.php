<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
     public $table = 'assets';

    public $fillable = ['title','device_type','serial_number','notes','main_title'];

    public function setMainTitleAttribute($value){
    	$merge_data = request('title').' '.request('device_type').' '.request('serial_number');
    	return $this->attributes['main_title'] = $merge_data;
    }
}
