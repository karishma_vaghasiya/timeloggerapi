<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Assets;
use App\helpers\FileHelp;
use Illuminate\Http\Request;
class AssetsJob 
{
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $laptop_assets;
    public function __construct($laptop_assets)
    {
        $this->laptop_assets = $laptop_assets;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        
        $laptop_assets = $this->laptop_assets;

        $id = \Request::segment(2);
        $save_laptop_assets = Assets::firstOrNew(['id'=>$id]);
        $save_laptop_assets->fill($laptop_assets);
        // dd($laptop_assets);
        /*$save_laptop_assets->main_title = str_slug($this->laptop_assets['title']).' '.$this->laptop_assets['device_type'].' '.$this->laptop_assets['serial_number'];*/ 
        $save_laptop_assets->save();
        return;
    }
}
