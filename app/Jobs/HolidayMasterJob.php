<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\HolidayMaster;
use Illuminate\Support\Facades\Input;
use App\helpers\FileHelp;

class HolidayMasterJob 
{
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $holiday;
    public function __construct($holiday)
    {
        $this->holiday = $holiday;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        
        $holidays = $this->holiday;
        // dd($id);
       
        $id = \Request::segment(2);
        $save_holiday = HolidayMaster::firstOrNew(['id' => $id]);
        $save_holiday->fill($holidays);
        $save_holiday->save();

        return;
        
    }
}
