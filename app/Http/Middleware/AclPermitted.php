<?php

namespace App\Http\Middleware;

use Closure;
use Auth, Debugbar;
use Illuminate\Contracts\Auth\Guard;
use App\Models\User;
use App\Models\UserPermission;
use App\Models\Permission;

class AclPermitted
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

        $permitted = false;
        $user = Auth::user();
        //dd($user);

        $mapping_route = $request->route()->getName();

        $route_arr = explode('.', $mapping_route);
        $route_arr_last = $route_arr[sizeof($route_arr) - 1];

        if($route_arr_last == 'store')
        {
            $route_arr[sizeof($route_arr) - 1] = 'create';
            $mapping_route = implode('.', $route_arr);
        }

        if($route_arr_last == 'update')
        {
            $route_arr[sizeof($route_arr) - 1] = 'edit';
            $mapping_route = implode('.', $route_arr);
        }
        if($route_arr_last == 'destroy')
        {
            $route_arr[sizeof($route_arr) -1] = 'delete';
            $mapping_route = implode('.',$route_arr);
        }
        //Debugbar::info($mapping_route);
        //dd($mapping_route);
        $permission_id = Permission::select('id')->where('route', $mapping_route)->first();

        $check_user_permission = UserPermission::where('user_id', Auth::id())->where('permission_id', $permission_id['id'])->count();

        if ( $check_user_permission >= 1) {
          return $next($request);
        } else {
            return redirect()->route('user.denied');
        }
     }
}
