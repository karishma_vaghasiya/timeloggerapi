<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\WorkingDays;
use App\Models\Holiday;
use App\Models\HolidayMaster;
use App\Models\HolidayYear;

class WorkingDaysController extends Controller
{
    public function index(Request $request) {

        $dates=[];
        $day_name=[];
        $main_dates=[];
        //get current month all dates
        for($i = 1; $i <=  date('t'); $i++)
        {
            $date = date("Y").'/'.date('m').'/'.$i;
            $dates['dayname'] = date('l', strtotime($date));
            $dates['dates'] = str_pad($i, 2, '0', STR_PAD_LEFT) . "-" . date('m') . "-" . date("Y");
            $dates['holiday_name']='';
            $main_dates[str_pad($i, 2, '0', STR_PAD_LEFT) . "-" . date('m') . "-" . date("Y")]=$dates;

        }
        
        //get month and year of current month
        $month = date('m');
        $year = date('Y');
        $working_hrs = "";
        $hours = "";
        $ids = "";

        $workingdays_data = WorkingDays::where('month',$month)->where('year',$year)->get();
        if(count($workingdays_data)>0){
            $workingdays_data = $workingdays_data->toArray();
            foreach ($workingdays_data as $key => $value) {
                $working_hrs[]= $value['working_date'];
                $hours[$value['working_date']] = $value['working_hours'];
                $ids[$value['working_date']] = $value['id'];
            }
        }
        //get all holiday
        // $holiday = Holiday::get();
        $holiday = HolidayYear::select('holiday_year.holiday_date as dates','holiday_year.day as dayname','holiday_master.title as holiday')->leftjoin('holiday_master','holiday_master.id','=','holiday_year.holiday_id')->get();
        if(count($holiday)>0){
            $holiday_data = $holiday->toArray();
            foreach($main_dates as $maindate_key=>$maindate_value){
                foreach ($holiday_data as $key => $value) {
                    if($value['dates'] == $maindate_key){
                        $holidays['holiday_name'] = $value['holiday'];
                        $holidays['holiday'] = true;
                        $holidays['dayname'] = $maindate_value['dayname'];
                        $holidays['dates'] = $maindate_key;
                        $main_dates[$maindate_key] = $holidays;
                    }
                }
            }
        }

        //for even and odd sat
        $c = 0;
        $c1 = 0;
        $odd = ['1'=>'1st','3'=>'3rd','5'=>'5th'];
        $even = ['2'=>'2nd','4'=>'4th','6'=>'6th'];
        $weekname = ['','First Week','Second Week','Third Week','Fourth Week'];
        $week_name=[];

        foreach($main_dates as $maindate_key=>$maindate_value){
            if($maindate_value['dayname'] == "Saturday"){
                $c++;
                if($c==1 || $c==3 || $c==5){
                    $odd_sat_date['odd_sat_date'] = $odd[$c];
                    $odd_sat_date['odd_sat_day'] = true;
                    $odd_sat_date['dayname'] = $maindate_value['dayname'];
                    $odd_sat_date['dates'] = $maindate_key;
                    $odd_sat_date['holiday_name'] = $maindate_value['holiday_name'];
                    $main_dates[$maindate_key] = $odd_sat_date;
                }else{
                    $even_sat_date['even_sat_date'] = $even[$c];
                    $even_sat_date['even_sat_day'] = true;
                    $even_sat_date['dayname'] = $maindate_value['dayname'];
                    $even_sat_date['dates'] = $maindate_key;
                    $even_sat_date['holiday_name'] = $maindate_value['holiday_name'];
                    $main_dates[$maindate_key] = $even_sat_date;
                }
            }
            elseif($maindate_value['dayname'] == "Sunday"){
                $c1++;
                if($c1==1 || $c1==3 || $c1==5){
                    $odd_sun_date['odd_sun_date'] = $odd[$c1];
                    $odd_sun_date['odd_sun_day'] = true;
                    $odd_sun_date['dayname'] = $maindate_value['dayname'];
                    $odd_sun_date['dates'] = $maindate_key;
                    $odd_sun_date['holiday_name'] = $maindate_value['holiday_name'];
                    $main_dates[$maindate_key] = $odd_sun_date;
                }else{
                    $even_sun_date['even_sun_date'] = $even[$c1];
                    $even_sun_date['even_sun_day'] = true;
                    $even_sun_date['dayname'] = $maindate_value['dayname'];
                    $even_sun_date['dates'] = $maindate_key;
                    $even_sun_date['holiday_name'] = $maindate_value['holiday_name'];
                    $main_dates[$maindate_key] = $even_sun_date;
                }
            }
        }
        // dd($main_dates);
        //for week nane
        $timestamp = mktime(0, 0 , 0, $month, 1, $year); 
        $timestamp += (60 * 60 * 24); 
        $first_week_no = date('W', $timestamp); 
        $range = range($first_week_no, $first_week_no + 5); 
        $weeks=[];
        $weekname = ['','First Week','Second Week','Third Week','Fourth Week','Fifth Week','Sixth Week'];

        $c1=0;
        
        foreach ($range as $week_no) {
            $c1++;
            $week_start = new \DateTime();
            $week_start->setISODate($year, $week_no);
            $week_start->modify('-1 day');

            $seven_day_week = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
            $week = array();

            for ($i = 0; $i < 7; $i++) {
                $day = $seven_day_week[$i];
                $week[$day] = $week_start->format('Y-n-j');
                $week_start->modify('+1 day');
            }
            $index = $weekname[$c1];
            $weeks[$index]=$week;
        }

        return view('workingdays.index',['dates'=>$main_dates,'working_hrs'=>$working_hrs,'week_name'=>$weeks,'hours'=>$hours,'ids'=>$ids]);
    }

    public function workingdays(Request $request){
        if($request->ajax()){
            $dates=[];
            $day_name = [];
            $data = $request->all();
            $main_dates = [];

            //calculate day of given month and year
            $count_days = cal_days_in_month(CAL_GREGORIAN,$data['month'], $data['year']);

            for ($i = 1; $i <= $count_days; $i++) {
                $date = '2018'.'/'.$data['month'].'/'.$i; 
                $dates['dayname'] = date('l', strtotime($date));
                $dates['dates'] = str_pad($i, 2, '0', STR_PAD_LEFT) . "-" .str_pad($data['month'], 2, "0", STR_PAD_LEFT). "-" . $data['year'];
                $dates['holiday_name']='';
                $main_dates[str_pad($i, 2, '0', STR_PAD_LEFT) . "-" .str_pad($data['month'], 2, "0", STR_PAD_LEFT). "-" . $data['year']]=$dates;
            }
        }
        
        //get month(in 2 digit) and year of current month
        $month = str_pad($data['month'], 2, "0", STR_PAD_LEFT);
        $year = $data['year'];
        $working_hrs = "";
        $hours = "";
        $ids = "";
        $workingdays_data = WorkingDays::where('month',$month)->where('year',$year)->get();

        if(count($workingdays_data)>0){
            $workingdays_data = $workingdays_data->toArray();
            foreach ($workingdays_data as $key => $value) {
                $working_hrs[]= $value['working_date'];
                $hours[$value['working_date']] = $value['working_hours'];
                $ids[$value['working_date']] = $value['id'];
            }
        }
        //get all holiday
        $holiday = HolidayYear::select('holiday_year.holiday_date as dates','holiday_year.day as dayname','holiday_master.title as holiday')->leftjoin('holiday_master','holiday_master.id','=','holiday_year.holiday_id')->get();
        if(count($holiday)>0){
            $holiday_data = $holiday->toArray();
            foreach($main_dates as $maindate_key=>$maindate_value){
                foreach ($holiday_data as $key => $value) {
                    if($value['dates'] == $maindate_key){
                        $holidays['holiday_name'] = $value['holiday'];
                        $holidays['holiday'] = true;
                        $holidays['dayname'] = $maindate_value['dayname'];
                        $holidays['dates'] = $maindate_key;
                        $main_dates[$maindate_key] = $holidays;
                    }
                }
            }
        }

        //for even and odd sat
        $c = 0;
        $c1 = 0;
        $odd = ['1'=>'1st','3'=>'3rd','5'=>'5th'];
        $even = ['2'=>'2nd','4'=>'4th','6'=>'6th'];
        foreach($main_dates as $maindate_key=>$maindate_value){
            if($maindate_value['dayname'] == "Saturday"){
                $c++;
                if($c==1 || $c==3 || $c==5){
                    $odd_sat_date['odd_sat_date'] = $odd[$c];
                    $odd_sat_date['odd_sat_day'] = true;
                    $odd_sat_date['dayname'] = $maindate_value['dayname'];
                    $odd_sat_date['dates'] = $maindate_key;
                    $odd_sat_date['holiday_name'] = $maindate_value['holiday_name'];
                    $main_dates[$maindate_key] = $odd_sat_date;
                }else{
                    $even_sat_date['even_sat_date'] = $even[$c];
                    $even_sat_date['even_sat_day'] = true;
                    $even_sat_date['dayname'] = $maindate_value['dayname'];
                    $even_sat_date['dates'] = $maindate_key;
                    $even_sat_date['holiday_name'] = $maindate_value['holiday_name'];
                    $main_dates[$maindate_key] = $even_sat_date;
                }
            }
            elseif($maindate_value['dayname'] == "Sunday"){
                $c1++;
                if($c1==1 || $c1==3 || $c1==5){
                    $odd_sun_date['odd_sun_date'] = $odd[$c1];
                    $odd_sun_date['odd_sun_day'] = true;
                    $odd_sun_date['dayname'] = $maindate_value['dayname'];
                    $odd_sun_date['dates'] = $maindate_key;
                    $odd_sun_date['holiday_name'] = $maindate_value['holiday_name'];
                    $main_dates[$maindate_key] = $odd_sun_date;
                }else{
                    $even_sun_date['even_sun_date'] = $even[$c1];
                    $even_sun_date['even_sun_day'] = true;
                    $even_sun_date['dayname'] = $maindate_value['dayname'];
                    $even_sun_date['dates'] = $maindate_key;
                    $even_sun_date['holiday_name'] = $maindate_value['holiday_name'];
                    $main_dates[$maindate_key] = $even_sun_date;
                }
            }
        }

        //for week name
        $timestamp = mktime(0, 0 , 0, $month, 1, $year); 
        $timestamp += (60 * 60 * 24); 
        $first_week_no = date('W', $timestamp); 
        $range = range($first_week_no, $first_week_no + 5); 
        $weeks=[];
        $weekname = ['','First Week','Second Week','Third Week','Fourth Week','Fifth Week','Sixth Week'];
        $c1=0;
        foreach ($range as $week_no) {
            $c1++;
            $week_start = new \DateTime();
            $week_start->setISODate($year, $week_no);
            $week_start->modify('-1 day');

            $seven_day_week = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
            $week = array();

            for ($i = 0; $i < 7; $i++) {
                $day = $seven_day_week[$i];
                $week[$day] = $week_start->format('Y-n-j');
                $week_start->modify('+1 day');
            }
            $index = $weekname[$c1];
            $weeks[$index]=$week;
        }

        $res_html = view('workingdays.dates',['dates'=>$main_dates,'working_hrs'=>$working_hrs,'week_name'=>$weeks,'hours'=>$hours,'ids'=>$ids])->render();

        return response()->json(['success' => true,'response_html' => $res_html], 200);
    }

    public function storeworkingdays(Request $request){

        $working_days = $request->all();
        $ids =[];
        $working_days['month'] = str_pad($working_days['month'], 2, "0", STR_PAD_LEFT);
        $working_day = WorkingDays::where('month',$working_days['month'])->where('year',$working_days['year'])->delete();
        foreach ($working_days['id'] as $key => $value) {
            $day_hours = explode(',',$value);
            $working_day = new WorkingDays();
            $working_day->month = $working_days['month'];
            $working_day->year  = $working_days['year'];
            $working_day->working_date = $day_hours[0];
            if(!empty(trim($day_hours[1]))){
                $working_day->working_hours = trim($day_hours[1]);
            }else{
                $working_day->working_hours = '09:00:00';
            }
            // dd($working_day);
            $working_day->save(); 
        }

        return response()->json(['success' => true], 200);
    }
    public function workingdaysHours(Request $request){
        $data = $request->all();
        $submit_data = $data['submitdata'];
        $value = $submit_data['value'];
        $id = $submit_data['id'];
        WorkingDays::where('id',$id)->update(['working_hours'=>$value]);
        return $value;
    }
}
