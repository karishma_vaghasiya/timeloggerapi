<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Assets;
use App\Events\AssetsEvent;
use Event,Log;
use App\Models\User;

class AssetsController extends Controller
{
    public function index(Request $request)
    {

        if($request->ajax()){
          
            $where_str = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                 $where_str .= " and (title like \"%{$search}%\""
                            ." or device_type like \"%{$search}%\""
                            ." or serial_number like \"%{$search}%\""
                            .")";
            }
             $columns =['id','title','device_type','serial_number','notes'];

            $brand = Assets::select($columns)
                    ->whereRaw($where_str,$where_params);
                  
            $brand_count = Assets::select($columns)
                        ->whereRaw($where_str,$where_params)
                        ->count();

           

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $brand = $brand->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $brand = $brand->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $brand = $brand->get();
            $response['iTotalDisplayRecords'] = $brand_count;
            $response['iTotalRecords'] = $brand_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $brand;

            return $response;
        }

        return view('assets.index');
    }
    public function create()
    {  
    	return view('assets.create',compact('users'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[

            'title' => 'required',
            'device_type' => 'required',
            'serial_number' => 'required',
            'notes' => 'required',   
        ]);
    	$laptop_assets = $request->all();
    	Event::fire(new AssetsEvent($laptop_assets));
    	if($laptop_assets['save_button'] == "save_new"){
            return redirect()->back()->with('message','Record Added Successfully')->with('message_type','success');
        }
        return redirect()->route('assets.index')
                         ->with('message','Record Added Successfully.')
                         ->with('message_type','success'); 
    }
    public function edit($id)
    {
        $laptop_assign = Assets::where('id',$id)->first();
        
        return view('assets.edit',compact('laptop_assign'));
    }
    public function update(Request $request)
    {
        $this->validate($request,[

            'title' => 'required',
            'device_type' => 'required',
            'serial_number' => 'required',
            'notes' => 'required',   
        ]);
        $laptop_assets = $request->all();

        Event::fire(new AssetsEvent($laptop_assets));
        if($laptop_assets['save_button'] == "save"){
            return redirect()->back()
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');    
        }
        return redirect()->route('assets.index')
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');
    }
    public function delete(Request $request)
    {
        $id = $request->get('id');
        $laptop_assets = Assets::where('id', $id)->first();
        Assets::where('id', $id)->delete();

        return back()->with('message', 'Record Deleted Successfully.')
            ->with('message_type', 'success');
    }
}
