<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HolidayMaster;
use App\Http\Requests;
use App\helpers\FileHelp;
use Input,Debugbar,Auth,URL,Validator,Redirect;
use App\Utility\ImageResize;
use App\Events\HolidayMastersEvent;
use Event;
class HolidaysMasterController extends Controller
{
	public function index(Request $request)
	{

        if ($request->ajax()) {
            $where_str = '1 = ?';
            $where_params = array('1');

            if ($request->get('search') ['value'] != "") {
                $search = $request->get('search') ['value'];
                $where_str .= " and ( title like \"%{$search}%\""
                             
                    . ")";
            }
            $user = HolidayMaster::select('id','title')
                ->whereRaw($where_str, $where_params);

            $user_count = HolidayMaster::select('id')
                ->whereRaw($where_str, $where_params)
                ->count();

            $columns = ['id','title'];
            $order_columns = ['title'];

             if($request->has('start') && $request->get('length') !='-1'){
            $user = $user->take($request->get('length'))
                                    ->skip($request->get('start'));
            }
            if($request->has('order')){
                $sql_order='';
                for ( $i = 0; $i <= $request->input('order.0.column'); $i++ )
                {
                    $column = $order_columns[$i];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $user = $user->orderBy($column,$request->input('order.'.$i.'.dir'));
                }
            }


            $user = $user->get();
            $response['iTotalDisplayRecords'] = $user_count;
            $response['iTotalRecords'] = $user_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $user;

            return $response;


            
        }

       
        return view('holiday-master.index');
    }   

    public function create()
    {
        $holiday_master = HolidayMaster::orderBy('id','desc')->first();
    	return view('holiday-master.create',compact('holiday_master'));
    }
    public function store(Request $request)
    {

        $HolidayMaster = HolidayMaster::where('title',null)->orderBy('id','desc')->first();
        $this->validate($request,[

            'title'      => 'required',
        ],[
            'title.required' => 'Holiday name is required',
        ]);

        $holiday = $request->all();
        // dd($holiday);
         
        Event::fire(new HolidayMastersEvent($holiday));
        if($holiday['save_button'] == "save_new"){
            return redirect()->back()->with('message','Your Holiday Has Been Updated Successfully')->with('message_type','success');
        }
        return redirect()->route('holiday-master.index')
                     ->with('message','Your Holiday Has Been Updated Successfully')
                     ->with('message_type','success');
    }
    public function edit($id)
    {
        $holiday_master = HolidayMaster::where('id',$id)->first();
        return view('holiday-master.edit',compact('holiday_master'));
    }
    public function update(Request $request, $id)
    {
        $holiday = $request->all();

        Event::fire(new HolidayMastersEvent($holiday));
        if($holiday['save_button'] == "save_exit"){
            return redirect()->route('holiday-master.index')
                         ->with('message','Holiday Updated successfully')
                         ->with('message_type','success');    
        }
        return redirect()->back()
                         ->with('message','Holiday Updated successfully')
                         ->with('message_type','success');
    }
    public function delete(Request $request) {
      
        $id = $request->get('id');
        $holiday_master = HolidayMaster::where('id', $id)->first();

        HolidayMaster::where('id', $id)->delete();

        return back()->with('message', 'Record Deleted Successfully.')
            ->with('message_type', 'success');

    }
    public function getUploadProfilePhoto()
    {
        return view('holiday-image.upload-photo');
    }
    public function postUploadProfilePhoto(Request $request)
    {
        $holiday_master = new HolidayMaster;

        $rules = ['image' => 'required|mimes:jpeg,jpg,png|image|max:4096'];

        $validator = Validator::make($request->all(), $rules);

        if ( $validator->passes() )
        {
            $microtime = microtime();
            $search = array('.',' ');
            $microtime = str_replace($search, "_", $microtime);
            $photo = $microtime . '.' . $request->file('image')->getClientOriginalExtension();

            $path = public_path()."/upload/";

            $old_file = $path . $holiday_master->photo;

            list($width, $height, $type, $attr) = getimagesize($request->file('image'));

            $save_w = $width;
            $save_h = $height;

            $request->file('image')->move($path, $photo);

            if ( $save_w >= 500 || $save_h >= 500 )
            {
                $resizeObj = new ImageResize($path . $photo);
                $resizeObj->resizeImage(100, 100);
                $resizeObj->saveImage($path . $photo, 100);
            }
            $holiday_master->temp_photo = $photo;

            $holiday_master->save();

            return redirect()->route('holiday.photo.crop.get');
        }
        else
        {
            return back()->withErrors($validator);
        }
    }

    public function getCropProfilePhoto(Request $request)
    {
        //$profile = User::find(Auth::id());
        $get_photo = HolidayMaster::orderBy('id','desc')->first();
        
        return view('holiday-image.crop-photo',compact('get_photo'));
    }
    public function postCropProfilePhoto(Request $request) {

        $holiday = HolidayMaster::orderBy('id','desc')->first();
        $photo = $holiday->temp_photo;

        $old_photo = $holiday->image;

        $public_path = str_replace("\\","/", public_path());

        $save_folder =  $public_path . '/upload/';
        $photo_file = $save_folder . '/' . $photo;

        if ($old_photo != NULL AND $old_photo != '') {
            $old_photo_path = $save_folder . '/' . $old_photo;

            @unlink($old_photo_path);
        }

        list($width, $height, $type, $attr) = getimagesize($photo_file);

        $save_w = $width;
        $save_h = $height;

        $crop_x = $request->get('x');
        $crop_y = $request->get('y');
        $crop_w = $request->get('w');
        $crop_h = $request->get('h');

        $resizeObj = new ImageResize($photo_file);

        $resizeObj->cropCustomized($crop_x, $crop_y,$crop_w, $crop_h, $crop_w, $crop_h);
        $outputfile = $save_folder . $photo;

        $resizeObj->saveImage($outputfile, 100);

        $holiday->image = $photo;
        $holiday->save();

        return Redirect::route('holiday-master.create')->with('message','Your Profile Photo Updated Successfully')
                                           ->with('message_type','success');
    }
}
