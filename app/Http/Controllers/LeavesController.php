<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Leave;
use App\Models\User;
use App\Models\Notification;
use Input,Response,Auth,Mail,Session,View,Validator;

class LeavesController extends Controller{

    public function index(Request $request){
        if($request->ajax()){

            if(auth()->user()->is_admin){
                $where_str = "1 = ?";
                $where_param = array(1);
                if($request->has('sSearch')){

                    $search = $request->get('sSearch');

                    $where_str .= " and (DATE_FORMAT(leaves.date, '%d-%m-%Y') like \"%{$search}%\""
                                ." or leaves.leave_type like \"%{$search}%\""
                                ." or leaves.leave_time like \"%{$search}%\""
                                ." or leaves.leave_purpose like \"%{$search}%\""
                                ." or leaves.status like \"%{$search}%\""
                                ." or users.fullname like \"%{$search}%\""
                                .")";
                }
                $user=null;
                if($request->has('user')){
                    $user = $request->get('user');
                    $where_str .= " and user_id = '$user'";
                }
                if($request->has('leavetype')){
                    $leavetype = $request->get('leavetype');
                    $where_str .= " and leave_type = '$leavetype'";
                }
                //=========
                $user_id = $user;

                $sick_leave = Leave::where('user_id',$user_id)->where('status','a')->where('leave_type','Sick Leave')->count();
                $privilege_leave = Leave::where('user_id',$user_id)->where('status','a')->where('leave_type','Privilege Leave')->count();
                $casual_leave = Leave::where('user_id',$user_id)->where('status','a')->where('leave_type','Casual Leave')->count();

                $res_html = view('leaves.leavelist',['sick_leave'=>$sick_leave,'privilege_leave'=>$privilege_leave,'casual_leave'=>$casual_leave])->render();
                //===========


                $columns = array('leaves.id','leaves.date','leaves.leave_type','leaves.leave_time','leaves.leave_purpose','leaves.status','leaves.from_hour','leaves.to_hour','users.fullname as name');
                $leaves_count = Leave::select('leaves.user_id')
                            ->whereRaw($where_str,$where_param)
                            ->leftjoin('users','users.id','=','leaves.user_id')
                            ->count();

                $leaves = Leave::select($columns)
                        ->whereRaw($where_str,$where_param)
                        ->leftjoin('users','users.id','=','leaves.user_id');
            }else{
                $where_str = "user_id=?";

                $where_param =array( Auth::id() );
                if($request->has('sSearch')){

                    $search = $request->get('sSearch');

                    $where_str .= " and (DATE_FORMAT(leaves.date, '%d-%m-%Y') like \"%{$search}%\""
                            ." or leaves.leave_type like \"%{$search}%\""
                            ." or leaves.leave_time like \"%{$search}%\""
                            ." or leaves.status like \"%{$search}%\""
                            ." or leaves.leave_purpose like \"%{$search}%\""
                            .")";
                }
                $columns = array('id','date','date','leave_type','leave_time','leave_purpose','status','from_hour','to_hour');
                $leaves_count = Leave::select('user_id')
                            ->whereRaw($where_str,$where_param)
                            ->count();

                $leaves = Leave::select($columns)
                        ->whereRaw($where_str,$where_param);

            }
            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $leaves = $leaves->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $leaves = $leaves->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $leaves = $leaves->get();
            $response['iTotalDisplayRecords'] = $leaves_count;
            $response['iTotalRecords'] = $leaves_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $leaves;
            if($request->has('user')){
                $response['res_html'] = $res_html;
            }

            return $response;
        }
        $user = ["" => "Select User"] + User::orderBy('fullname','asc')->pluck('fullname', 'id')->toArray();

		return view('leaves.index',['user'=>$user]);
	}

	public function create(){

		return view('leaves.create');
	}

	public function store(Request $request){

        $id = Auth::id();
        $data = $request->all();

        $token = str_random(40);

        $data['user_id'] = $id;
        $data['token'] = $token;

        $rules = [
            'date' => 'required',
            'leave_type' => 'required',
            'leave_time' => 'required',
            'leave_purpose' => 'required',
        ];

        if (isset($data['leave_time']) && $data['leave_time'] == 'half day') {
            $rules['from_hour'] = 'required';
            $rules['to_hour'] = 'required';
        }

		$this->validate($request,$rules);

        $leaves = new Leave($data);
		$leaves->save();

        $email_id = 'rajan@thinktanker.in';

        if ($leaves->save())
        {
            $data = array(
                'token' => $token
            );

            Mail::send('notify', $data, function($message) use ($email_id,$request)
            {
                $message->from(Auth::user()->email);

                $message->to($email_id)->subject('Please Approve Leave');
            });
        }

        return back()->with('message','Your Leave Submitted Successfully')
                     ->with('message_type','success');
	}

	public function edit($id){

		$leaves = Leave::find($id);

		return view('leaves.edit',compact('leaves'));
	}

	public function update(Request $request,$id){

		$leaves = Leave::find($id);

        $rules = [
            'date' => 'required',
            'leave_type' => 'required',
            'leave_time' => 'required',
            'leave_purpose' => 'required',
        ];

        if (isset($data['leave_time']) && $data['leave_time'] == 'half day') {
            $rules['from_hour'] = 'required';
            $rules['to_hour'] = 'required';
        }

        $this->validate($request,$rules);

		$leaves->fill($request->all());
        $leaves->save();

        $email_id = 'rajan@thinktanker.in';

        if ($leaves->save())
        {
            $data = array(
                'id' => $id
            );

            Mail::send('notify', $data, function($message) use ($email_id,$request)
            {
                 $message->to($email_id)->subject('Please Approve The Editing Leave');
            });
        }

        return back()->with('message','Your Leave Updated Successfully')
                     ->with('message_type','success');

	}

	public function destroy(Request $request)
	{
        $leave=$request->get('id');

        if(!is_array($leave)){

            $leave = array($leave);
        }

        $leave = leave::whereIn('id',$leave)->delete();

	}

    public function approveLeave($token)
    {
        $leaves = Leave::where('token',$token)->where('status','p')->first();

        if(! $leaves) {
            return 'wrong request';
        }

        $user = User::select('fullname')->where('id',$leaves->user_id)->first();

        return view('leaves.approveleave',compact('leaves','token','user'));
    }

    public function approvemail(Request $request,$token)
    {
		Leave::where('token', $token)->update(array('status' => 'a'));

        $msg = $request->get('message');

        $leaves = Leave::where('token',$token)->first();

        $email_id = User::where('id',$leaves->user_id)->pluck('email')[0];

        $data = array(
            'msg' => $msg
        );

        Mail::send('response',$data, function($message) use ($email_id)
        {
            $message->from('rajan@thinktanker.in');
            $message->to($email_id)->subject("Approve Leave");
        });

        return response()->json(array('success' => true),200);

    }
    public function rejectemail(Request $request,$token)
    {
        Leave::where('token', $token)->update(array('status' => 'd'));

        $msg = $request->get('message');

        $leaves = Leave::where('token',$token)->first();

        $email_id = User::where('id',$leaves->user_id)->pluck('email')[0];

        $data = array(
            'msg' => $msg
        );

        Mail::send('rejectleave',$data, function($message) use ($email_id,$request)
        {
            $message->from('rajan@thinktanker.in');
            $message->to($email_id)->subject("Approve Leave");
        });

        return response()->json(array('success' => true),200);
    }

    public function userCreate($userId)
    {
        return view('leaves.user-create',compact('userId'));
    }

    public function userStore(Request $request, $userId)
    {
        $data = $request->all();

        $data['user_id'] = $userId;
        $data['token'] = $token;

        $rules = [
            'date' => 'required',
            'leave_type' => 'required',
            'leave_time' => 'required',
            'leave_purpose' => 'required',
        ];

        if (isset($data['leave_time']) && $data['leave_time'] == 'half day') {
            $rules['from_hour'] = 'required';
            $rules['to_hour'] = 'required';
        }

        $this->validate($request,$rules);

        $leaves = new Leave($data);

        $leaves->status = 'a';

        if (isset($data['ungrant'])) {
            $leaves->is_granted = 1;
        }

        $leaves->save();

        return back()->with('message','leave added Successfully')
                     ->with('message_type','success');
    }
    public function leavestore(Request $request)
    {
      
        $firststep_store = $request->formData;
        // dd($firststep_store);
        $date_of_leave = explode(',', $firststep_store['date']);
        $array_count_value = count($date_of_leave);
        $firstform_date = Session::put('firstform_date',$date_of_leave);  
        // $session_data = json($firstform_date);        
        $date = Session::put('date',$firststep_store['date']);
        $first_leavetype = Session::put('leave_type',$firststep_store['leave_type']);
        $first_leavepurpose = Session::put('leave_purpose',$firststep_store['leave_purpose']);
        $get_leavetype = Session::get('leave_type');
        $get_leavepurpose = Session::get('leave_purpose');
        // dd($get_leavetype);
        $content = View::make('dashboard.leave_form', compact('date_of_leave','get_leavetype','get_leavepurpose'))->render();

        return response()->json(['success'=>true,'html_content'=>$content,'firststep_store'=>$firststep_store,'date_of_leave'=>$date_of_leave]);
  
    }
    public function leavesecondstore(Request $request)
    {
        $requestAll = $request->all();
        $leave_detail = $request['leave_detail'];

        foreach ($leave_detail as $key => $value) {
            // dd($value);
            if(isset($value['day_type']))
            {
                $validator = Validator::make($value, [
                'leave_purpose' => 'required',
                 ]);
                if($validator->fails())
                {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            }else
            {
                $validator = Validator::make($value, [
                'from_hour' => 'required',
                'to_hour' => 'required',    
                'leave_purpose' => 'required',
                 ]);
                if($validator->fails())
                {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            } 
        }
        $session_put_data = Session::put('leave_detail',$leave_detail);
        
        return response()->json(['success'=>true,'leave_detail'=>$leave_detail]);

    }
    public function leavethirdstore(Request $request)
    {
        $step2_session = Session::get('leave_detail');
        if(!empty($step2_session))
        {
            $leave_confirmation = []; 
            foreach ($step2_session as $key => $value) {
        
                $leaves = new Leave;
                $leaves->user_id = Auth::id();
                $leaves->date = $key;
                $leaves->leave_type = $value['leave_type'];
                if($value['from_hour'] == "10:00 AM" && $value['to_hour'] == "07:00 PM")
                {
                    $leaves->leave_time = "full day";
                     $leaves->to_hour = $value['to_hour'];
                     $leaves->from_hour = $value['from_hour'];
                    
                }else
                {
                    $leaves->from_hour = $value['from_hour'];
                    $leaves->to_hour = $value['to_hour'];
                    $leaves->leave_time = "half day";
                }
                $leaves->leave_purpose = $value['leave_purpose'];
                
                $leaves->save();    
            }
             
        }else
        {
        $firstform_date = Session::get('firstform_date');
        $first_leavetype = Session::get('leave_type');
        $first_leavepurpose = Session::get('leave_purpose');
             //dd($firstform_date);
        $leave_confirm = [];
        foreach ($firstform_date as $key => $value) {

                $leave_confirm[$key] = $value;
                $leaves = new Leave;
                $leaves->user_id = Auth::id();
                $leaves->date = $value;
                $leaves->leave_type = $first_leavetype;
                $leaves->leave_time = "full day";
                $leaves->leave_purpose = $first_leavepurpose;
                
                $leaves->save();    
            }
            

         } 
       
         return response()->json(['success'=>true,'request'=>$request->all()]);
    }
    public function leaveStatus(Request $request){
        $leave_data = $request->all();
        $id = $leave_data['id'];
        if (!is_array($id)) {

            $id = array($id);
        }
        // dd($id);
        $leaves = Leave::whereIn('id',$id)->first()->toArray();

        $notification = new Notification();

        if($leave_data['leaveapprove'] == "approve"){
            $leave = Leave::whereIn('id',$id)->update(['status'=>'a']);
            $notification->user_id = $leaves['user_id'];
            $notification->title = '<strong>Leave Approved</strong><br/>'.substr($leaves['leave_purpose'],0,20).'...';
            $notification->is_read = 0;
            $notification->is_view = 0;
            $notification->save();
            return back()->with('message','Leave Approved Successfully')
                        ->with('message_type','success');
        }else{
            $leave = Leave::whereIn('id',$id)->update(['status'=>'d']);
            $notification->user_id = $leaves['user_id'];
            $notification->title = '<strong>Leave Disapproved</strong><br/>'.substr($leaves['leave_purpose'],0,20).'...';
            $notification->is_read = 0;
            $notification->is_view = 0;
            $notification->save();
            return back()->with('message','Leave Disapproved Successfully')
                        ->with('message_type','success');
        }

    }
    public function notificationIsread(Request $request){
        // $notification = $notifications['notification'];
        $notification_data = $request->all();
        $notification = json_decode($notification_data['notification'],true);
        foreach ($notification as $key => $value) {
            $ids[]=$value['id'];
        }
        $notification = Notification::whereIn('id',$ids)->update(['is_read'=>'1']);
    }
    public function notificationIsview(Request $request){
        $notification_data = $request->all();
        $notification = Notification::where('id',$notification_data['id'])->update(['is_view'=>'1']);
        
    }
}   
