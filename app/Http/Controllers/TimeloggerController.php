<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Log;
use App\Models\Leave;
use App\Models\HolidayYear;
use App\Models\WorkingDays;
use App\Utility\ImageResize;
use Mail,Auth,DB,Validator,Redirect,Carbon;

class TimeloggerController extends Controller
{
    public function index(Request $request)
    {
         

      $type = $request->get('type');
      
    
       if($request->ajax()){

            $where_str = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                  $where_str .= " and (fullname like \"%{$search}%\""
                            ." or position like \"%{$search}%\""
                            ." or email like \"%{$search}%\""
                            .")";
            }

            $columns = array('id','profile_pic','fullname','position','email');


            $users_count = User::select('id')
                        ->whereRaw($where_str,$where_params);
                        
            $users = User::select($columns);

            if($type == 'currentemp'){
                $is_verified = '1';
            }else{
                $is_verified = '0';
            } 
             
            $users_count = $users_count->where('is_verified',$is_verified)
                            ->count();
            
            $users = $users->where('is_verified',$is_verified)
                    ->whereRaw($where_str,$where_params);
                  

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $users = $users->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $users = $users->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $users = $users->get();
            $response['iTotalDisplayRecords'] = $users_count;
            $response['iTotalRecords'] = $users_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $users;

            return $response;
        }

        return view('user.index');
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $btn_id = $data['id'];

        $this->validate($request,[
            'gender' => 'required',
            'fullname' => 'required|regex:/^[a-zA-Z .]*$/',
            'position' => 'required',
            'birthdate' => 'required',
            'email' => 'required|regex:/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]{5,}+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/|unique:users,email',
            'contact' => 'required|regex:/^[- +()]*[0-9][- +()             0-9]*$/|max:15|min:10',
            'emergency_contact' => 'required|regex:/^[- +()]*[0-9][-+()0-9]*$/|max:15|min:10',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'address' => 'required',
            'joining_date'=>'required',
            'linkedin'=>'required'
        ]);
        if(\Session::has('image_name')){
            $path = public_path()."/upload/".trim(\Session::get('image_name'));
            $temp_path = public_path()."/temp_image/".trim(\Session::get('image_name'));
            \File::move($temp_path,$path);
        }

        $user = new User($data);
        $user->profile_pic = trim(\Session::get('image_name'));
        $user->password = \Hash::make($data['password']);
        $activation_code = str_random('20');
        $user->confirm_code = $activation_code;
        $user->is_verified = 1;
        $user->save();
        $email_id = $user->email;

        if ($user->save())
        {
            $data = array(
                'fullname' => $user->fullname,
                'code'     => $activation_code);

            Mail::send('notification', $data, function($message) use ($email_id)
            {
                $message->from('noreply@thinktanker.in');
                $message->to($email_id)->subject('Please activate your account');
            });
        }
        if ($btn_id == 'submit') {
            return response()->json('new');
        }else{
            \Session::forget('image_name');
            return response()->json('exit');
        }
    }

    public function admin()
    {
       
       $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")->whereMonth('working_date','=',date('m'))
                                 ->where('year','=',date('Y'))
                                 ->get()
                                 ->toArray();
        $exp_total_hours = explode(':',$total_working_hours[0]['working_hours']);                         
        $work_hours = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as work_hour")
                            ->whereMonth('time','=',date('m'))
                            ->whereYear('time','=',date('Y'))
                            ->where('user_id','=',Auth::id())
                            ->get()
                            ->toArray();

        $exp_work_hours = explode(':',$work_hours[0]['work_hour']);
       
        $time_differance = Log::selectRaw("TIME_FORMAT(MIN(check_in), '%h:%i:%s') as check_in,TIME_FORMAT(MAX(check_out), '%h:%i:%s') as check_out,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_difference")
                ->where('time','=',date('Y-m-d'))
                ->where('user_id','=',Auth::id())
                ->get()
                ->toArray();       
               
        $logs = Log::select(DB::raw("DATE_FORMAT(check_in,'%h:%i %p') as check_in") , DB::raw("DATE_FORMAT(check_out,'%h:%i %p') as check_out") , 'time')->where('user_id', Auth::id())
            ->where('time', date('Y-m-d'))
            ->orderBy('id', 'DESC')
            ->first();
        $count_log_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count(); 
        $total_leaves = Leave::where('user_id',Auth::id())
                        ->whereYear('date','=',date('Y'))
                        ->where('status','=','a')
                        ->limit(5)
                        ->orderBy('id','desc')
                        ->get()
                        ->toArray();
                 //  dd($total_leaves);    
        $casual_leaves = Leave::where('leave_type','=','Casual Leave') 
                        ->where('user_id',Auth::id())
                        ->where('status','a')
                        ->whereYear('date','=',date('Y'))
                        ->count();
        
        $sick_leaves = Leave::where('leave_type','=','Sick Leave') 
                        ->where('user_id',Auth::id())
                        ->where('status','a')
                        ->whereYear('date','=',date('Y'))
                        ->count(); 
        $priviledge_leaves = Leave::where('leave_type','=','Privilege Leave') 
                        ->where('user_id',Auth::id())
                        ->where('status','a')
                        ->whereYear('date','=',date('Y'))
                        ->count();
        $checkin_time = Log::where('time','=',date('Y-m-d'))
                        ->where('user_id','=',Auth::id())
                        ->orderBy('check_in','desc')
                        ->first();
        $checkin_empty = Log::where('time','=',date('Y-m-d'))
                        ->where('user_id','=',Auth::id())
                        ->where('check_in','<>','')
                        ->where('check_out','=',NULL)
                        ->orderBy('check_in','desc')
                        ->first();
         $pending_leave = Leave::where('user_id',Auth::id())
                        ->whereYear('date','=',date('Y'))
                        ->where('status','=','p')
                        ->limit(3)
                        ->orderBy('id','desc')
                        ->get()
                        ->toArray();                           
       $holiday = HolidayYear::where('holiday_year.holiday_date','>=',date('Y-m-d'))
                        ->leftjoin('holiday_master','holiday_master.id','=','holiday_year.holiday_id')   
                        ->orderBy('holiday_year.holiday_date','asc')
                        ->limit(3)
                        ->get()
                        ->toArray();
   
        $month_back_date = date('Y-m-d',strtotime('-30 days'));   
        $new_employee = User::where('created_at','>=',$month_back_date)->get()->toArray();
        
        $current_date = date('Y-m-d');
        $date = new Carbon\Carbon;
        $today = $date->format('d');
        $month = $date->format('m');
        $birthday_members = User::select('position', 'fullname','birthdate','profile_pic')
                        ->where('is_verified',1)
                        ->whereDay('birthdate', '>=', date('m-d'))
                        ->orderBy(DB::raw('CONCAT(SUBSTR(`birthdate`,6) < SUBSTR(CURDATE(),6), SUBSTR(`birthdate`,6))'))
                        ->limit(3)
                        ->get()
                        ->toArray();
        //logs and leaves graph  

        $missing_logs = DB::select(DB::raw("SELECT SUM(q.timeSum) AS count,q.month_main,q.year,q.month,q.user_id FROM (SELECT (CASE WHEN IFNULL(SUM( TIME_TO_SEC( time_diff ) ),0) < 32400 THEN 1 ELSE 0 END)  AS timeSum ,DATE_FORMAT(`time`,'%Y-%m') AS month_main,YEAR(`time`) AS YEAR,MONTH(TIME) AS MONTH,user_id AS user_Id FROM `logs` WHERE user_id = ".Auth::id()." GROUP BY  `time`) q GROUP BY q.month_main  ORDER BY q.month_main DESC limit 6"));
                                        
        $leaves_data = DB::table("leaves")
                        ->selectRaw('COUNT(*) as count,user_id,MONTH(date) as month,YEAR(date) as year')
                        ->where('user_id','=',Auth::id())
                        ->groupby(DB::raw('MONTH(date)'))
                        ->orderBy(DB::raw('MONTH(date)'), 'desc')->take(6)
                        ->get();  

        $logs_data = json_decode(json_encode($missing_logs),true);
        $leaves_data = json_decode(json_encode($leaves_data),true);
        $count_logs = [];
        $month = [];

        $last_six_months = [];
        $old = [];
        $new = [];
        $leaves_new = [];
        $leaves_old = [];
        $count_logs = [];
        $count_leaves = [];

        for ($i = 0; $i <= 5; $i++) {
          $last_six_months[date('m', strtotime("-$i month"))] = date('Y', strtotime("-$i month"));
          // $last_six_months[][date(', Y', strtotime("-$i month"))] = date(', Y', strtotime("-$i month"));
        }
        // dd($last_six_months);
        $count = 0;
        foreach ($last_six_months as $last_six_months_key => $last_six_months_value) {
            foreach ($logs_data as $array_data_key => $array_data_value) {
                if($last_six_months_key == $array_data_value['month'])
                {
                    $old[$count]['count'] = $array_data_value['count'];
                    $old[$count]['user_id'] = $array_data_value['user_id'];
                    $old[$count]['month'] = $array_data_value['month'];
                    $old[$count]['year'] = substr($array_data_value['year'],-2);
                }else{

                    $new[$count]['count'] = 0;
                    $new[$count]['user_id'] = $array_data_value['user_id'];
                    $new[$count]['month'] = $last_six_months_key;
                    $new[$count]['year'] =  substr($last_six_months_value,-2);
                }
            }
            foreach ($leaves_data as $array_data_key => $array_data_value) {
                if($last_six_months_key == $array_data_value['month']){
                    $leaves_old[$count]['count'] = $array_data_value['count'];
                }else{
                    $leaves_new[$count]['count'] = 0;
                }
            }
            $count++;
        }

        $logs_data = array_replace_recursive($new,$old);
        $leaves_data = array_replace_recursive($leaves_new,$leaves_old);
        krsort($logs_data);
        krsort($leaves_data);
        foreach ($logs_data as $logs_key => $logs_value) {
            $count_logs[] = $logs_value['count'];
            $dateObj = \DateTime::createFromFormat('!m', $logs_value['month']);
            $month[] = "'".substr($dateObj->format('F'),0,3).' '.$logs_value['year']."'";
        }
        foreach ($leaves_data as $logs_key => $logs_value) {
            $count_leaves[] = $logs_value['count'];
        }
        $count_logs = implode(',',$count_logs);
        $month = implode(',',$month);
        $count_leaves = implode(',',$count_leaves);
        
        //logs graph
        $leaves_log = DB::table("leaves")
            ->selectRaw('COUNT(*) as count,user_id,users.fullname as name,users.is_verified')
            ->join('users','users.id','=','leaves.user_id')
            ->where('users.is_verified','=','1')
            ->where('users.deleted_at','=',NULL)
            ->groupby(DB::raw('user_id'))
            ->get();  

        $leaves_log1 = json_decode(json_encode($leaves_log),true);
        foreach ($leaves_log1 as $leaves_log_key => $leaves_log_value) {
            $leaves_count_logs [] = $leaves_log_value['count'];
            $fullname [] = $leaves_log_value['name'];
            $words = explode(" ", $leaves_log_value['name']);
            $username_leaves = "";
            $count = 0;
            foreach ($words as $w) {
                if($count<2){
                    $username_leaves .= $w[0];
                }
                $count++;
            }
            $username[] = '"'.$username_leaves.' '.$leaves_log_value['user_id'].'"';
        }
        $leaves_count_logs = implode(',',$leaves_count_logs);
        $username = implode(',',$username);

        return view('dashboard.admin', compact('logs', 'leaves', 'holiday', 'birthday_data','new_employee','casual_leaves','sick_leaves','priviledge_leaves','total_leaves','checkin_time','checkin_empty','count_log_row','time_differance','pending_leave','count_logs','month','count_leaves','leaves_count_logs','username','fullname','birthday_members','exp_total_hours','exp_work_hours'));
    }
    public function getChart(Request $request){
        $data = $request->all();
        $user_id = $data['id'];
        $username = User::where('id',$user_id)->first();
        $fullname = $username['fullname'];
        $leaves_data = DB::table("leaves")
            ->selectRaw('COUNT(*) as count,user_id,YEAR(date) as year')
            ->where('user_id',$user_id)
            ->groupby(DB::raw('YEAR(date)'))
            ->orderBy(DB::raw('YEAR(date)'), 'desc')->take(6)
            ->get();  
        $leaves_data = json_decode(json_encode($leaves_data),true);
        for ($i = 0; $i <= 5; $i++) {
          $last_six_years[] = date('Y', strtotime("-$i year"));
        }

        foreach ($last_six_years as $last_six_years_key => $last_six_years_value) {
            foreach ($leaves_data as $array_data_key => $array_data_value) {
                    if($last_six_years_value == $array_data_value['year']){
                        $leaves_old[$last_six_years_key]['count'] = $array_data_value['count'];
                        $leaves_old[$last_six_years_key]['user_id'] = $array_data_value['user_id'];
                        $leaves_old[$last_six_years_key]['year'] = $array_data_value['year'];
                    }else{
                        $leaves_new[$last_six_years_key]['count'] = 0;
                        $leaves_new[$last_six_years_key]['user_id'] = $user_id;
                        $leaves_new[$last_six_years_key]['year'] = $last_six_years_value;
                    }
            }
        }

        $leaves_data = array_replace_recursive($leaves_new,$leaves_old);
        ksort($leaves_data);
        
        foreach ($leaves_data as $leaves_key => $leaves_value) {
            $count_leaves[] = $leaves_value['count'];
            $year[] = (string) $leaves_value['year'];
        }
        // dd($year);
        return response()->json(['count_leaves'=>$count_leaves,'year'=>$year,'fullname'=>$fullname]);
    }
    public function getSecondChart(Request $request){
        $data = $request->all();
        $leaves_data = DB::table("leaves")
            ->selectRaw('COUNT(*) as count,user_id,MONTH(date) as month,YEAR(date) as year')
            ->where('user_id',$data['id'])
            ->where(DB::raw('YEAR(date)'),$data['year'])
            ->groupby(DB::raw('MONTH(date)'))
            ->get();   
        $leaves_data = json_decode(json_encode($leaves_data),true);

        for ($i = 1; $i <= 12; $i++) {
          $last_six_months[] = $i;
        }
        // dd($last_six_months);
        $count = 0;
        foreach ($last_six_months as $last_six_months_key => $last_six_months_value) {
            foreach ($leaves_data as $array_data_key => $array_data_value) {
                if($last_six_months_value == $array_data_value['month']){
                    $leaves_old[$count]['count'] = $array_data_value['count'];
                    $leaves_old[$count]['month'] = $last_six_months_value;
                    $leaves_old[$count]['year'] = $data['year'];
                }else{
                    $leaves_new[$count]['count'] = 0;
                    $leaves_old[$count]['month'] = $last_six_months_value;
                    $leaves_old[$count]['year'] = $data['year'];
                }
            }
            $count++;
        }
        $leaves_data = array_replace_recursive($leaves_new,$leaves_old);

        foreach ($leaves_data as $leaves_key => $leaves_value) {
            $count_leaves[] = $leaves_value['count'];
            $dateObj = \DateTime::createFromFormat('!m', $leaves_value['month']);
            $month[] = substr($dateObj->format('F'),0,3).' '.substr($leaves_value['year'],-2);
        }
        $username = User::select('fullname')->where('id',$data['id'])->first()->toArray();
        $username = $username['fullname'];

        return response()->json(['count_leaves'=>$count_leaves,'month'=>$month,'fullname'=>$username]);

    }
    public function edit($id)
    {
        $users = User::find($id);
        $last_logs_date = Log::select('time')->where('user_id',$id)->orderby('time','desc')->first();

        return view('user.edit',compact('users','last_logs_date'));
    }

    public function update(Request $request, $id)
    {
        $users = User::find($id);
        $data = $request->all();
        $this->validate($request,[
            'profile_pic' => 'image',
            'gender' => 'required',
            'fullname' => 'required|regex:/^[a-zA-Z .]*$/',
            'position' => 'required',
            'birthdate' => 'required',
            'email' => 'required|regex:/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]{5,}+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/',
            'contact' => 'required|regex:/^[- +()]*[0-9][- +()             0-9]*$/|max:15',
            'emergency_contact' => 'required|regex:/^[- +()]*[0-9][-+()0-9]*$/|max:15',
            'address' => 'required',
            'joining_date'=>'required',
            'linkedin'=>'required'
        ]);

        $users->fill($request->all());
        if(!empty($data['serving_date'])){
            $users->is_verified = 0;
        }else{
            $users->is_verified = 1;   
        }
        $users->save();
        // dd($data);
        if($data['save_button'] == "save_exit"){
            return redirect()->route('user.index',['type'=>'currentemp'])->with('message_type','success')
                     ->with('message','User Updated Successfully');
        }
        return back()->with('message_type','success')
                     ->with('message','User Updated Successfully');

    }

    public function trash(Request $request){
        $id = $request->get('id');
        
        $user = User::where('id',$id)->delete();

        return back()->with('message','User Deleted Successfully')
                     ->with('message_type','success');
    }

    public function restore(Request $request,$id){

        User::withTrashed()->where('id',$id)->restore();

        return back()->with('message_type','success')
                     ->with('message','User Successfully Restore');
    }
    public function destroy(Request $request,$id)
    {
        dd($id);
        User::withTrashed()->where('id',$id)->forceDelete();

        return back()->with('message','User Successfully Deleted')
                     ->with('message_type','success');
    }
    public function recyclebin(Request $request){

        if($request->ajax()){
            $where_str = "1 =?";
            $where_param =array('1');

                if($request->has('search.value')){
                     $search = $request->get('search.value');
                     $where_str .= "and (fullname like \"%{$search}%\""
                                ." or profile_pic like \"%{$search}%\""
                                ." or position like \"%{ $search}%\""
                                .")";
                }
            $columns = array('id','fullname','profile_pic','position','email');

            $sort_columns = array('id','fullname','position');

            $users_count = User::select('id')
                        ->onlyTrashed()->whereRaw($where_str,$where_param)
                        ->count();

            $users = User::select($columns)
                    ->onlyTrashed()->whereRaw($where_str,$where_param);
            if($request->has('iDisplayStart') && $request->get('iDisplayLenghth') !='-1')
            {
                $users = $users ->take($request->get('iDisplayLenghth'))
                                ->skip($request->get('iDisplayStart'));
            }
            if($request->has('iSortCol_0'))
            {
                $sql_order = '';
                for ( $i = 0 ; $i < $request->get('iSortingCols') ; $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                        if(false !== ($index = strpos($column, ' as ')))
                        {
                            $column = substr($column, 0, $index);
                        }
                        $users = $users->orderBy($columns,$request->get('sSortDir_'.$i));
                }
            }
            $users = $users->get();

            $response['iTotalDisplayRecords'] =$users_count;
            $response['iTotalRecords'] = $users_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $users->toArray();

            return $response;
        }

        return view('user.recycle');
    }
    //for upload photo
    public function getUploadProfilePhoto(Request $request)
    {
        $id = '';
        if(isset($request->id)){
            $id = $request->id;
        }
        return view('user.upload-photo',['id'=>$id]);
    }

    public function postUploadProfilePhoto(Request $request)
    {
        $data = $request->all();
        $profile = User::findornew($data['id']);

        $rules = ['profile_pic' => 'required|mimes:jpeg,jpg,png|image|max:4096'];

        $validator = Validator::make($request->all(), $rules);

        if ( $validator->passes() )
        {
            $microtime = microtime();
            $search = array('.',' ');
            $microtime = str_replace($search, "_", $microtime);
            $photo = $microtime . '.' . $request->file('profile_pic')->getClientOriginalExtension();
            if(!empty($data['id'])){
                $path = public_path()."/upload/";
            }else{
                $path = public_path()."/temp_image/";
            }

            $old_file = $path . $profile->photo;

            list($width, $height, $type, $attr) = getimagesize($request->file('profile_pic'));

            $save_w = $width;
            $save_h = $height;

            $request->file('profile_pic')->move($path, $photo);

            if ( $save_w >= 500 || $save_h >= 500 )
            {
                $resizeObj = new ImageResize($path . $photo);
                $resizeObj->resizeImage(300, 200, 0);
                $resizeObj->saveImage($path . $photo, 100);
            }
            if(!empty($data['id'])){
                $profile->temp_photo = $photo;
                $profile->save();
            }else{
                $profile = $photo;
            }

            return redirect()->route('user.profile.photo.crop.get',['id'=>$data['id'],'profile'=>$profile]);
        }
        else
        {
            return back()->withErrors($validator);
        }
    }

    public function getCropProfilePhoto(Request $request)
    {
        $data = $request->all();

        if($data['id'] == ''){
            $profile['temp_photo'] = $data['profile'];
        }else{
            $profile = User::find($data['id']);
        }

        return view('user.crop-photo',['profile'=>$profile,'id'=>$data['id']]);
    }

    public function postCropProfilePhoto(Request $request) {
        $data = $request->all();

        if(!empty($data['id'])){

            $profile = User::find($data['id']);

            $photo = $profile->temp_photo;

            $old_photo = $profile->profile_pic;
            $public_path = str_replace("\\","/", public_path());

            $save_folder =  $public_path . '/upload/';
            $photo_file = $save_folder . '/' . $photo;

            if ($old_photo != NULL AND $old_photo != '') {
                $old_photo_path = $save_folder . '/' . $old_photo;

                @unlink($old_photo_path);
            }

            list($width, $height, $type, $attr) = getimagesize($photo_file);

            $save_w = $width;
            $save_h = $height;

            $crop_x = $request->get('x');
            $crop_y = $request->get('y');
            $crop_w = $request->get('w');
            $crop_h = $request->get('h');

            $resizeObj = new ImageResize($photo_file);
            $resizeObj->cropCustomized($crop_x, $crop_y,$crop_w, $crop_h, $crop_w, $crop_h);
            $outputfile = $save_folder . $photo;

            $resizeObj->saveImage($outputfile, 100);

            $profile->profile_pic = $photo;
            $profile->save();

            return Redirect::route('show.edit')->with('message','Your Profile Photo Updated Successfully')
                                               ->with('message_type','success');
        }else{
            if(\Session::has('image_name')){
                $save_folder =  public_path() . '/temp_image/';
                $old_photo_path = $save_folder.trim(\Session::get('image_name'));
                @unlink($old_photo_path);
            }

            $profile = $data['image_name'];

            $public_path = str_replace("\\","/", public_path());
            $save_folder =  $public_path . '/temp_image/';
            $photo_file = $save_folder .trim($profile);

            $crop_x = $data['x'];
            $crop_y = $data['y'];
            $crop_w = $data['w'];
            $crop_h = $data['h'];

            $resizeObj = new ImageResize($photo_file);
            $resizeObj->cropCustomized($crop_x, $crop_y,$crop_w, $crop_h, $crop_w, $crop_h);
            $outputfile = $save_folder . trim($profile);

            $resizeObj->saveImage($outputfile, 100);

            \Session::put('image_name',$profile);

            return view('user.create');
        }   
    }
    public function userDeactive(Request $request){
        $this->validate($request,[
            'date' => 'required',
            'note' => 'required',
        ]);
        $data = $request->all();
        $user = User::find($data['id']);
        $user->is_verified = '0';
        $user->deactive_date = $data['date'];
        $user->note = $data['note'];
        $user->save();

        return response()->json('User Deactivated Successfully');
    }
    //for dashboard
    public function leavestore(Request $request)
    {
      
        $firststep_store = $request->formData;
        // dd($firststep_store);
        $date_of_leave = explode(',', $firststep_store['date']);
        $array_count_value = count($date_of_leave);
        $firstform_date = Session::put('firstform_date',$date_of_leave);  
        // $session_data = json($firstform_date);        
        $date = Session::put('date',$firststep_store['date']);
        $first_leavetype = Session::put('leave_type',$firststep_store['leave_type']);
        $first_leavepurpose = Session::put('leave_purpose',$firststep_store['leave_purpose']);
        $get_leavetype = Session::get('leave_type');
        $get_leavepurpose = Session::get('leave_purpose');
        // dd($get_leavetype);
        $content = View::make('dashboard.leave_form', compact('date_of_leave','get_leavetype','get_leavepurpose'))->render();

        return response()->json(['success'=>true,'html_content'=>$content,'firststep_store'=>$firststep_store,'date_of_leave'=>$date_of_leave]);
  
    }
    public function leavesecondstore(Request $request)
    {
        
        $requestAll = $request->all();
        $leave_detail = $request['leave_detail'];

        foreach ($leave_detail as $key => $value) {
            // dd($value);
            if(isset($value['day_type']))
            {
                $validator = Validator::make($value, [
                'leave_purpose' => 'required',
                 ]);
                if($validator->fails())
                {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            }else
            {
                $validator = Validator::make($value, [
                'from_hour' => 'required',
                'to_hour' => 'required',    
                'leave_purpose' => 'required',
                 ]);
                if($validator->fails())
                {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }
            } 
        }
        $session_put_data = Session::put('leave_detail',$leave_detail);
        
        return response()->json(['success'=>true,'leave_detail'=>$leave_detail]);

    }
    public function leavethirdstore(Request $request)
    {
        $step2_session = Session::get('leave_detail');
        if(!empty($step2_session))
        {
            $leave_confirmation = []; 
            foreach ($step2_session as $key => $value) {
        
                $leaves = new Leave;
                $leaves->user_id = Auth::id();
                $leaves->date = $key;
                $leaves->leave_type = $value['leave_type'];
                if($value['from_hour'] == "10:00 AM" && $value['to_hour'] == "07:00 PM")
                {
                    $leaves->leave_time = "full day";
                     $leaves->to_hour = $value['to_hour'];
                     $leaves->from_hour = $value['from_hour'];
                    
                }else
                {
                    $leaves->from_hour = $value['from_hour'];
                    $leaves->to_hour = $value['to_hour'];
                    $leaves->leave_time = "half day";
                }
                $leaves->leave_purpose = $value['leave_purpose'];
                
                $leaves->save();    
            }
             
        }else
        {
        $firstform_date = Session::get('firstform_date');
        $first_leavetype = Session::get('leave_type');
        $first_leavepurpose = Session::get('leave_purpose');
             //dd($firstform_date);
        $leave_confirm = [];
        foreach ($firstform_date as $key => $value) {

                $leave_confirm[$key] = $value;
                $leaves = new Leave;
                $leaves->user_id = Auth::id();
                $leaves->date = $value;
                $leaves->leave_type = $first_leavetype;
                $leaves->leave_time = "full day";
                $leaves->leave_purpose = $first_leavepurpose;
                
                $leaves->save();    
            }
            

         } 
       
         return response()->json(['success'=>true,'request'=>$request->all()]);
    }
    public function checkIn(Request $request)
    {
    
        $current_time = date("H:i:s");

        $previous_checkin = Log::where('time','=',date('Y-m-d'))
                           ->where('user_id','=',Auth::id())
                           ->count();     
        $check_out =  Log::where('time','=',date('Y-m-d'))
                           ->where('user_id','=',Auth::id())
                           ->where('check_out','=',NULL)
                           ->count();
                                       
        if($previous_checkin == 0)
        {

        $checkin = new Log;
        $checkin->user_id = Auth::id();
        $checkin->time = date('Y-m-d');
        $checkin->check_in = $current_time;
        $checkin->save();
        $count_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count();
        return response()->json(['success'=>true,'message'=>'first time Check in successfully','check_in'=>$checkin,'count_row'=>$count_row]);
        }else if($previous_checkin > 0)
        {
            $checkin_count = Log::where('time','=',date('Y-m-d'))
                        ->where('user_id','=',Auth::id())
                        ->where('check_in','<>','')
                        ->where('check_out','<>','')
                        ->count();

           if($checkin_count > 0)
           {
                 $check_in_count = Log::where('time','=',date('Y-m-d'))
                                ->where('user_id','=',Auth::id())
                                ->where('check_in','<>','')
                                ->where('check_out','=',NULL)
                                ->count();

                if($check_in_count > 0)
                {
                    $checkin = Log::where('time','=',date('Y-m-d'))
                                ->where('user_id','=',Auth::id())
                                ->where('check_in','<>','')
                                ->where('check_out','=',NULL)->first();
                    
                       // dd($checkin);         
                    $check = Log::find($checkin['id']); 
                    $interval = \DB::select("select TIMEDIFF('".$current_time."','".$checkin['check_in']."') AS `interval`");
                    $check->check_out = $current_time;
                    $check->time_diff = $interval[0]->interval;
                    $check->save();
                    $count_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count();
                    $checkout_time_count =  Log::selectRaw("TIME_FORMAT(MIN(check_in), '%H:%i:%s') as check_in,TIME_FORMAT(MAX(check_out), '%H:%i:%s') as check_out,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_difference")
                    ->where('time','=',date('Y-m-d'))
                    ->where('user_id','=',Auth::id())
                    ->get()
                    ->toArray();
                    $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")->whereMonth('working_date','=',date('m'))
                                 ->where('year','=',date('Y'))
                                 ->get()
                                 ->toArray();
                    $exp_total_hours = explode(':',$total_working_hours[0]['working_hours']);             
                    $work_hours = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as work_hour")
                            ->whereMonth('time','=',date('m'))
                            ->whereYear('time','=',date('Y'))
                            ->where('user_id','=',Auth::id())
                            ->get()
                            ->toArray();
                    $exp_work_hours = explode(':',$work_hours[0]['work_hour']);           

                    return response()->json(['success'=>true,'message'=>'second Checkout in successfully','check_out'=>$check,'count_row'=>$count_row,'checkout_time_count'=>$checkout_time_count,'exp_total_hours'=>$exp_total_hours,'exp_work_hours'=>$exp_work_hours]);

                }else
                {
                 $checkin = new Log;
                 $checkin->user_id = Auth::id();
                 $checkin->time = date('Y-m-d');
                 $checkin->check_in = $current_time;
                 $checkin->save();
                 $count_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count();
                 $checkout_time_count =  Log::selectRaw("TIME_FORMAT(MIN(check_in), '%H:%i:%s') as check_in,TIME_FORMAT(MAX(check_out), '%H:%i:%s') as check_out,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_difference")
                    ->where('time','=',date('Y-m-d'))
                    ->where('user_id','=',Auth::id())
                    ->get()
                    ->toArray();                      
                 return response()->json(['success'=>true,'message'=>'second time Check in successfully','check_in'=>$checkin,'count_row'=>$count_row,'checkout_time_count'=>$checkout_time_count]);        
                }    
            }else
            {
            

                $checkin = Log::where('time','=',date('Y-m-d'))
                                ->where('user_id','=',Auth::id())
                                ->where('check_in','<>','')
                                ->where('check_out','=',NULL)
                                ->first()
                                ->toArray();
                $check = Log::find($checkin['id']); 
                $interval = \DB::select("select TIMEDIFF('".$current_time."','".$checkin['check_in']."') AS `interval`");
                $check->check_out = $current_time;
                $check->time_diff = $interval[0]->interval;
                $check->save();
                $count_row = Log::where('time','=',date('Y-m-d'))
                                       ->where('user_id','=',Auth::id())
                                       ->count();
                $checkout_time_count =  Log::selectRaw("TIME_FORMAT(MIN(check_in), '%H:%i:%s') as check_in,TIME_FORMAT(MAX(check_out), '%h:%i:%s') as check_out,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_difference")
                    ->where('time','=',date('Y-m-d'))
                    ->where('user_id','=',Auth::id())
                    ->get()
                    ->toArray();
                $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")->whereMonth('working_date','=',date('m'))
                                 ->where('year','=',date('Y'))
                                 ->get()
                                 ->toArray();
                    $exp_total_hours = explode(':',$total_working_hours[0]['working_hours']);             
                    $work_hours = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as work_hour")
                            ->whereMonth('time','=',date('m'))
                            ->whereYear('time','=',date('Y'))
                            ->where('user_id','=',Auth::id())
                            ->get()
                            ->toArray();
                    $exp_work_hours = explode(':',$work_hours[0]['work_hour']); dd($exp_work_hours);                           
                return response()->json(['success'=>true,'message'=>'first Checkout in successfully','check_out'=>$check,'count_row'=>$count_row,'checkout_time_count'=>$checkout_time_count,'exp_total_hours'=>$exp_total_hours,'exp_work_hours'=>$work_hours]);
            }
        }
    }
}
