<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\User;
use App\Models\UserPermission;
use Validator;

class UserPermissionController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::lists('fullname','id')->toArray();

        $permissions = Permission::select()->get()->toArray();

        $secPermissions = [];

        foreach($permissions as $key => $value) {
            $secPermissions[$value['section']][] = ['description' => $value['description'],'id' => $value['id']] ;
        }

        return view('permission.create', compact('permissions','users','secPermissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_id = $request->user_id;
        $permissions = $request->permissions;

        $rules = [
			'user_id' => 'required',
			'permissions' => 'required',
		];
        $messages = array(
        'user_id.required' => 'The User field is required.',
        );

		$validator = Validator::make($request->all(),$rules,$messages);

		if ($validator->fails()) {
			return back()->withInput()
							   ->withErrors($validator->errors());
							  // ->with('message', 'Unable to add details.')
							   //->with('message_type', 'danger');
		}

        UserPermission::where(['user_id' => $user_id])->delete();

        foreach ($permissions as $permission_id) {

            UserPermission::create(array(
                'user_id' => $user_id,
                'permission_id' => $permission_id
            ));
        }
        return back()->with('message', 'User Permission Changed/Added Successfully')
                     ->with('message_type', 'success');;
    }

    public function getSelectedPermissions(Request $request)
    {
        $id = $request->user_id;

        $user = User::find($id);

        if( ! $user) {
            return response()->json(['success' => false],400);
        }

        $userPermissions = UserPermission::where('user_id',$id)->get()->toArray();

        return response()->json(['success' => true,'user_permissions' => $userPermissions],200);
    }

}
