<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Configuration;
use App\Models\LocationDetails;
use Log,Mail;

class UserPasswordController extends Controller
{

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
		$rules = [
			'email' => 'required|email',
		];

	    $validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->createMessageResponse(true,'Reset Link sent successfully, check you mail',200);

            case Password::INVALID_USER:
                return $this->createMessageResponse(false,'Invalid User',400);
        }
    }

    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function getReset($token = null)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('api.reset')->with('token', $token);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $rules = [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {

            return redirect()->back()
                             ->withInput()
                             ->withErrors($validator->errors())
                             ->with('message_type','danger')
                             ->with('message','There Were Some Error Try Again');
        }

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::INVALID_USER:
                return Redirect()->back()->with('message_type','danger')
                                 ->with('message','Invalid User');
            case Password::INVALID_TOKEN:
                return Redirect()->back()->with('message_type','danger')
                                 ->with('message','Invalid Token');
            case Password::PASSWORD_RESET:
                return Redirect()->back()->with('message_type','success')
                                 ->with('message','Password successfully changed');
            default:
                return redirect()->back()
                            ->withInput($request->only('email'))
                            ->withErrors(['email' => trans($response)]);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);

        $user->save();
        //Auth::user()->login($user);
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }

    public function DatabaseInsert(Request $request){
        $data = $request->get('arr');

        foreach ($data as $key => $value) {
            $LocationDetails_dave = new LocationDetails();

            $LocationDetails_dave->device_id = $value['device_id'];
            $LocationDetails_dave->latitude = $value['latitude'];
            $LocationDetails_dave->longitude = $value['longitude'];
            $LocationDetails_dave->time_mili = $value['timeMili'];
            $LocationDetails_dave->save();
        }

        return response()->json(['success'=>true],200);
    }

    public function LocationGet(Request $request)
    {
        $device_id = $request->get('device_id');
        $start_at = $request->get('start_at');
        $end_at = $request->get('end_at');

        $location_list = LocationDetails::where('device_id',$device_id)
                                            ->where('time_mili','>=',$start_at)
                                            ->where('time_mili','<=',$end_at)
                                            ->orderBy('time_mili','asc')
                                            ->get();

        if (count($location_list) && !empty($location_list)) {

            $location_list = $location_list->toArray();

            return response()->json(['success'=>true,'location'=>$location_list],200);
        }
        else
        {
            $location_list = LocationDetails::where('device_id',$device_id)
                                            ->where('time_mili','<=',$start_at)
                                            ->orderBy('time_mili','desc')
                                            ->first()->toArray(); 

            $arr[] = $location_list;
            
            return response()->json(['success'=>true,'location'=>$arr],200);
        }                                            

        return response()->json(['success'=>false,'location'=>'No Location Found'],200);
    }

    public function LocationCurrent(Request $request){
        $device_id = $request->get('device_id');
        $id = $request->get('id');

        $location_list = LocationDetails::where('device_id',$device_id)
                                            ->where('id','>',$id)
                                            ->orderBy('time_mili','asc')
                                            ->get();

        if (count($location_list) && !empty($location_list)) {
            $location_list = $location_list->toArray();

            return response()->json(['success'=>true,'location'=>$location_list],200);
        }                                            

        return response()->json(['success'=>false,'location'=>'No Location Found'],200);
    }

    public function LatestLocation(Request $request)
    {
        $device_id = $request->get('device_id');

        $date = date('Y-m-d');
        
        $latest_location = LocationDetails::select('created_at')->where('device_id',$device_id)->orderBy('created_at','desc')->first();

        $latest_date = $latest_location->created_at->format('Y-m-d');
        
        if($date == $latest_date)
        {
            $latest_location = LocationDetails::where('device_id',$device_id)->orderBy('created_at','desc')->first();            
            
            return response()->json(['success'=>true,'latest_location'=>$latest_location],200);
        }
        return response()->json(['success'=>false,'location'=>'No Location Found'],200);
    }

    public function LocationPush(Request $request)
    {
        $request_new = json_decode($request->location_array);
                
        foreach ($request_new as $storeData) {
        
            foreach ($storeData as $key) {
             
            $location_data = new LocationDetails();
            
            $location_data->device_id = $key->device_id;    
            $location_data->latitude = $key->latitude;    
            $location_data->longitude = $key->longitude;    
            $location_data->time_mili = $key->timeMili;

            $location_data->save();
        
            }

        }
        return response()->json(['success'=>true],200);

    }

    public function ConfigurationPush(Request $request){
        
        $configuration_data = Configuration::firstOrNew(['device_id'=>$request->get('device_id')]);
        $configuration_data->fill($request->all());
        $configuration_data->save();

        return response()->json(['success'=>true],200);
    }

    public function ConfigurationGet()
    {
        $device_details = Configuration::get()->toArray();

        return response()->json(['success'=>true,'device_details'=>$device_details],200);
    }
    public function EditUser(Request $request)
    {
       
        $device_id = $request->get('device_id');
        $name = $request->get('name');
        $mobile_no = $request->get('mobile_no');
        
        $update_user = Configuration::where('device_id',$device_id)->first();
        $update_user->name = $name;
        $update_user->mobile_no = $mobile_no;
        $update_user->save();
                                    
         return response()->json(['success'=>true,'update_user'=>$update_user],200);                            
    }
    
}
