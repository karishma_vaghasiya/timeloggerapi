<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Holiday;
use App\Http\Controllers\Controller;

class HolidayController extends Controller
{
	public function index(Request $request)
	{
		$holidays = Holiday::all();

		foreach ($holidays as $holiday) {
			$holiday->date = date("d-m-Y", strtotime($holiday->date));
		}

		return $this->createOnlyDataResponse(true,$holidays,200);
	}
}
