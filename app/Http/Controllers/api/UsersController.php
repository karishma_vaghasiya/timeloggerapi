<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Log;
use App\Models\Uuid;
use App\Models\Setting;
use App\Models\Reminder;
use App\Models\SettingInfo;
use App\Models\NewReminder;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use DB;

class UsersController extends Controller
{
	public function login(Request $request)
	{
		$rules = [
			'email' => 'required|email',
			'password' => 'required',
			'uuid' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails login fail',$validator->errors(),400);
		}

		$userExists = User::where('email', $request->email)->count();

		if( ! count($userExists)) {
			return $this->createMessageResponse(false,'Username or Password not match, login failed',400);
		}

		$user = User::where('email',$request->email)->first();

		if( ! (Hash::check($request->password, $user['password']) )) {
			return $this->createMessageResponse(false,'Username or Password not match, login failed',400);
		}

		if($user->is_verified == 0) {
			return $this->createMessageResponse(false,'User is not verified, login failed',400);
		}

		$logs = Log::selectRaw("DATE_FORMAT(check_in, '%h:%i %p') as check_in,DATE_FORMAT(check_out,'%h:%i %p') as check_out")->where('time',date('Y-m-d'))->where('user_id',$user['id'])->orderBy('id','DESC')->get()->toArray();

		if ( count($logs) > 0 ) {
			$user['logs'] = $logs[0];
		}

		$setting = Setting::take('1')->orderBy('id','DESC')->get()->toArray();

		if ( count($setting) > 0 ) {
			$user['setting'] = $setting[0];
		}

		$reminder = Reminder::where('user_id',$user['id'])->orderBy('id','DESC')->get()->toArray();

		$newreminder = NewReminder::where('user_id',$user['id'])->orderBy('id','DESC')->get()->toArray();
		// dd($newreminder);
		if ( count($newreminder) > 0 ) {
			$user['new_reminder'] = $newreminder[0];
		}


		if ( count($reminder) > 0 ) {
			$user['reminder'] = $reminder[0];
		}

		$uuid = Uuid::firstOrCreate(['user_id' => $user->id,'uuid' => $request->uuid]);

		$company = Company::where('id',$user->company_id)->first();
		
		$setting = SettingInfo::select('lat','long','wifi_info')->where('company_id',$user->company_id)->get()->toArray();

		$user['company'] = $company;
		$user['setting'] = $setting;

		$user['birthdate'] = date("d-m-Y", strtotime($user['birthdate']));
		$user['profile_pic'] = url('upload').'/'.$user['profile_pic'];

		return $this->createDataResponse(true,'Login Successfully Done',$user,200);
	}

	public function store(Request $request)
	{
		$data = $request->all();

		$rules = [
			'gender' => 'required|in:male,female',
			'fullname' => 'required',
			'position' => 'required',
			'birthdate' => 'required|date',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|confirmed',
			'password_confirmation' => 'required',
			'contact' => 'required|unique:users,contact',
			'emergency_contact' => 'required',
			'address' => 'required'
		];

		$messages = [
			'gender.in' => 'gender should be male or female'
		];

		$validator = Validator::make($request->all(),$rules,$messages);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails user not created',$validator->errors(),400);
		}

		$email = explode('@', $data['email'])[1];

		$company = Company::where('domain',$email)->first();

		if(! $company) {
			return $this->createMessageResponse(false,'Company does not exist.',400);
		}

		$path = app()->basePath('public/uploads');

        if ( !file_exists($path) ) {
            mkdir($path, 0777);
        }

        $confirm_code = str_random(30);

        $data['birthdate'] = date("Y-m-d", strtotime($data['birthdate']));
        $data['password'] = Hash::make($data['password']);
        $data['confirm_code'] = $confirm_code;
        $data['company_id'] = $company->id;

		$user = new User($data);
		$user->save();

		Mail::send('emails.verify', ['confirm_code' => $confirm_code], function($message) use($data) {

            $message->from('noreply@thinktanker.in');

            $message->to($data['email'], $data['fullname'])->subject('Verify your email address');
        });

		return $this->createDataResponse(true,'Registration Successfully Done. Check your mail and verified it.',['id' => $user->id],201);
	}

	public function showProfile(Request $request)
	{
		$rules = [
			'id' => 'required|numeric'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$id = $request->get('id');

		$user = User::find($id);

		if(!$user){
			return $this->createMessageResponse(false,'user with id '.$id.', not found',404);
		}

		return $this->createOnlyDataResponse(true,$user,200);
	}

	public function update(Request $request)
	{
		$data = $request->all();

		$rules = [
			'user_id' => 'required|numeric',
			'gender' => 'required|in:male,female',
			'fullname' => 'required',
			'position' => 'required',
			'birthdate' => 'required|date',
			'emergency_contact' => 'required',
			'address' => 'required',
			'contact' => 'required'
		];

		$messages = [
			'gender.in' => 'gender should be male or female'
		];

		$validator = Validator::make($request->all(),$rules,$messages);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails user not updated',$validator->errors(),400);
		}

		$rules['contact'] = 'required|unique:users,contact,'.$data['user_id'];

		$validator = Validator::make($request->all(),$rules,$messages);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails user not updated',$validator->errors(),400);
		}

        $data['birthdate'] = date("Y-m-d", strtotime($data['birthdate']));

		$user = User::find($data['user_id']);
		$user->fill($data);
		$user->save();

		return $this->createMessageResponse(true,'User Profile Updated Successfully Done',201);
	}

	public function confirm($confirm_code)
    {
        $user = User::where('confirm_code',$confirm_code)->first();

        if ( ! $user) {
            $message = 'Invalid Confirmation Code';
            return view('api.confirm',compact('message'));
        }

        $message = 'Your account have been verified successfully, you may login now.';

        $user->is_verified = 1;
        $user->confirm_code = null;
        $user->save();

        return view('api.confirm',compact('message'));
    }

    public function logout(Request $request)
    {
    	$rules = [
			'user_id' => 'required|numeric'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		Uuid::where('user_id',$request->user_id)->delete();

		return $this->createMessageResponse(true,'logout Successfully',200);
    }

    public function userList()
    {
    	$users = User::where('is_admin',0)->get()->toArray();

		foreach ($users as $key => $value) {
			$users[$key]['birthdate'] = date("d-m-Y", strtotime($users[$key]['birthdate']));
			$users[$key]['profile_pic'] = url("upload").'/'.$users[$key]['profile_pic'];
		}

    	return $this->createOnlyDataResponse(true,$users,200);
    }

    public function userBirthday()
    {
    	$users = User::where('is_admin',0)
    					->orderBy(DB::raw('CONCAT(SUBSTR(`birthdate`,6) < SUBSTR(CURDATE(),6), SUBSTR(`birthdate`,6))'))
    					->get()
    					->toArray();

    	$usersCount = count($users);

    	array_unshift($users, $users[$usersCount - 1]);
    	unset($users[$usersCount]);

    	foreach ($users as $key => $value) {
			$users[$key]['birthdate'] = date("d-m-Y", strtotime($users[$key]['birthdate']));
			$users[$key]['profile_pic'] = url("upload").'/'.$users[$key]['profile_pic'];
		}

    	return $this->createOnlyDataResponse(true,$users,200);
    }
}
