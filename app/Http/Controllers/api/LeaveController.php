<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Leave;
use App\Models\KnowledgeShare;
use App\Models\Company;
use App\Models\Update;
use App\Models\Uuid;
use PushNotification;
use App\Models\Log;
use DB,Mail;
use App\Http\Controllers\Controller;

class LeaveController extends Controller
{
	public function create(Request $request)
	{
		$rules = [
			'leaverequest' => 'required',
			'user_id' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$leaveRequests = json_decode($request->leaverequest);

		foreach($leaveRequests as $leaveRequest) {

			$leave = new Leave();

			$leave->user_id = $request->user_id;
			$leave->date = date("Y-m-d", strtotime($leaveRequest->senddate));
			$leave->leave_purpose = $leaveRequest->purpose;
			$leave->leave_type = $leaveRequest->type;

			if( ! $leaveRequest->iswholeday) {

				$leave->from_hour = $leaveRequest->fromhour;
				$leave->to_hour = $leaveRequest->tohour;
				$leave->leave_time = 'half day';

			} else {
				$leave->leave_time = 'full day';
			}

			$leave->save();
		}

		$user = User::find($request->user_id);
		
		try{
			if($user) {
				Mail::send('emails.leave-application', ['user' => $user,'leaveRequests' => $leaveRequests], function($message) use($user) {

			        $message->from($user->email);

		            $message->to('rajan@thinktanker.in', 'rajan')->subject('thinkTANKER leave request');
		        });
	        }
		}
		catch(\Exception $e){
			
		}

        $user_id = User::select('id')->where('is_admin',1)->first();
        $admin_id = ($user_id['id']);

        $uuids = Uuid::select('uuid')->where('user_id',$admin_id)->first();
		
		$name = ($user['fullname']);
		if ($uuids != null) {
			
			 
				PushNotification::app('appNameAndroid')
				                ->to($uuids['uuid'])
				                ->send( $name. ' has requested a Leave!');
			
		}else{
			
			$this->createMessageResponse(true,'$name your requeste for Leave Failed!',201);
			
		}

		return $this->createMessageResponse(true,'Leave approved',201);
	}
	public function adminLeave(Request $request){

		$rules = [
			'leaverequest' => 'required',
			'user_id' => 'required',
			'message' => 'required'
			
		];
		
		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$leaveRequests = json_decode($request->leaverequest);

		foreach($leaveRequests as $leaveRequest) {

			$leave = new Leave();

			$leave->user_id = $request->user_id;
			$leave->date = date("Y-m-d", strtotime($leaveRequest->senddate));
			$leave->leave_purpose = $leaveRequest->purpose;
			$leave->leave_type = $leaveRequest->type;

			if( ! $leaveRequest->iswholeday) {

				$leave->from_hour = $leaveRequest->fromhour;
				$leave->to_hour = $leaveRequest->tohour;
				$leave->leave_time = 'half day';

			} else {
				$leave->leave_time = 'full day';
			}

			$leave->status = 'a';

			$leave->save();

			$updates = new Update($request->all());
			$updates->type = 'user';
			$updates->type_id = $request->user_id;
			$updates->save();
		}

		$uuids = Uuid::where('user_id',$request->user_id)->first();
		
		if ($uuids != null) {
			// dd('asd');
			foreach ($uuids as $uuid) {
				PushNotification::app('appNameAndroid')
				                ->to($uuid['uuid'])
				                ->send($request->message);
				            }
		}else{
			
			$this->createMessageResponse(true,'Leave Send Success!',201);
			
		}
 

		return $this->createMessageResponse(true,'Leave Send Successfully!',201);

}
	public function getLeave(Request $request)
	{
		$rules = [
			'user_id' => 'required|numeric'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$user_id = $request->get('user_id');

		$leaves = DB::select("SELECT SUM(pc.sick_leaves) AS sick_leaves,SUM(pc.casual_leaves) AS casual_leaves,SUM(pc.privilege_leaves) AS privilege_leaves FROM (
							  SELECT COALESCE(SUM(CASE leave_time WHEN 'full day' THEN 1  ELSE 0.5 END) , 0) AS sick_leaves,0 AS casual_leaves,0 AS privilege_leaves FROM `leaves` WHERE user_id = ".$user_id." AND YEAR(`date`) = YEAR(NOW()) AND leave_type = 'sick leave' AND STATUS = 'a'
							  UNION SELECT 0 AS sick_leaves,COALESCE(SUM(CASE leave_time WHEN 'full day' THEN 1 ELSE 0.5 END) , 0) AS casual_leaves,0 AS privilege_leaves FROM `leaves` WHERE user_id = ".$user_id." AND YEAR(`date`) = YEAR(NOW()) AND leave_type = 'casual leave' AND STATUS = 'a'
							  UNION SELECT 0 AS sick_leaves,0 AS casual_leaves,COALESCE(SUM(CASE leave_time WHEN 'full day' THEN 1 ELSE 0.5 END) , 0) AS ATS_instantidprivilege_leaves FROM `leaves` WHERE user_id = ".$user_id." AND YEAR(`date`) = YEAR(NOW()) AND leave_type = 'privilege leave' AND STATUS = 'a') AS pc");

		$leaveData = Leave::selectRaw("id,leave_type,leave_time,leave_purpose,status,DATE_FORMAT(date,'%d %b %Y') as date,from_hour,to_hour")
							->where('user_id',$user_id);

		if ($request->has('month')) {
			$leaveData = $leaveData->whereMonth('date','=',$request->get('month'));
		}

		if ($request->has('year')) {
			$leaveData = $leaveData->whereYear('date','=',$request->get('year'));
		}

		$leaveData = $leaveData->orderBy('leaves.date','DESC')
							   ->get();

		return response()->json(['success' => true,'sick_leaves' => $leaves[0]->sick_leaves,'casual_leaves' => $leaves[0]->casual_leaves,'privilege_leaves' => $leaves[0]->privilege_leaves,'data' => $leaveData,],200);
	}

	public function status(Request $request)
	{
		$rules = [
			'id' => 'required',
			'status' => 'required|in:a,d'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$leave = Leave::find($request->id);
		$leave->status = $request->status;
		$leave->save();

		return $this->createMessageResponse(true,'Leave Status Changed Successfully',200);
	}
	public function LeaveDelete(Request $request){

		$rules = [
			
			'user_id' => 'required',
			'leave_date' => 'required|date'
			
		];
		
		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}
		$date = date('Y-m-d',strtotime($request->leave_date));

		$leave = Leave::where('user_id',$request->user_id)->where('date',$date);

		if (!$leave->first()) {
			return $this->createMessageResponse(false,'given data not found',200);
		}

		$leave->delete();

		return $this->createMessageResponse(true,'Leave deleted Successfully',200);

	}
	public function dashboard(Request $request)
	{	
		$data = $request->all();

		$rules = [
			'user_id' => 'required|numeric',
			'company_id' => 'required|numeric'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}
		$today = date("Y-m-d");
		
		// dd($date);
		$user_log = Log::selectRaw("TIME_FORMAT(MIN(check_in), '%h:%i%p') as check_in,TIME_FORMAT(MAX(check_out), '%h:%i%p') as check_out")->where('user_id',$request->user_id)->where('time',$today)->orderBy('id','desc')->first();


		// dd($user_log);
		if($user_log != null)
		{
			if($user_log->check_in != null){

				$check_in = date('h:i A', strtotime($user_log->check_in));
			}else{
				$check_in = null;
			}

			if($user_log->check_out != null){
				$check_out = date('h:i A', strtotime($user_log->check_out));
			}else{
				$check_out = null;
			}
		$user_checkin = Log::select('check_in')->where('user_id',$request->user_id)->where('time',$today)->orderBy('id','desc')->value('check_in');
		$user_checkout = Log::select('check_out')->where('user_id',$request->user_id)->where('time',$today)->orderBy('id','desc')->value('check_out');

			if($user_checkin == null && $user_checkout == null){
				$check_in_status = 'true';
			}
			elseif($user_checkout != null ){
				
				$check_in_status = 'true';
			}
			else{
							
				$check_in_status = 'false';
			}
		 }
			else{
				$check_in = null;
				$check_out = null;
				
		}

		$current_date = date("d-M");
		$current_year = date('Y');


		// $sick_leave = Leave::where('user_id',$request->user_id)->where(DB::raw('YEAR(date)'),$current_year)->where('leave_type','Sick Leave')->where('status','a')->count();
		$sick_leave = DB::select(" SELECT (companies.sick_leave - COALESCE(SUM(CASE leave_time WHEN 'full day' THEN 1  ELSE 0.5 END) , 0)) AS sick_leaves FROM `leaves` INNER JOIN users ON `leaves`.user_id = users.id INNER JOIN companies ON companies.id = users.company_id
  			WHERE user_id = ".$request->user_id." AND YEAR(`date`) = YEAR(NOW()) AND leave_type = 'sick leave' AND STATUS = 'a'");


		$casual_leave = DB::select(" SELECT (companies.casual_leave - COALESCE(SUM(CASE leave_time WHEN 'full day' THEN 1  ELSE 0.5 END) , 0)) AS casual_leaves FROM `leaves` INNER JOIN users ON `leaves`.user_id = users.id INNER JOIN companies ON companies.id = users.company_id
  			WHERE user_id = ".$request->user_id." AND YEAR(`date`) = YEAR(NOW()) AND leave_type = 'casual leave' AND STATUS = 'a'");
		$privilege_leave = DB::select(" SELECT (companies.privilege_leave - COALESCE(SUM(CASE leave_time WHEN 'full day' THEN 1  ELSE 0.5 END) , 0)) AS privilege_leaves FROM `leaves` INNER JOIN users ON `leaves`.user_id = users.id INNER JOIN companies ON companies.id = users.company_id
  			WHERE user_id = ".$request->user_id." AND YEAR(`date`) = YEAR(NOW()) AND leave_type = 'Privilege leave' AND STATUS = 'a'");

		// dd($casual_leave);

		$company = Company::select('sick_leave','casual_leave','privilege_leave')->where('id',$request->company_id)->first();

		
		 // dd($remaining_leave);

		$birthdate = DB::select("SELECT fullname,contact,DATE_FORMAT(birthdate,'%d-%b') AS 'date',CONCAT('http://timeloggerapp.thinktanker.in/upload/',profile_pic) AS image FROM users WHERE is_verified = '1' AND company_id = ".$request->company_id." AND DATEDIFF(DATE_FORMAT(NOW(),'%y-%m-%d'),DATE_FORMAT(birthdate,CONCAT(YEAR(NOW()),'-%m-%d'))) BETWEEN -2 AND 0 ORDER BY DATE_FORMAT(birthdate,'%m-%d') ASC");

		$holiday = DB::select("SELECT `day`,holiday,DATE_FORMAT(`date`,'%d-%b') AS 'date',image AS image FROM holidays WHERE company_id = ".$request->company_id." AND DATEDIFF(DATE_FORMAT(CURDATE(),'%y-%m-%d'),DATE_FORMAT(`Date`,CONCAT(YEAR(CURDATE()),'-%m-%d'))) BETWEEN -2 AND 0 ORDER BY DATE_FORMAT(`date`,'%m-%d') ASC");

		$knowledge = DB::select("SELECT users.fullname,users.profile_pic, title,description,url,DATE_FORMAT(knowledge_share.created_at,'%h:%i %p') AS 'time'
			FROM knowledge_share LEFT JOIN users ON knowledge_share.user_id = users.id
	 		WHERE knowledge_share.company_id = ".$request->company_id." AND (DATE_FORMAT(CURDATE(),'%y-%m-%d')) ORDER BY knowledge_share.id DESC");



		// $user = User::select('*',CONCAT('http://timeloggerapp.thinktanker.in/upload/','profile_pic')->where('company_id', $request->company_id)->where('is_admin',0)->where('is_verified',1)->get()->toArray();
		// dd($user);
		$path = asset('upload').'/';

		$user = DB::select("SELECT id,company_id,CONCAT('$path',profile_pic) AS profile_pic,gender,fullname,position,DATE_FORMAT(birthdate,'%d-%m-%Y') AS 'birthdate',email,personal_email,password,contact,emergency_contact,address,is_verified,confirm_code,temp_photo,is_admin,device_id,remember_token,deleted_at,created_at,updated_at from users WHERE company_id = ".$request->company_id." AND id !=".$request->user_id." AND is_admin = '0' AND is_verified = '1'");

		return response()->json(['success' => true,'check_in' => $check_in,'check_out'=>$check_out,'check_in_status' => $check_in_status,"remaining_sick_Leave" => $sick_leave,'remaining_casual_leave' => $casual_leave,'remaining_privilege_leave' => $privilege_leave,"current_date" => $current_date,"Next_birthdate" => $birthdate,"Next_holiday" => $holiday,"Knowledge_Share" => $knowledge,"Employees" => $user,],200);

	}

}
