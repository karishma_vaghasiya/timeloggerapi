<?php
namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Update;
use App\Models\Uuid;
use PushNotification;
use App\Http\Controllers\Controller;

class UpdatesController extends Controller
{
	public function getUpdates(Request $request)
	{
		$rules = [
			'user_id' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$user = User::find($request->user_id);

		if( ! $user){
			return $this->createMessageResponse(false,'User with is id doesn\'t exists',400);
		}

		$updates = Update::selectRaw("DATE_FORMAT(created_at, '%d-%b-%Y') as date,DATE_FORMAT(created_at, '%h:%i%p') as time,message")
							->whereRaw("type_id = ".$request->user_id." OR `type` = 'group'")
							->orderBy('id','DESC')
							->take(10)
							->get()
							->toArray();

		return $this->createOnlyDataResponse(true,$updates,200);
	}

	public function create(Request $request)
	{
		$rules = [
			'type' => 'required|in:group,user',
			'message' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		if( $request->type == 'user' ) {

			$rules['type_id'] = 'required|numeric';

			$validator = Validator::make($request->all(),$rules);

			if ($validator->fails()) {
				return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
			}

			$user = User::find($request->type_id);

			if( ! $user) {
				return $this->createMessageResponse(false,'Invalid Type id',400);
			}

			$uuids = Uuid::where('user_id',$request->type_id)->get()->toArray();

		} else {
			$uuids = Uuid::get()->toArray();
		}

		$updates = new Update($request->all());
		$updates->save();

		foreach($uuids as $uuid) {
			PushNotification::app('appNameAndroid')
		                ->to($uuid['uuid'])
		                ->send($request->message);
		}
	}
}
