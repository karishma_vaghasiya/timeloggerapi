<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ATSInstantId;
use DB,Mail;
use App\Http\Controllers\Controller;

class ATSInstantIdController extends Controller
{
	public function store(Request $request)
	{
		$rules = [
		'instant_id' => 'required ',

		];

	$validator = Validator::make($request->all(),$rules);

	if ($validator->fails()) {
			return $this->createDataResponse(false,'Instant Id Not Found!',$validator->errors(),400);
		}

	$instant_id = ATSInstantId::firstOrNew(array('instant_id'=>$request->instant_id));

	$instant_id->save();

	return $this->createMessageResponse(true,'Instant Id Saved Successfully.',201);
	}

	public function sendMessage(Request $request)
	{
		$rules = [
			'message' => 'required',
		];

	$validator = Validator::make($request->all(),$rules);

	if ($validator->fails()) {
			return $this->createDataResponse(false,'Message Not Found!',$validator->errors(),400);
		}
		$data = array('message'=>$request->message);

		$target = ATSInstantId::select('instant_id')->lists('instant_id')->toArray();
		
		//FCM api URL
		$url = 'https://fcm.googleapis.com/fcm/send';
		//api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
		$server_key = 'AIzaSyCJchgODg_RPKz1P2txze5vMpg-ZHl4r9M';
					
		$fields = array();
		$fields['data'] = $data;
		if(is_array($target)){
			$fields['registration_ids'] = $target;
		}else{
			$fields['to'] = $target;
		}
		//header with content_type api key
		$headers = array(
			'Content-Type:application/json',
		  'Authorization:key='.$server_key
		);
					
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		if ($result === FALSE) {
			die('FCM Send Error: ' . curl_error($ch));
		}
		curl_close($ch);
		// dd($result);
		return $result;

		return $this->createMessageResponse(true,'Message Send Successfully.',201);

	}
	public function confirmEmail(Request $request)
    {
        $email = $request->get('email');
        $name = $request->get('name');
        $send_mail = 'amtsamc@gmail.com';

       	$imagePath = public_path('upload').'/amts';
        $cardImageName = sha1(microtime()).'.jpg';
        $cardImageWithPath = $imagePath.'/'.$cardImageName;
        $cardImageData = base64_decode($request->get('image'));
        file_put_contents($cardImageWithPath,$cardImageData);
        
       if(isset($cardImageName))
       {
       	$cardImageName = public_path().'/upload/amts/'.$cardImageName;
       }

       $data = [
	        'title' => $request->get('title'),
	        'description' => $request->get('description'),
	        'email' => $email,
	        'name' => $name,
        ];
        // dd($cardImageName);
        if(! empty($request->get('image'))){
        	
	        Mail::send('amts',$data,function ($message) use ($send_mail,$name,$cardImageName)  {
	        $message->subject('Confirm Email')
	              ->from('amtsthinktanker@gmail.com')
	              ->to($send_mail)
	              ->attach($cardImageName);
	        });
    	}else{
    		Mail::send('amts',$data,function ($message) use ($send_mail,$name)  {
	        $message->subject('Confirm Email')
	              ->from('amtsthinktanker@gmail.com')
	              ->to($send_mail);
	        });
    	}

		
		if (file_exists($cardImageName)) {
		    @unlink($cardImageName);
		}

        return response()->json(['success'=>true, 'Mail' => 'Mail Send Successfully!'],200);
        }
}
