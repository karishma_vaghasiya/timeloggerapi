<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ImageUploadController extends Controller
{
	public function index(Request $request)
	{
		$rules = [
			'id' => 'required|numeric',
			'image' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails image not changed',$validator->errors(),400);
		}

		$user = User::find($request->id);

		if( ! $user) {
			return $this->createMessageResponse(false,'user with this id - '.$request->id.', not exists',404);
		}

		$imagePath = public_path('upload');

		if( isset($user['profile_pic']) ) {

			@unlink($imagePath.'/'.$user['profile_pic']);
		}

		$imageName = sha1(microtime()).'.jpg';

		$imageWithPath = $imagePath.'/'.$imageName;

		$data = base64_decode($request->image);

		file_put_contents($imageWithPath,$data);

		$user->update( ['profile_pic' => $imageName] );

		$imageUrl = url("upload").'/'.$imageName;

		return $this->createDataResponse(true,'Image Stored Successfully',['image' => $imageUrl],200);
	}
}
