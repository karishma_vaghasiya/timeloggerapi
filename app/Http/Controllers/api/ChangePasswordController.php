<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class ChangePasswordController extends Controller
{
	public function index(Request $request)
	{
		$rules = [
			'id' => 'required|numeric',
			'old_password' => 'required',
			'password' => 'required|confirmed',
			'password_confirmation' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails password not changed',$validator->errors(),400);
		}

		$user = User::find($request->id);

		if( ! $user) {
			return $this->createMessageResponse(false,'user with this id - '.$request->id.', not exists',404);
		}

		if( ! (Hash::check($request->old_password, $user['password']) )) {
			return $this->createMessageResponse(false,'Your Old password is not correct, try again',400);
		}

		$password = Hash::make($request->password);

		User::where('id',$request->id)->update( ['password' => $password] );

		return $this->createMessageResponse(true,'User\'s password changed successfully',200);
	}
}
