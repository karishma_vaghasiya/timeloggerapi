<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Holiday;
use App\Models\Leave;
use App\Models\Log;
use DB;
use App\Http\Controllers\Controller;

class AdminDashboardController extends Controller
{
	public function getData()
	{
		$adminData = [];

		$leaveData = Leave::selectRaw("leaves.id,leave_type,leave_time,leave_purpose,status,DATE_FORMAT(`date`, '%d %b') as `date`,from_hour,to_hour,user_id,fullname")
							->where('status','p')
							->leftJoin('users','users.id','=','leaves.user_id')
							->orderBy('id','DESC')
							->get();

		$holiday = Holiday::selectRaw("CONCAT(DATE_FORMAT(`date`, '%d %b'),' (',holiday,')') as holiday")
							->whereRaw('`date` >= NOW()')
							->orderBy('date')
							->first();

		$users = User::where('is_admin',0)->where('is_verified',1)->get()->toArray();

		foreach ($users as $key => $value) {

			$users[$key]['birthdate'] = date("d-m-Y", strtotime($value['birthdate']));

			$users[$key]['profile_pic'] = url("upload").'/'.$value['profile_pic'];

			$users[$key]['logs'] = Log::selectRaw("DAY(`time`) as date,TIME_FORMAT(check_in, '%h:%i%p') as check_in,TIME_FORMAT(check_out, '%h:%i%p') as check_out")
											 ->whereRaw('MONTH(`time`) = '.date("m"))
											 ->whereRaw('DAY(`time`) = '.date("d"))
											 ->where('user_id',$value['id'])
											 ->orderBy('id','desc')
											 ->first();
		}

		$adminData['leaves'] = $leaveData;
		$adminData['holiday'] = $holiday['holiday'];
		$adminData['users'] = $users;

		return $this->createOnlyDataResponse(true,$adminData,200);
	}

	public function postUserData(Request $request)
	{
		$data = $request->all();

		$rules = [
			'company_id' => 'required|numeric'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}
		// $user = User::select('*',CONCAT('http://timeloggerapp.thinktanker.in/upload/','profile_pic')->where('company_id', $request->company_id)->where('is_admin',0)->where('is_verified',1)->get()->toArray();
		// dd($user);
		$path = asset('upload').'/';

		$user = DB::select("SELECT id,company_id,CONCAT('$path',profile_pic) AS profile_pic,gender,fullname,position,DATE_FORMAT(birthdate,'%d-%m-%Y') AS 'birthdate',email,personal_email,password,contact,emergency_contact,address,is_verified,confirm_code,temp_photo,is_admin,device_id,remember_token,deleted_at,created_at,updated_at from users WHERE is_admin = '0' AND is_verified = '1'");

		$birthday = DB::select("SELECT fullname,contact,DATE_FORMAT(birthdate,'%d-%m-%Y') AS 'date',CONCAT('http://timeloggerapp.thinktanker.in/upload/',profile_pic) AS image FROM users WHERE company_id = ".$request->company_id." AND DATEDIFF(DATE_FORMAT(CURDATE(),'%y-%m-%d'),DATE_FORMAT(birthdate,CONCAT(YEAR(CURDATE()),'-%m-%d'))) BETWEEN -2 AND 0");

		$holiday = DB::select("SELECT `day`,holiday,DATE_FORMAT(`date`,'%d-%m-%Y') AS 'date',image AS image FROM holidays WHERE company_id = ".$request->company_id." AND DATEDIFF(DATE_FORMAT(CURDATE(),'%y-%m-%d'),DATE_FORMAT(`Date`,CONCAT(YEAR(CURDATE()),'-%m-%d'))) BETWEEN -2 AND 0");
		
		$leave_pending_user = User::with(array('countPending' => function($query){ 
											$query->where('status','p')->orderBy('created_at','desc')->get(); 
										}))
									->with(array('countApprove' => function($query){ 
											$query->where('status','a')->get(); 
										}))
									->with(array('countDisApprove' => function($query){ 
											$query->where('status','d')->get(); 
										}))
									->get()->toArray();

		$final_leave_count = [];

		foreach ($leave_pending_user as $key => $value) {

			if (!empty($value['count_pending'])) {
				$pending = count($value['count_pending']);
				$approve = count($value['count_approve']);
				$dis_approve = count($value['count_dis_approve']);

				$single_final_count['user_id'] = $value['id'];
				$single_final_count['fullname'] = $value['fullname'];
				$single_final_count['pending_list'] = $value['count_pending'];
				$single_final_count['count_pending'] = $pending;
				$single_final_count['count_approve'] = $approve;
				$single_final_count['count_dis_approve'] = $dis_approve;

				$single_final_count['total'] = $pending + $approve + $dis_approve;

				$final_leave_count[] = $single_final_count;
			}

		}

		
		$userData['users'] = $user;
		$userData['next_Birthday'] = $birthday;
		$userData['next_Holiday'] = $holiday;
		$userData['leave_user'] = $final_leave_count;
		

		return $this->createOnlyDataResponse(true,$userData,200);

	}
	public function getLogData(Request $request)
	{
		$data = $request->all();

		$rules = [
			'date' => 'required|date',
			'company_id' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$data['date'] = date('Y-m-d',strtotime($data['date']));

		$logs = User::selectRaw("DAY(`time`) as date,TIME_FORMAT(MIN(check_in), '%h:%i%p') as check_in,TIME_FORMAT(MAX(check_out), '%h:%i%p') as check_out,TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))),'%H:%i') as time_difference,users.fullname,users.profile_pic")
								 ->leftJoin('logs','logs.user_id','=','users.id')
								 ->where('time',$data['date'])
								 ->where('users.is_admin',0)
								 // ->orderBy('users.id','desc')
								 // ->orderBy('logs.id','desc')
								 ->orderBy('users.fullname','asc')
								 ->groupBy(DB::raw('DAY(`time`),logs.user_id'))
								 // ->toSql();
								 ->get()->toArray();
								  // dd($logs);
		
		foreach ($logs as $key => $log) {
			$logs[$key]['profile_pic'] = asset('upload').'/'.$logs[$key]['profile_pic'];
		}

		return $this->createOnlyDataResponse(true,$logs,200);
	}
}