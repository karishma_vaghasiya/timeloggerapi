<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Quiz;
use App\Models\QuizOption;
use App\Models\UserQuizAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use DB;

class QuizController extends Controller
{
    public function getQuiz(Request $request)
	{
        $data = $request->all();

		$rules = [
			'quiz_id' => 'required',
            'user_id' => 'required'
		];

		$validator = Validator::make($data,$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

        $quiz = Quiz::find($data['quiz_id']);

        if( ! $quiz ) {
            return $this->createMessageResponse(false,'quiz not found with this id',400);
        }

        $user = User::find($data['user_id']);

        if ( ! $user) {
           return $this->createMessageResponse(false,'user not found with this id',400); 
        }

        UserQuizAnswer::where('user_id',$data['user_id'])->where('quiz_id',$data['quiz_id'])->update(['unanswered' => 1]);

        $quizOptions = QuizOption::select('option','id')->where('quiz_id',$data['quiz_id'])->get()->toArray();

        $quiz['options'] = $quizOptions;

        return $this->createOnlyDataResponse(true,$quiz,200);
	}

    public function storeQuizAnswer(Request $request)
    {
        $data = $request->all();

        $rules = [
			'user_id' => 'required',
			'quiz_id' => 'required'
		];

		$validator = Validator::make($data,$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

        $userQuizAns = UserQuizAnswer::where('quiz_id',$data['quiz_id'])->where('user_id',$data['user_id']);
        $getUserQuizAns = $userQuizAns->first();

        if( ! $getUserQuizAns ) {
            return $this->createMessageResponse(false,'there is no quiz with this quiz id and user id',400);
        }

        if( $getUserQuizAns->quiz_option_id != NULL ) {
            return $this->createMessageResponse(false,'this user with this answer is already attempted',400);
        }

        $userQuizAns->update(['unanswered' => 1]);

        $rules['quiz_option_id'] = 'required';

		$validator = Validator::make($data,$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

        $userQuizAns->update(['quiz_option_id' => $data['quiz_option_id']]);

        return $this->createMessageResponse(true,'quiz answer successfully updated',200);
    }

    public function quizList(Request $request)
    {
        $data = $request->all();

        $rules = [
            'user_id' => 'required'
        ];

        $validator = Validator::make($data,$rules);

        if ($validator->fails()) {
            return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
        }

        $quizList = UserQuizAnswer::select('quizzes.id','quizzes.question','user_quiz_answers.unanswered','quiz_options.option','quizzes.created_at')
                                    ->where('user_id',$data['user_id'])
                                    ->leftJoin('quizzes','user_quiz_answers.quiz_id','=','quizzes.id')
                                    ->leftJoin('quiz_options','user_quiz_answers.quiz_option_id','=','quiz_options.id')
                                    ->orderBy('quizzes.id','DESC')
                                    ->where('quizzes.is_send',1)
                                    ->get()
                                    ->toArray();
    
        foreach ($quizList as $key => $value) {
            $quizList[$key]['created_at'] = date('d-M h:i A',strtotime($value['created_at']));
        }

        return $this->createOnlyDataResponse(true, $quizList, 200);
    }

    public function getLeaderBoard(Request $request)
    {
        $data = $request->all();

        $rules = [
            'time' => 'required | in:m,lm,at'
        ];

        $validator = Validator::make($data,$rules);

        if ($validator->fails()) {
            return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
        }

        if ($data['time'] == 'lm') {
            $leaderBoardList = DB::select("SELECT xs.id,xs.position,xs.fullname,xs.profile_pic,TRUNCATE(IFNULL(( (xs.total_right_ans  * 100) / xs.total_attempt  ), 0 ),2)AS marks FROM 
                                            (SELECT  u.`id`,u.`position`,u.`fullname`,u.`profile_pic`,
                                            (SELECT COUNT(*) FROM user_quiz_answers WHERE unanswered = '1' AND user_id = u.`id` AND user_quiz_answers.created_at <= (NOW() - INTERVAL 1 MONTH) ) AS total_attempt,
                                            (SELECT COUNT(*) FROM user_quiz_answers
                                            LEFT JOIN quiz_options ON quiz_options.`id` = user_quiz_answers.`quiz_option_id`
                                            WHERE unanswered = '1' AND user_id = u.`id` AND is_true = '1' AND user_quiz_answers.created_at <= (NOW() - INTERVAL 1 MONTH) ) AS total_right_ans
                                            FROM users u  WHERE u.`is_admin` = '0') xs ORDER BY marks DESC");

        }elseif ($data['time'] == 'm') {
            $leaderBoardList = DB::select("SELECT xs.id,xs.position,xs.fullname,xs.profile_pic,TRUNCATE(IFNULL(( (xs.total_right_ans  * 100) / xs.total_attempt  ), 0 ),2)AS marks FROM 
                                            (SELECT  u.`id`,u.`position`,u.`fullname`,u.`profile_pic`,
                                            (SELECT COUNT(*) FROM user_quiz_answers WHERE unanswered = '1' AND user_id = u.`id` AND MONTH(user_quiz_answers.created_at) = MONTH(NOW()) ) AS total_attempt,
                                            (SELECT COUNT(*) FROM user_quiz_answers
                                            LEFT JOIN quiz_options ON quiz_options.`id` = user_quiz_answers.`quiz_option_id`
                                            WHERE unanswered = '1' AND user_id = u.`id` AND is_true = '1'  AND u.`is_admin` = '0' AND MONTH(user_quiz_answers.created_at) = MONTH(NOW()) ) AS total_right_ans
                                            FROM users u WHERE u.`is_admin` = '0') xs ORDER BY marks DESC");
        } else {
            $leaderBoardList = DB::select("SELECT xs.id,xs.position,xs.fullname,xs.profile_pic,TRUNCATE(IFNULL(( (xs.total_right_ans  * 100) / xs.total_attempt  ), 0 ),2)AS marks FROM 
                                            (SELECT  u.`id`,u.`position`,u.`fullname`,u.`profile_pic`,
                                            (SELECT COUNT(*) FROM user_quiz_answers WHERE unanswered = '1' AND user_id = u.`id`) AS total_attempt,
                                            (SELECT COUNT(*) FROM user_quiz_answers
                                            LEFT JOIN quiz_options ON quiz_options.`id` = user_quiz_answers.`quiz_option_id`
                                            WHERE unanswered = '1' AND user_id = u.`id` AND is_true = '1' AND u.`is_admin` = '0') AS total_right_ans
                                            FROM users u WHERE u.`is_admin` = '0') xs ORDER BY marks DESC");
        }

        $leaderBoardList = $this->getRank($leaderBoardList);

        return $this->createOnlyDataResponse(true, $leaderBoardList, 200);
    }

    public function getActivityBoard(Request $request)
    {
        $data = $request->all();

        $rules = [
            'time' => 'required | in:m,lm,at'
        ];

        $validator = Validator::make($data,$rules);

        if ($validator->fails()) {
            return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
        }

        if ($data['time'] == 'lm') {
            $leaderBoardList = DB::select("SELECT xs.id,xs.position,xs.fullname,xs.profile_pic,TRUNCATE(IFNULL(( (xs.total_attempt  * 100) / xs.total_ask_que  ), 0 ),2)AS marks FROM 
                                            (SELECT  u.`id`,u.`position`,u.`fullname`,u.`profile_pic`,
                                            (SELECT COUNT(*) FROM user_quiz_answers WHERE unanswered = '1' AND user_id = u.`id` AND user_quiz_answers.created_at <= (NOW() - INTERVAL 1 MONTH) ) AS total_attempt,
                                            (SELECT COUNT(*) FROM user_quiz_answers WHERE user_id = u.`id` AND user_quiz_answers.created_at <= (NOW() - INTERVAL 1 MONTH) ) AS total_ask_que
                                            FROM users u WHERE u.`is_admin` = '0') xs ORDER BY marks DESC");

        }elseif ($data['time'] == 'm') {
            $leaderBoardList = DB::select("SELECT xs.id,xs.position,xs.fullname,xs.profile_pic,TRUNCATE(IFNULL(( (xs.total_attempt  * 100) / xs.total_ask_que  ), 0 ),2)AS marks FROM 
                                            (SELECT  u.`id`,u.`position`,u.`fullname`,u.`profile_pic`,
                                            (SELECT COUNT(*) FROM user_quiz_answers WHERE unanswered = '1' AND user_id = u.`id` AND MONTH(user_quiz_answers.created_at) = MONTH(NOW()) ) AS total_attempt,
                                            (SELECT COUNT(*) FROM user_quiz_answers WHERE user_id = u.`id`AND MONTH(user_quiz_answers.created_at) = MONTH(NOW()) ) AS total_ask_que
                                            FROM users u WHERE u.`is_admin` = '0') xs ORDER BY marks DESC");
        } else {
            $leaderBoardList = DB::select("SELECT xs.id,xs.position,xs.fullname,xs.profile_pic,TRUNCATE(IFNULL(( (xs.total_attempt  * 100) / xs.total_ask_que  ), 0 ),2)AS marks FROM 
                                            (SELECT  u.`id`,u.`position`,u.`fullname`,u.`profile_pic`,
                                            (SELECT COUNT(*) FROM user_quiz_answers WHERE unanswered = '1' AND user_id = u.`id`) AS total_attempt,
                                            (SELECT COUNT(*) FROM user_quiz_answers WHERE user_id = u.`id`) AS total_ask_que
                                            FROM users u WHERE u.`is_admin` = '0') xs ORDER BY marks DESC");
        }

        $leaderBoardList = $this->getRank($leaderBoardList);

        return $this->createOnlyDataResponse(true, $leaderBoardList, 200);
    }

    public function getRank($leaderBoardList)
    {
        $rank = 1;
        foreach ($leaderBoardList as $key => $value) {

            $leaderBoardList[$key] = (array)$value;
            $leaderBoardList[$key]['profile_pic'] = url("upload").'/'.$value->profile_pic;

            if ($key > 0) {
                if ($leaderBoardList[$key - 1]['marks'] == $leaderBoardList[$key]['marks']) {
                    $leaderBoardList[$key]['rank'] = $leaderBoardList[$key - 1]['rank'];
                } else {
                    $leaderBoardList[$key]['rank'] = $rank;
                }
            } else {
                $leaderBoardList[$key]['rank'] = $rank;
            }

            $rank++;
        }

        return $leaderBoardList;
    }
}
