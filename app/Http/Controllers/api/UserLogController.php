<?php
namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Log;
use App\Models\Reminder;
use App\Models\NewReminder;
use App\Models\Holiday;
use App\Models\Leave;
use App\Http\Controllers\Controller;
use DB;

class UserLogController extends Controller
{
	public function create(Request $request)
	{
		$data = $request->all();

		$rules = [
			'lat' => 'required',
			'long' => 'required',
			'user_id' => 'required',
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$checkLog = Log::where('user_id',$request->user_id)->where('time',date('Y-m-d'))->whereNull('check_out')->get();

		if (count($checkLog)) {

			$check_out = date('H:i:s');

			$interval = \DB::select("select TIMEDIFF('".$check_out."','".$checkLog[0]['check_in']."') AS `interval`");

			$logUpdate = Log::find($checkLog[0]['id']);
			$logUpdate->check_out = $check_out;
			$logUpdate->time_diff = $interval[0]->interval;
			$logUpdate->save();

			$data['check_out'] = date("h:i a", strtotime($check_out));

			return $this->createDataResponse(true,'Check Out Successfully done',$data,201);

		} else {

			$check_in = date('H:i:s');

			$logNew = new Log($data);
			$logNew->time = date('Y-m-d');
			$logNew->check_in = $check_in;
			$logNew->save();

			$data['check_in'] = $data['check_in'] = date("h:i a", strtotime($check_in));

			return $this->createDataResponse(true,'Check In Successfully done',$data,201);
		}
	}

	public function getLog(Request $request)
	{
		$rules = [
			'user_id' => 'required|numeric',
			'month' => 'required|numeric',
			'year' => 'required|numeric'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$logs = Log::selectRaw("DAY(`time`) as date,TIME_FORMAT(MIN(check_in), '%h:%i%p') as check_in,TIME_FORMAT(MAX(check_out), '%h:%i%p') as check_out,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_difference")
					 ->whereRaw("MONTH(`time`) = ".$request->month." AND YEAR(`time`) = ".$request->year." AND user_id = ".$request->user_id)
					 ->orderBy('time','desc')
					 ->groupBy(DB::raw('DAY(`time`)'))
					 ->get()
					 ->toArray();

		// foreach ($logs as $key => $log) {

		// 	$nextLog = $logs[$i + 1];

		// 	if($nextLog['date'] == $log['date']) {

		// 		$logs[$key]['check_out'] = $nextLog['check_out'];


		// 		unset($logs[$i + 1]);

		// 		$logs = array_values($logs);
		// 	}
		// }

		// $daysCount = cal_days_in_month(CAL_GREGORIAN, 2, $request->year);

		return $this->createOnlyDataResponse(true,$logs,200);
	}

	public function userLog(Request $request){

		$rules = [
			'user_id' => 'required|numeric',
			'month' => 'required|numeric',
			'year' => 'required|numeric'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$month = $request->get('month');
		$year = $request->get('year');
		$user_id = $request->get('user_id');

		
		$no_of_date = cal_days_in_month(CAL_GREGORIAN, $request->month, $request->year);

		$final = [];

		for ($i=1; $i <= $no_of_date ; $i++) { 
			$date = $request->year."-".$request->month."-".$i;

			$single_final = [];

			$single_final['checkin']  = null;
			$single_final['checkout'] = null;
			$single_final['working_hour']  = null;
			$single_final['holiday']  = null;
			$single_final['date'] 	  = date('d-M-Y', strtotime($date));

			$check_log = Log::whereDate('time','=',$date)
								->where('user_id',$user_id)
							 	->get();
						// dd($check_log);

			$check_hollyday = Holiday::where('date',$date)->first();
			// dd($check_hollyday);
			$check_leave = Leave::whereDate('date','=',$date)
									->where('user_id',$user_id)
								 	->first();

			if (count($check_log)) {
		 		$check_log = $check_log->toArray();

		 		$first_entry = head($check_log);
		 		$last_entry = last($check_log);
		 		// dd($last_entry);
		 		$working_hour = array_column($check_log,'time_diff');
		 		$total = '';

		 		if (!empty($working_hour)) {
		 			$working_hour_array = [];
		 			$hour_temp = 0;
		 			$min_temp = 0;
		 			$sec_temp = 0;
		 			foreach ($working_hour as $key => $single_working_hour) {
		 				if (!empty($single_working_hour)) {
		 				
			 				$divide = explode(':',$single_working_hour);
			 				$hour_temp = $divide[0] + $hour_temp;
			 				$min_temp = $divide[1] + $min_temp;
			 				$sec_temp = $divide[2] + $sec_temp;
		 				}
		 			}

		 			$sec_final = number_format($sec_temp%60);
		 			$min_temp = (explode('.',$sec_temp/60)[0]) + $min_temp;
		 			
		 			$min_final = number_format($min_temp%60);
		 			$hour_temp = (explode('.',$min_temp/60)[0]) + $hour_temp;

		 			$working_hour_array = array_sum($working_hour_array);

		 			$total = explode('.', $working_hour_array);

		 		}

		 		$single_final['checkin']  = date('h:i a', strtotime( $first_entry['check_in']));
		 		if($last_entry['check_out'] != null){
				$single_final['checkout'] = date('h:i a', strtotime( $last_entry['check_out']));
			}
				$single_final['working_hour'] = sprintf('%02d', $hour_temp). ":" . sprintf('%02d', $min_final);
		 		
		 	}elseif (count($check_hollyday)) {
				$single_final['holiday'] = $check_hollyday['holiday'];
			}elseif(count($check_leave)){
				$check_leave->toArray();

				$single_final['holiday'] = $check_leave['leave_type'];
			}else{
				
				if((date('l', strtotime( $date)) == 'Sunday') || (date('l', strtotime( $date)) == 'Saturday')){
					$single_final['holiday'] = date('l', strtotime( $date));
				}else{
					$single_final['holiday'] = 'absent';
				}
				
		
			}

			$final[] = $single_final;
		}

		// echo "<pre>";
		// print_r($final);
		// exit();

		// $userlogs[$key]['holidays'] = Holiday::where('date',$date)->pluck('holiday');

		return $this->createOnlyDataResponse(true,$final,200);
	}

	public function setReminder(Request $request)
	{
		$rules = [
			'user_id' => 'required|numeric',
			'is_wifi' => 'required|in:0,1',
			'is_time' => 'required|in:0,1',
			'is_mute' => 'required|in:0,1',
			'check_in' => 'required',
			'check_out' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$reminder = Reminder::firstOrCreate(['user_id' => $request->user_id]);
		$reminder->fill($request->all());
		$reminder->save();

		return $this->createMessageResponse(true,'Reminder Added or Updated Successfully',201);
	}

	public function newReminder(Request $request)
	{

		$rules = [
			'user_id' => 'required|numeric',
			'is_wifi' => 'required|in:0,1',
			'check_in_start' => 'required',
			'check_out_start' => 'required',
			'check_in_end' => 'required',
			'check_out_end' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$new_reminder = NewReminder::firstOrCreate(['user_id' => $request->user_id]);
		$new_reminder->fill($request->all());
		$new_reminder->save();

		return $this->createMessageResponse(true,'New Reminder Added or Updated Successfully',201);


	}

	public function setting()
	{
		$rules = [
			'wifi_info' => 'required',
			'lat' => 'required',
			'long' => 'required'
		];

		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return $this->createDataResponse(false,'validation fails',$validator->errors(),400);
		}

		$setting = Setting::firstOrCreate(['id' => 1]);
		$setting->fill($request->all());
		$setting->save();
	}
}
