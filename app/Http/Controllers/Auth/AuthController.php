<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    // public function accountIsActive($activation_code)
    // {
    //     if(isset($activation_code))
    //     {
    //         $user = User::where('activation_code', $activation_code)->first();
    //         $user->isactive = 1;
    //         $user->activation_code = '';
    //         if($user->save()) {
    //              return redirect('/login')->with('msg','your account has been activated.');
    //         }
    //     return redirect('/create')->with('msg','your account is not activated.');
    //     }
    // }

    // public function authenticate()
    // {
    //     if (Auth::attempt(['email' => $email, 'password' => $password]))
    //     {
    //         return redirect()->intended();
    //     }
    // }

    protected $redirectPath = '/dashboard';
    protected $loginPath = '/login';
    protected $redirectAfterLogout = '/login';
}
