<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Quiz;
use App\Models\Uuid;
use App\Models\QuizOption;
use App\Models\UserQuizAnswer;
use Validator,PushNotification;

class QuizController extends Controller
{
    public function index(Request $request)
    {
       if($request->ajax()){

            $where_str = "1=?";
            $where_param = array('1');

            if($request->has('search.value')){

                $search = $request->get('search.value');

                $where_str .= " and (question like \"%{$search}%\""
                            ." or time like \"%{$search}%\""
                            .")";
            }

            $columns = array('id','question','time','is_send');

            $sort_columns = array('id','question','time');

            $quiz_count = Quiz::select('id')
                                ->whereRaw($where_str,$where_param)
                                ->count();

            $quiz = Quiz::select($columns)
                                ->whereRaw($where_str,$where_param)
                                ->orderBy('id','ASC');

            if($request->has('start') && $request->get('length') !='-1'){
                        $quiz = $quiz->take($request->get('length'))
                                    ->skip($request->get('start'));
            }
            if($request->has('order'))
            {
                for ( $i = 0; $i < $request->get('order.0.column'); $i++ )
                {
                    $column = $sort_columns[$i];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $quiz = $quiz->orderBy($column,$request->get('order.'.$i.'.dir'));
                }
            }

            $quiz = $quiz->get();
            $response['iTotalDisplayRecords'] =$quiz_count;
            $response['iTotalRecords'] = $quiz_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $quiz->toArray();

            return $response;
        }

        return view('quiz.index');
    }

    public function create()
    {
        $users = User::lists('fullname','id');

        return view('quiz.create',compact('users'));
    }

    public function store(Request $request)
    {
        $quiz_input = $request->all();
        $options = $quiz_input['dynamic_form'];

        $rules = [
            'question' => 'required',
            'time' => 'required|numeric|min:1',
            'image' => 'image',
            'user' => 'required',
            'dynamic_form.dynamic_form.0.options' => 'required',
            'dynamic_form.dynamic_form.1.options' => 'required'
        ];

        $messages = [
            'dynamic_form.dynamic_form.0.options.required' => 'this field is required',
            'dynamic_form.dynamic_form.1.options.required' => 'this field is required'
        ];

        $a = 0;

        foreach ($quiz_input['dynamic_form']['dynamic_form'] as $key => $value) {

            if ($a = 0) {

                if ( ! isset($value['is_true'])) {

                    $rules['dynamic_form.dynamic_form.0.is_true'] = 'required';
                    $messages['dynamic_form.dynamic_form.0.is_true.required'] = 'check atleast one true answer';

                } else {
                    $a = 1;
                    unset($rules['dynamic_form.dynamic_form.0.is_true']);
                    unset($messages['dynamic_form.dynamic_form.0.is_true']);
                }
            }
        }

        $count = count($options['dynamic_form']);

        for ($i=2; $i < $count; $i++) {
            if (isset($options['dynamic_form'][$i])) {
                $rules['dynamic_form.dynamic_form.'.$i.'.options'] = 'required';
            }
        }

        for ($i=2; $i < $count; $i++) {
            $messages['dynamic_form.dynamic_form.'.$i.'.options.required'] = 'this field is required';
        }

        $validator = Validator::make($quiz_input,$rules,$messages);

        if ($validator->fails()) {
            return back()->withInput()
                           ->withErrors($validator->errors())
                           ->with('message', 'Unable to add details.')
                           ->with('message_type', 'danger');
        }

        $quiz = new Quiz();
        $quiz->question = $quiz_input['question'];
        $quiz->time = $quiz_input['time'];

        if ($request->hasFile('image')) {

            $path = public_path().'/upload/quiz';
            $file = $request->file('image');

            $filename = sha1(microtime()).'_'.$file->getClientOriginalName();

            if ( ! file_exists($path) ) {
                mkdir($path, 0777);
            }

            $file->move($path,$filename);

            $quiz->image = $filename;
        }

        if ($request->quiz_post == 'send') {
            $quiz->is_send = 1;
        }

        $quiz->save();

        foreach ($options as $option) {
            foreach ($option as $value) {
                $quizopt = new QuizOption();
                $quizopt->option = $value['options'];
                if (isset($value['is_true'])) {
                    $quizopt->is_true = 1;
                }
                $quizopt->quiz_id = $quiz->id;
                $quizopt->save();
            }
        }

        foreach ($quiz_input['user'] as $user_id) {
            $userQuizAns = new UserQuizAnswer();
            $userQuizAns->user_id = $user_id;
            $userQuizAns->quiz_id = $quiz->id;
            $userQuizAns->save();
        }

        $uuids = Uuid::whereIn('user_id',$quiz_input['user'])->get();

        if ($request->quiz_post == 'send') {
            foreach($uuids as $uuid) {

                $message = PushNotification::Message('one new question for you. click here to answer.',array(
                    'quiz_id' => $quiz->id
                ));

    			PushNotification::app('appNameAndroid')
    		                ->to($uuid->uuid)
    		                ->send($message);
    		}
        }

        return redirect()->route('quiz.index')->with('message', 'Quiz Successfully Added.')
                                              ->with('message_type', 'success');
    }

    public function edit($quiz_id)
    {
        $quiz_edit = Quiz::find($quiz_id);
        $users = User::lists('fullname','id');

        $quiz_options = QuizOption::where('quiz_id',$quiz_id)->get()->toArray();

        $final = [];
        foreach ($quiz_options as $quiz_option) {
            $hello = [];
            $key = 'options';
            $key1 = 'is_true';
            $hello[$key] = $quiz_option['option'];
            if ($quiz_option['is_true']) {
                $hello[$key1] = $quiz_option['is_true'];
            }
            $final[] = $hello;
        }
        $getSelectedUsers = [];
        $user_ids = UserQuizAnswer::lists('user_id');

        foreach ($user_ids as $key => $value) {
            $getSelectedUsers[] = $value;
        }

        return view('quiz.edit',compact('site','quiz_edit','final','users','getSelectedUsers'));
    }

    public function update(Request $request,$quiz_id)
    {
        $quiz_input = $request->all();

        $options = $quiz_input['dynamic_form'];

        $rules = [
            'question' => 'required',
            'time' => 'required',
            'image' => 'image',
            'dynamic_form.dynamic_form.0.options' => 'required',
            'dynamic_form.dynamic_form.1.options' => 'required'
        ];

        $messages = [
            'dynamic_form.dynamic_form.0.options.required' => 'this field is required',
            'dynamic_form.dynamic_form.1.options.required' => 'this field is required'
        ];

        $count = count($options['dynamic_form']);

        for ($i=2; $i < $count; $i++) {
            if (isset($options['dynamic_form'][$i])) {
                $rules['dynamic_form.dynamic_form.'.$i.'.options'] = 'required';
            }
        }

        for ($i=2; $i < $count; $i++) {
            $messages['dynamic_form.dynamic_form.'.$i.'.options.required'] = 'this field is required';
        }

        $validator = Validator::make($quiz_input,$rules,$messages);

        if ($validator->fails()) {
            return back()->withInput()
                           ->withErrors($validator->errors())
                           ->with('message', 'Unable to add details.')
                           ->with('message_type', 'danger');
        }

        $quiz = Quiz::select('image')->where('id',$quiz_id)->first();

        if ($request->hasFile('image')) {

            $path = public_path().'/upload/quiz';

            if ($quiz->image != NULL) {
                @unlink($path . '/' .$quiz->image);
            }

            $file = $request->file('image');

            $filename = sha1(microtime()).'_'.$file->getClientOriginalName();

            if ( ! file_exists($path) ) {
                mkdir($path, 0777);
            }

            $file->move($path,$filename);
        }

        if (isset($filename)) {

            $quiz_update = Quiz::where('id',$quiz_id)->update([
                'question' => $quiz_input['question'],
                'time' => $quiz_input['time'],
                'image' => $filename
            ]);

        } else {

            $quiz_update = Quiz::where('id',$quiz_id)->update([
                'question' => $quiz_input['question'],
                'time' => $quiz_input['time']
            ]);
        }

        QuizOption::where('quiz_id',$quiz_id)->delete();

        foreach ($options as $option) {
            foreach ($option as $value) {
                $quizopt = new QuizOption();
                $quizopt->option = $value['options'];
                if (isset($value['is_true'])) {
                    $quizopt->is_true = 1;
                }
                $quizopt->quiz_id = $quiz_id;
                $quizopt->save();
            }
        }

        return back()->with('message', 'Quiz Updated Successfully.')
                     ->with('message_type', 'success');

    }

    public function delete($quiz_id)
    {
        Quiz::where('id',$quiz_id)->delete();

        return back()->with('message', 'Quiz Delted Successfully.')
                     ->with('message_type', 'success');
    }

    public function send($quiz_id)
    {
        $quiz = Quiz::where('id',$quiz_id)->where('is_send',0)->first();

        if ( ! $quiz) {
            return back()->with('message','Quiz not found')
                     ->with('message_type','danger');
        }

        $user_ids = UserQuizAnswer::where('quiz_id',$quiz->id)->lists('user_id')->toArray();

        $uuids = Uuid::whereIn('user_id',$user_ids)->get();

        foreach($uuids as $uuid) {

            $message = PushNotification::Message('one new question for you. click here to answer.',array(
                'quiz_id' => $quiz->id
            ));

            PushNotification::app('appNameAndroid')
                        ->to($uuid->uuid)
                        ->send($message);
        }

        Quiz::where('id',$quiz->id)->update(['is_send' => 1]);

        return back()->with('message','Quiz sender successfully')
                     ->with('message_type','success');
    }
}
