<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HolidayYear;
use App\Models\HolidayMaster;
use App\Http\Requests;
use App\Events\HolidayYearEvent;
use Event;

class HolidayYearController extends Controller
{
    public function index(Request $request)
    {
    	if($request->ajax())
        {
            $where_str = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                  $where_str .= " and (holiday_master.title like \"%{$search}%\""
                            .")";
            }
             $columns = ['holiday_year.id','holiday_year.holiday_date','holiday_master.title as holiday_title'];
          

            $holiday_year = HolidayYear::select($columns)
                    ->leftjoin('holiday_master','holiday_master.id','=','holiday_year.holiday_id')
                    ->whereRaw($where_str,$where_params);
                  
            $holiday_year_count = HolidayYear::select($columns)
                    ->leftjoin('holiday_master','holiday_master.id','=','holiday_year.holiday_id')
                    ->whereRaw($where_str,$where_params)
                    ->count();

           

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $holiday_year = $holiday_year->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $holiday_year = $holiday_year->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $holiday_year = $holiday_year->get();
            $response['iTotalDisplayRecords'] = $holiday_year_count;
            $response['iTotalRecords'] = $holiday_year_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $holiday_year;

            return $response;
        }
        return view('holiday-year.index'); 
    }
    public function create()
    {
    	$holiday_master = HolidayMaster::all()->pluck('title','id')->toArray();
    	
    	return view('holiday-year.create',compact('holiday_master'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[

            'holiday_id'      => 'required',
            'holiday_date'      => 'required',
        ],[
            'holiday_id.required' => 'Please Select Holiday',
            'holiday_date.required' => 'Please Select Date',
        ]);

        $holiday = $request->all();
       
        Event::fire(new HolidayYearEvent($holiday));
        if($holiday['save_button'] == "save_new"){
            return redirect()->back()->with('message','Your Holiday Has Been Updated Successfully')->with('message_type','success');
        }
        return redirect()->route('holiday-year.index')
                     ->with('message','Your Holiday Has Been Updated Successfully')
                     ->with('message_type','success');
    }
    public function edit($id)
    {
    	$holiday_master = HolidayMaster::all()->pluck('title','id')->toArray();
        $holiday_year = HolidayYear::where('id',$id)->first();

        return view('holiday-year.edit',compact('holiday_year','holiday_master'));
    }
    public function update(Request $request, $id)
    {
        $holiday = $request->all();
        
        Event::fire(new HolidayYearEvent($holiday));
        if($holiday['save_button'] == "save"){
            return redirect()->back()->with('message','Your Holiday Has Been Updated Successfully')->with('message_type','success');
        }
        return redirect()->route('holiday-year.index')
                         ->with('message','Your Holiday Has Been Updated Successfully')
                         ->with('message_type','success');
    }
    public function delete(Request $request) {
      
        $id = $request->get('id');
        $holiday_year = HolidayYear::where('id', $id)->first();

        HolidayYear::where('id', $id)->delete();

        return back()->with('message', 'Record Deleted Successfully.')
            ->with('message_type', 'success');

    }
}
