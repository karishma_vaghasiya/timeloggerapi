<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Log;
use App\Models\Leave;
use App\Models\WorkingDays;
use Input,Debugbar,Auth,URL,Validator,Redirect;
use App\Utility\ImageResize;
use File,DB,PDF,Mpdf;

class ShowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function log(Request $request,$id)
    {
        if($request->ajax()){
            $where_str = "1 = ?";
            $where_param = array(1);
            
            if($request->has('search.value')){
                $search = $request->get('search');
                $search = $search['value'];
                $where_str .= " and (DATE_FORMAT(time, '%d-%m-%Y') like '%{$search}%'"
                 ." or check_in like '%{$search}%'"
                ." or check_out like '%{$search}%'"
                ." or time_diff like '%{$search}%'" .")";
            }

            if($request->has('month')){
                $month = intval( $request->get('month') );
                $where_str .= " and MONTH(`time`) = $month";
            }

            if($request->has('year')){
                $year = intval( $request->get('year') );
                $where_str .= " and YEAR(`time`) = $year";
            }
            $columns = array('id','time','check_in','check_out',DB::raw("SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) as time_diff"));

            $sort_columns = array('time','check_in','check_out','time_diff');

            $users_count = Log::select('id')
                        ->where('user_id',$id)
                        ->groupby('user_id','time')
                        ->whereRaw($where_str,$where_param)
                        ->count();

            $users = Log::select($columns)
                    ->where('user_id',$id)
                    ->groupby('user_id','time')
                   ->whereRaw($where_str,$where_param);

            if($request->has('start') && $request->get('length') !='-1'){
                $users = $users->take($request->get('length'))
                               ->skip($request->get('start'));
            }
            if($request->has('order'))
            {
                $sql_order='';
                for ( $i = 0; $i < $request->input('order.0.column'); $i++ )
                {
                    $column = $sort_columns[$i];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $users = $users->orderBy($column,$request->input('order.'.$i.'.dir'));
                }
            }

            $users = $users->get();

            $response['iTotalDisplayRecords'] = $users_count;
            $response['iTotalRecords'] = $users_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $users->toArray();

            return $response;
        }
        $name = User::select('fullname')->where('id',$id)->first()->toArray();
        return view('show.logs',compact('id','name'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function leave(Request $request,$id)
    {
        if($request->ajax()){

            $where_str = "1=?";

            $where_param =array(1);

            if($request->has('search.value')){

                $search = $request->get('search.value');

                $where_str .= " and DATE_FORMAT(date, '%d-%m-%Y') like \"%{$search}%\""
                            ." or leave_type like \"%{$search}%\""
                            ." or leave_purpose like \"%{$search}%\""
                            .")";
            }
            $columns = array('id', 'date','leave_type','leave_time','leave_purpose','from_hour','to_hour','status');
            $leaves_count = Leave::select('id')
                        ->where('user_id',$id)
                        ->whereRaw($where_str,$where_param)
                        ->count();

            $leaves = Leave::select($columns)
                        ->where('user_id',$id)
                        ->whereRaw($where_str,$where_param);

            if($request->has('start') && $request->get('length') !='-1'){
            $leaves = $leaves->take($request->get('length'))
                                    ->skip($request->get('start'));
            }

            if($request->has('order')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('order.0.column'); $i++ )
                {
                    $column = $columns[$i];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $leaves = $leaves->orderBy($column,$request->input('order.'.$i.'.dir'));
                }
            }

            $leaves = $leaves->get();

            $response['iTotalDisplayRecords'] =$leaves_count;
            $response['iTotalRecords'] = $leaves_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $leaves->toArray();

            return $response;
        }
        $username = User::select('fullname')->where('id',$id)->first()->toArray();

        return view('show.leaves',compact('id','username'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile(){

        return view('show.user');
    }

    public function edit(Request $request){
        $user = User::find(Auth::id());

        $profile = User::find(Auth::id());

        return view('show.edit',compact('user','profile'));
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::id());

        $this->validate($request,[

            'fullname' => 'required|regex:/^[a-zA-Z .]*$/',
            'position'      => 'required',
            'contact'       => 'required|min:10|max:11',
        ]);
        $user->fill($request->all());

        $user->save();

        return back()->with('message','Your Profile Has Been Updated Successfully')
                     ->with('message_type','success');
    }

    public function getUploadProfilePhoto()
    {
        return view('profile.upload-photo');
    }

    public function postUploadProfilePhoto(Request $request)
    {
        $profile = User::find(Auth::id());

        $rules = ['profile_pic' => 'required|mimes:jpeg,jpg,png|image|max:4096'];

        $validator = Validator::make($request->all(), $rules);

        if ( $validator->passes() )
        {
            $microtime = microtime();
            $search = array('.',' ');
            $microtime = str_replace($search, "_", $microtime);
            $photo = $microtime . '.' . $request->file('profile_pic')->getClientOriginalExtension();

            $path = public_path()."/upload/";

            $old_file = $path . $profile->photo;

            list($width, $height, $type, $attr) = getimagesize($request->file('profile_pic'));

            $save_w = $width;
            $save_h = $height;

            $request->file('profile_pic')->move($path, $photo);

            if ( $save_w >= 500 || $save_h >= 500 )
            {
                $resizeObj = new ImageResize($path . $photo);
                $resizeObj->resizeImage(300, 200, 0);
                $resizeObj->saveImage($path . $photo, 100);
            }
            $profile->temp_photo = $photo;

            $profile->save();

            return redirect()->route('profile.photo.crop.get');
        }
        else
        {
            return back()->withErrors($validator);
        }
    }

    public function getCropProfilePhoto(Request $request)
    {
        $profile = User::find(Auth::id());

        return view('profile.crop-photo',compact('profile'));
    }

    public function postCropProfilePhoto(Request $request) {

        $profile = User::find(Auth::id());

        $photo = $profile->temp_photo;

        $old_photo = $profile->profile_pic;

        $public_path = str_replace("\\","/", public_path());

        $save_folder =  $public_path . '/upload/';
        $photo_file = $save_folder . '/' . $photo;

        if ($old_photo != NULL AND $old_photo != '') {
            $old_photo_path = $save_folder . '/' . $old_photo;

            @unlink($old_photo_path);
        }

        list($width, $height, $type, $attr) = getimagesize($photo_file);

        $save_w = $width;
        $save_h = $height;

        $crop_x = $request->get('x');
        $crop_y = $request->get('y');
        $crop_w = $request->get('w');
        $crop_h = $request->get('h');

        $resizeObj = new ImageResize($photo_file);
        $resizeObj->cropCustomized($crop_x, $crop_y,$crop_w, $crop_h, $crop_w, $crop_h);
        $outputfile = $save_folder . $photo;

        $resizeObj->saveImage($outputfile, 100);

        $profile->profile_pic = $photo;
        $profile->save();

        return Redirect::route('show.edit')->with('message','Your Profile Photo Updated Successfully')
                                           ->with('message_type','success');
    }
    public function exportlogs($id,$month,$year){
        $list_pdf = DB::table("logs")
            ->selectRaw("*,TIME_FORMAT(MAX(check_out),'%h:%i:%s') as check_out,TIME_FORMAT(MIN(check_in),'%h:%i:%s') as check_in,DATE_FORMAT(time, '%d-%m-%Y') as time,SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) as time_diff,SUM(TIME_TO_SEC(time_diff)) as time_diff_sec")
            ->whereRaw('MONTH(time) = ?',[$month])
            ->whereRaw('YEAR(time) = ?',[$year])
            ->where('user_id',$id)
            ->groupby('time')
            ->leftjoin('users','users.id','=','logs.user_id')
            ->get();
        $array_data = json_decode(json_encode($list_pdf),true);

        $improper_count = 0;
        $works_hours =[];
        foreach ($array_data as $key => $value) {
            $works_hours[] = $value['time_diff_sec'];
            if(($value['time_diff']) < '09:00:00'){
                $improper_count++;
            }
        }
        $dateObj   = \DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F'); 
        $works_hours = array_sum($works_hours);
        $w_h = intval($works_hours/3600);
        $r= $works_hours%3600;
        $w_m = intval($r/60);
        
        $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")
                                ->whereRaw('MONTH(working_date) = ?',[$month])
                                ->whereRaw('YEAR(working_date) = ?',[$year])
                                ->get()
                                ->toArray();
                                                      
        $mpdf = new \mPDF('c', 'A4','','','15','15','25','18');
        $mpdf -> SetFont('eurostyle');
        $header= '<table width="100%"><tr>  
            <td width="80%">'.ucwords($array_data[0]['fullname']).' - '.$monthName .' '.$year.' '.'Summary</td>
            <td width="20%"><h1>thinkTANKER</h1></td>
            </tr></table>';
        $mpdf->SetHTMLHeader($header);
        $mpdf->setFooter("Page {PAGENO} of {nb}");
        $mpdf->autoPageBreak = true;
        $mpdf->WriteHTML(\View::make('show.exportlogs',['pdf_data'=>$array_data,'improper_count'=>$improper_count,'works_hours'=>$w_h.':'.$w_m,'month_name'=>$monthName,'year'=>$year,'total_working_hours'=>$total_working_hours])->render());
        $filename = 'pdfview.pdf';
        $filepath = public_path() ."/uploads/" . $filename;
        $mpdf->output($filepath);

        return response()->download($filepath);
    }
    public function allUserExportLogs($id,$month,$year){
        $list_pdf = DB::table("logs")
            ->selectRaw("user_id,TIME_FORMAT(MAX(check_out),'%h:%i:%s') as check_out,TIME_FORMAT(MIN(check_in),'%h:%i:%s') as check_in,SEC_TO_TIME( SUM(TIME_TO_SEC(time_diff))) AS time_diff,time AS Date,users.fullname")
            ->leftjoin('users','users.id','=','logs.user_id')
            ->whereRaw('MONTH(time) = ?',[$month])
            ->whereRaw('YEAR(time) = ?',[$year])
            ->groupby('time','user_id')
            ->get();

        $array_data = json_decode(json_encode($list_pdf),true);
        //==========
        foreach ($array_data as $key => $value) {
            $unique_date[] = $value['Date'];
            $unique_name[] = $value['fullname'];
        }

        $data = [];
        $unique_date = array_unique($unique_date);
        $unique_name = array_unique($unique_name);
        $index = 0;
        foreach ($unique_date as $date_key => $date_value) {
            foreach ($unique_name as $name_key => $name_value) {
                $data[$index]['date'] = $date_value;
                $data[$index]['name'] = $name_value;
                $index++;
            }
        }

        $logs_data = [];
        $logs_data1 = [];
        $count = 0;
        $flag = true;
        foreach ($data as $data_key => $data_value) {
            foreach($array_data as $single_key => $single_value) {
                if(($data_value['date'] == $single_value['Date']) && ($data_value['name'] == $single_value['fullname'])){
                    $logs_data[$count]['user_id'] = $single_value['user_id'];
                    $logs_data[$count]['check_out'] = $single_value['check_out'];
                    $logs_data[$count]['check_in'] = $single_value['check_in'];
                    $logs_data[$count]['time_diff'] = $single_value['time_diff'];
                    $logs_data[$count]['Date'] = $single_value['Date'];
                    $logs_data[$count]['fullname'] = $single_value['fullname'];
                }else{
                    $flag = false;
                }
            }
            if($flag == false){
                $logs_data1[$count]['user_id'] = 1;
                $logs_data1[$count]['check_out'] = "";
                $logs_data1[$count]['check_in'] = "";
                $logs_data1[$count]['time_diff'] = "";
                $logs_data1[$count]['Date'] = $data_value['date'];
                $logs_data1[$count]['fullname'] = $data_value['name'];
            }
                $count++;
        }

        $main_pdf_arr = array_replace_recursive($logs_data1, $logs_data);
        
        $total_working_hours =  WorkingDays::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(working_hours))) as working_hours")
                                ->whereRaw('MONTH(working_date) = ?',[$month])
                                ->whereRaw('YEAR(working_date) = ?',[$year])
                                 ->get()
                                 ->toArray();
        $exp_total_hours = explode(':',$total_working_hours[0]['working_hours']);                         
        $work_hours = Log::selectRaw("SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as work_hour,user_id")
                            ->whereRaw('MONTH(time) = ?',[$month])
                            ->whereRaw('YEAR(time) = ?',[$year])
                            ->groupby('user_id')
                            ->get()
                            ->toArray();

        $improper_count = 0;
        foreach ($array_data as $key => $value) {
            if(($value['time_diff']) < '09:00:00'){
                $improper_count++;
            }
        }
        $dateObj   = \DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F'); 

        $mpdf = new \mPDF('c', 'A4-L','','','15','15','25','18');
        $mpdf -> SetFont('eurostyle');
        $header= '<table width="100%"><tr>  
            <td width="80%">'.'<h4>Employee Logs'." ".$monthName." ".$year.'</h4></td>
            <td width="20%"><h1>thinkTANKER</h1></td>
            </tr></table>';
        $mpdf->SetHTMLHeader($header);
        $mpdf->setFooter("Page {PAGENO} of {nb}");
        $mpdf->autoPageBreak = true;
        $mpdf->WriteHTML(\View::make('show.all-user-exportlogs',['pdf_data'=>$main_pdf_arr,'improper_count'=>$improper_count,'month_name'=>$monthName,'year'=>$year,'work_hours'=>$work_hours,'exp_total_hours'=>$exp_total_hours])->render());
        $filename = 'pdfview.pdf';
        $filepath = public_path() ."/uploads/" . $filename;
        $mpdf->output($filepath);
        
        return response()->download($filepath);
        
    }
}
