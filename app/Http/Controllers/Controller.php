<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function createMessageResponse($success, $message, $code)
    {
    	return response()->json(['success' => $success, 'message' => $message],$code);
    }

    public function createDataResponse($success, $message, $data, $code)
    {
        if (is_array($data)) {
            if (isset($data['access_token'])) {
                unset($data['access_token']);
            }
        }
    	return response()->json(['success' => $success,'message' => $message,'data' => $data],$code);
    }

    public function createOnlyDataResponse($success, $data, $code)
    {
    	if (isset($data['access_token'])) {
    		unset($data['access_token']);
    	}

    	return response()->json(['success' => $success,'data' => $data],$code);
    }
}
