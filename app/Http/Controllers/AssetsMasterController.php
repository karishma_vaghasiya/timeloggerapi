<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Assets;
use App\Models\AssetsMaster;
use App\Models\User;
use App\Events\AssetsMasterEvent;
use App\Http\Requests;
use Event;

class AssetsMasterController extends Controller
{
   public function index(Request $request)
   {
   		if($request->ajax()){

            $where_str = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_str .= " and (assets.title like \"%{$search}%\""
                            ." or users.fullname like \"%{$search}%\""
                            ." or DATE_FORMAT(assets_master.start_date, '%d-%m-%Y') like \"%{$search}%\""
                             .")";           
            }
            $columns = ['assets_master.id','assets.main_title as title','users.fullname as fullname','assets_master.start_date as startdate'];

             $laptop_count = AssetsMaster::select($columns)
                        ->leftjoin('assets','assets.id','=','assets_master.assets_id')
                        ->leftjoin('users','users.id','=','assets_master.user_id')
                        ->whereRaw($where_str,$where_params)
                        ->count();
            $laptops = AssetsMaster::select($columns)
                       ->leftjoin('assets','assets.id','=','assets_master.assets_id')
                       ->leftjoin('users','users.id','=','assets_master.user_id')
                       ->whereRaw($where_str,$where_params);


           

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $laptops = $laptops->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $laptops = $laptops->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $laptops = $laptops->get();
            $response['iTotalDisplayRecords'] = $laptop_count;
            $response['iTotalRecords'] = $laptop_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $laptops;

            return $response;
        }

        return view('assetsMaster.index');
   }
   public function create()
   {
   		$user = User::all()->pluck('fullname','id')->toArray();
   		$assets = Assets::all()->pluck('main_title','id')->toArray();
   		
   		return view('assetsMaster.create',compact('user','assets'));
   }
   public function store(Request $request)
    {
        $this->validate($request,[

            'assets_id' => 'required',
            'user_id' => 'required',  
            'start_date' => 'required',  
        ],[
            'assets_id.required' => 'Please Select Assets',
            'user_id.required' => 'Please Select User',
            'start_date.required' => 'Please Start Date',
        ]);
    	$assets_master = $request->all();
    	Event::fire(new AssetsMasterEvent($assets_master));
    	if($assets_master['save_button'] == "save_new"){
            return redirect()->back()->with('message','Record Added Successfully')->with('message_type','success');
        }
        return redirect()->route('assets.master.index')
                         ->with('message','Record Added Successfully.')
                         ->with('message_type','success'); 
    }
    public function edit($id)
    {
        $assets_master = AssetsMaster::where('id',$id)->first();
        $user = User::all()->pluck('fullname','id')->toArray();
   		$assets = Assets::all()->pluck('main_title','id')->toArray();
        
        return view('assetsMaster.edit',compact('assets_master','user','assets'));
    }
    public function update(Request $request)
    {
        $this->validate($request,[

            'assets_id' => 'required',
            'user_id' => 'required',  
            'start_date' => 'required',  
        ],[
            'assets_id.required' => 'Please Select Assets',
            'user_id.required' => 'Please Select User',
            'start_date.required' => 'Please Start Date',
        ]);
        $assets_master = $request->all();

        Event::fire(new AssetsMasterEvent($assets_master));
        if($assets_master['save_button'] == "save"){
            return redirect()->back()
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');    
        }
        return redirect()->route('assets.master.index')
                         ->with('message','Record Updated successfully')
                         ->with('message_type','success');
    }
    public function delete(Request $request)
    {
        $id = $request->get('id');
        $assets_master = AssetsMaster::where('id', $id)->first();
        AssetsMaster::where('id', $id)->delete();

        return back()->with('message', 'Record Deleted Successfully.')
            ->with('message_type', 'success');
    }
}
