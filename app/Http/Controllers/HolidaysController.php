<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Holiday;
use Input,Auth,Debugbar;

class HolidaysController extends Controller{

	public function index(Request $request){

		if($request->ajax()){
            $where_str = "1 =?";
            $where_param =array('1');

             if($request->has('search.value')){

                $search = $request->get('search.value');

                $where_str .= " and (day like \"%{$search}%\""
							." or DATE_FORMAT(date, '%d-%m-%Y') like \"%{$search}%\""
                            ." or holiday like \"%{$search}%\""
                            .")";
            }
            $columns = array('id','day', 'date','holiday');
            $order_columns = ['day', 'date','holiday'];
            $holidays_count = Holiday::select('id')
                        ->whereRaw($where_str,$where_param)
                        ->count();
            //Debugbar::info($holidays_count);

            $holidays = Holiday::select($columns)
                    ->whereRaw($where_str,$where_param);

            if($request->has('start') && $request->get('length') !='-1'){
            $holidays = $holidays->take($request->get('length'))
                                    ->skip($request->get('start'));
            }

            if($request->has('order')){
            $sql_order='';
            for ( $i = 0; $i <= $request->input('order.0.column'); $i++ )
            {
                $column = $order_columns[$i];
                if(false !== ($index = strpos($column, ' as '))){
                    $column = substr($column, 0, $index);
                }
                $holidays = $holidays->orderBy($column,$request->input('order.'.$i.'.dir'));
            }
        }

            $holidays = $holidays->get();
            $response['iTotalDisplayRecords'] =$holidays_count;
            $response['iTotalRecords'] = $holidays_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $holidays->toArray();

            return $response;
        }

        return view('holiday.index');
    }

    public function create(){
    	return view('holiday.create');
	}

	public function store(Request $request){
		$data = $request->all();

        $this->validate($request,[
            'day' => 'required',
            'date' => 'required',
            'holiday' => 'required'
        ]);

		$holiday = new Holiday($data);
		$holiday->save();

		return back()->with('message','Congratulations!! Your Holiday Created Successfully')
                     ->with('message_type','success');

	}
	public function edit($id){

		$holidays = Holiday::where('id',$id)->first();

        $holidays['date'] = date("Y-m-d", strtotime($holidays['date']));

		return view('holiday.edit',compact('holidays'));
	}
	public function update(Request $request ,$id){

        $this->validate($request,[
            'day' => 'required',
            'date' => 'required',
            'holiday' => 'required'
        ]);

		$holiday = Holiday::find($id);
		$holiday->fill($request->all());
		$holiday->save();

        return back()->with('message_type','success')
                     ->with('message','Holiday Updated Successfully');

	}
    public function destroy($id)
    {
        $holidays = Holiday::find($id);
        $holidays->delete();

        return back()->with('message','holiday deleted successfully')
                     ->with('message_type','success');
    }
}
