<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Log;
use Input,DB,Auth;

class LogController extends Controller {

    public function index(Request $request) {

        if($request->ajax()){

            $where_str = "1 = ?";
            $where_param = array(1);

            if($request->has('search.value')) {
                $search = $request->get('search.value');

                 $where_str .= " and DATE_FORMAT(time, '%d-%m-%Y') like '%{$search}%'";

            }
            if(auth()->user()->is_admin){
                if($request->has('date')){
                    $date = $request->get('date');
                    $where_str .= " and time = '$date'";
                }
                $columns = array('logs.id','logs.time','logs.check_in','logs.check_out','logs.time_diff','users.fullname as fullname');


                $users_counts = Log::select('logs.id')
                            ->leftjoin('users', 'users.id', '=', 'logs.user_id')
                            ->groupby(DB::raw('logs.user_id'))
                            ->whereRaw($where_str,$where_param)->get();
                $users_count = count($users_counts); 
                $users = Log::selectRaw("logs.id,TIME_FORMAT(MAX(check_out), '%h:%i:%s')  AS check_out,TIME_FORMAT(MIN(check_in), '%h:%i:%s') AS check_in,users.fullname as fullname,logs.time,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) AS time_diff")
                       /* ->orderBy('time', 'DESC')*/
                        ->groupby(DB::raw('logs.user_id'))
                        ->leftjoin('users', 'users.id', '=', 'logs.user_id')
                        ->whereRaw($where_str,$where_param);
            }else{
                if($request->has('month')){
                $month = intval( $request->get('month') );
                $where_str .= " and MONTH(`time`) = $month";
                }

                if($request->has('year')){
                $year = intval( $request->get('year') );
                    $where_str .= " and YEAR(`time`) = $year";

                }
                
                $columns = array('logs.id','logs.time','logs.check_in','logs.check_out','logs.time_diff');
                $users_count_c = Log::select('logs.id')
                            ->where('user_id',Auth::id())
                            ->groupby(DB::raw('logs.time'))
                            ->whereRaw($where_str,$where_param)->get();
                $users_count = count($users_count_c);
                $users = Log::selectRaw("logs.id,TIME_FORMAT(MAX(check_out),'%h:%i:%s') as check_out,TIME_FORMAT(MIN(check_in),'%h:%i:%s') as check_in,logs.time,SEC_TO_TIME(SUM(TIME_TO_SEC(time_diff))) as time_diff")
                        ->where('user_id',Auth::id())
                        ->groupby(DB::raw('logs.time'))
                        ->whereRaw($where_str,$where_param);                       
            }

               
            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $users = $users->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $users = $users->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            
            $users = $users->get();
            $response['iTotalDisplayRecords'] = $users_count;
            $response['iTotalRecords'] = $users_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $users;

            return $response;
        }

    	return view('logs.index');
    }

    public function userCreate($userId)
    {
        return view('logs.user-create',compact('userId'));
    }

    public function userStore(Request $request, $userId)
    {
        $data = $request->all();

        $rules = [
            'date' => 'required',
            'check_in' => 'required',
            'check_out' => 'required'
        ];

        $this->validate($request,$rules);
    }
}
