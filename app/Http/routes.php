<?php
// use Hash;
Route::get('/hash/{str}', function($str) {
 	return Hash::make($str);
});

Route::get('testmail',function(){
	Mail::send('emails.test',['hello'],function($message){
		$message->from('noreply@thinktanker.in');
		$message->to('utsav@yopmail.com')->subject('nothing');
	});
});

Route::group(['middleware' => 'api'], function() {

	Route::get('api/v1.1/test1',function(){
		$push = PushNotification::app('appNameAndroid')
											->to('f1xKR2wimTk:APA91bEaj07KcoQFKTtnUesRvefwIYrhVROj00N1rM2UI4x597X-9RcloCV9vs__9i_TK992t2lg9Qb0i2ud-t8wSj5dOmlWOCGsJ_dXoh-FqSitupYhs9G_JxP7OmYBzpHhOLDR7j8g')
											->send('hello there');
		// dd($push);
	});

	Route::post('api/v1.1/oauth/access_token', function() {
		return response()->json(Authorizer::issueAccessToken());
	});


	Route::group(['namespace' => 'api'],function(){

		Route::get('register/verify/{confirm_code}','UsersController@confirm');

		Route::get('password/reset/{token}', 'UserPasswordController@getReset');
		// Route::get('/hash/{str}', function($str) {
		// 	return Hash::make($str);
		// });

		Route::post('amts/email','ATSInstantIdController@confirmEmail');
		Route::post('password/reset', 'UserPasswordController@postReset');
		Route::post('database-insert','UserPasswordController@DatabaseInsert');
		Route::post('location-get','UserPasswordController@LocationGet');
		Route::post('location-current','UserPasswordController@LocationCurrent');
		Route::post('location-latest','UserPasswordController@LatestLocation');
		Route::post('location-push','UserPasswordController@LocationPush');
		Route::post('configuration-push','UserPasswordController@ConfigurationPush');
		Route::get('configuration-get','UserPasswordController@ConfigurationGet');
		Route::post('edit-user','UserPasswordController@EditUSer');
		
	});

	Route::group(['prefix' => 'api/v1.1','middleware' => 'oauth', 'namespace' => 'api'] ,function(){

		Route::post('dashboard','LeaveController@dashboard');
		Route::post('register','UsersController@store');
		Route::post('user-update','UsersController@update');
		Route::get('user-list','UsersController@userList');

		Route::post('login','UsersController@login');
		Route::post('profile','UsersController@showProfile');
		Route::post('logout','UsersController@logout');

		Route::get('admin-data','AdminDashboardController@getData');
		Route::post('user-data','AdminDashboardController@postUserData');
		Route::post('admin-data/logs','AdminDashboardController@getLogData');

		Route::post('password/email','UserPasswordController@postEmail');

		Route::post('updates','UpdatesController@getUpdates');
		Route::post('updates/create','UpdatesController@create');

		Route::post('log','UserLogController@create');

		Route::post('getlog','UserLogController@getLog');

		Route::post('leave','LeaveController@create');
		Route::post('admin-leave','LeaveController@adminLeave');
		Route::post('leave-status','LeaveController@status');
		Route::post('leave-delete','LeaveController@LeaveDelete');

		Route::post('leave-report','LeaveController@getLeave');

		Route::get('holidays','HolidayController@index');

		Route::post('change-password','ChangePasswordController@index');

		Route::post('image-upload','ImageUploadController@index');

		Route::post('set-reminder','UserLogController@setReminder');
		Route::post('new-reminder','UserLogController@newReminder');
		Route::post('userlog','UserLogController@userLog');
		Route::post('settings','UserLogController@setting');

		Route::post('quiz','QuizController@getQuiz');
		Route::post('store-quiz-answer','QuizController@storeQuizAnswer');
		Route::post('quiz-list','QuizController@quizList');
		Route::post('quiz-leaderboard','QuizController@getLeaderBoard');
		Route::post('quiz-activityboard','QuizController@getActivityBoard');

		Route::get('user-birthday','UsersController@userBirthday');
		#---------------------------Routes For ATS----------------------
		Route::post('ats-instantid','ATSInstantIdController@store');
		Route::post('send-notification','ATSInstantIdController@sendMessage');

	});
});

Route::group(['middleware' => 'web'], function() {

	Route::get('login',[
		'as' => 'login',
		'uses' => 'Auth\AuthController@getLogin'
	]);

	Route::post('login',[
		'as' => 'user.login',
		'uses' => 'Auth\AuthController@postLogin'
	]);

	Route::get('logout','Auth\AuthController@getLogout');

	Route::get('/', function () {

		if( Auth::check() ) {
            return redirect()->route('dashboard.admin');
        }

		return view('auth.login');
	});

	//Route::get('verify/{code}', 'Auth\AuthController@accountIsActive');

	Route::post('/trash',[
		'as' => 'user.trash',
		'uses' => 'TimeloggerController@trash'
	]);
	Route::get('recyclebin',[
		'as' => 'user.recyclebin',
		'uses' => 'TimeloggerController@recyclebin'
	]);

	Route::get('restore/{id}',[
		'as' => 'user.restore',
		'uses' => 'TimeloggerController@restore'
	]);
	Route::get('user/delete/{id}',[
		'as'=> 'user.destroy',
		'uses'=>'TimeloggerController@destroy'
	]);

	Route::get('password/email','Auth\PasswordController@getEmail');
	Route::post('password/email','Auth\PasswordController@postEmail');
	Route::get('password/reset/{token}','Auth\PasswordController@getReset');
	Route::post('password/reset','Auth\PasswordController@postReset');

	Route::get('denied',[
		'as' => 'user.denied',
		'uses' => function(){
			return 'denied';
		}
	]);

	/*User authinticate*/
	Route::group(array('middleware' => ['auth']), function(){

			Route::get('dashboard',[
					'as'=>'dashboard.admin',
					'uses'=>'TimeloggerController@admin'
			]);

			Route::post('get-selected-pemission',array(
					'as' => 'selectpermission.get',
					'uses' => 'UserPermissionController@getSelectedPermissions'
			));
		//karishma	
		Route::post('leaves/delete',[
				'as'   => 'leaves.delete',
				'uses' => 'LeavesController@destroy'
			]);
		Route::post('leave/status','LeavesController@leaveStatus')->name('leave.status');
		Route::post('notification/isread','LeavesController@notificationIsread')->name('notification.isread');
		Route::post('notification/isview','LeavesController@notificationIsview')->name('notification.isview');
		Route::get('logs/exportlogs/{id}/{month}/{year}',array(
					'as' => 'logs.exportlogs',
					'uses' => 'ShowController@exportlogs'
				));
		Route::get('logs/alluserexportlogs/{id}/{month}/{year}',array(
					'as' => 'logs.alluserexportlogs',
					'uses' => 'ShowController@allUserExportLogs'
				));
		Route::get('workingdays',array(
					'as' => 'admin.workingdays',
					'uses' => 'WorkingDaysController@index'
				));
		Route::post('workingdays',array(
					'as' => 'admin.workingdays',
					'uses' => 'WorkingDaysController@workingdays'
				));
		Route::post('store/workingdays',array(
					'as' => 'admin.store.workingdays',
					'uses' => 'WorkingDaysController@storeworkingdays'
				));
		Route::get('user/profile/upload-photo/{id?}', array(
			'as'	=> 'user.profile.upload.get',
			'uses'	=> 'TimeloggerController@getUploadProfilePhoto'
		));

		Route::post('user/profile/upload-photo', array(
			'as'	=> 'user.profile.upload.post',
			'uses'	=> 'TimeloggerController@postUploadProfilePhoto'
		));

		Route::get('/user/profile/crop-photo', array(
			'as'	=> 'user.profile.photo.crop.get',
			'uses'  => 'TimeloggerController@getCropProfilePhoto'
		));

		Route::post('/user/profile/crop-photo', array(
			'as'	=> 'user.profile.photo.crop.post',
			'uses'  => 'TimeloggerController@postCropProfilePhoto'
		));
		Route::post('/user/deactive', array(
			'as'	=> 'user.deactive',
			'uses'  => 'TimeloggerController@userDeactive'
		));
		Route::post('/leaves/chart', array(
			'as'	=> 'dashboard.leaves.chart',
			'uses'  => 'TimeloggerController@getChart'
		));
		Route::post('/leaves/secondchart', array(
			'as'	=> 'dashboard.leaves.secondchart',
			'uses'  => 'TimeloggerController@getSecondChart'
		));
		Route::post('workingdays/hours',array(
			'as' => 'admin.workingdays.hours',
			'uses' => 'WorkingDaysController@workingdaysHours'
		));
		Route::resource('services','ServicesController',['except'=>['delete','show']]);
		Route::post('services/delete','ServicesController@delete')->name('services.delete');
		Route::post('category/store','ServicesController@categoryStore')->name('category.store');
		Route::get('weeklylogs',array(
			'as' => 'admin.weeklylogs',
			'uses' => 'WeeklyLogsController@weeklyLogs'
		));

		
		//end karishma
		//rushik
		Route::resource('/holiday-master','HolidaysMasterController',['except'=>['delete','show']]);
		Route::post('/holiday-master/delete','HolidaysMasterController@delete')->name('holiday-master.delete');
		Route::resource('/holiday-year','HolidayYearController',['except'=>['delete','show']]);
		Route::post('/holiday-year/delete','HolidayYearController@delete')->name('holiday-year.delete');
		Route::get('holiday/upload-photo', array(
					'as'	=> 'holiday.upload.get',
					'uses'	=> 'HolidaysMasterController@getUploadProfilePhoto'
				));
		Route::post('holiday/upload-photo', array(
					'as'	=> 'holiday.upload.post',
					'uses'	=> 'HolidaysMasterController@postUploadProfilePhoto'
				));
		Route::get('/holiday/crop-photo', array(
					'as'	=> 'holiday.photo.crop.get',
					'uses'  => 'HolidaysMasterController@getCropProfilePhoto'
				));
		Route::post('/holiday/crop-photo', array(
					'as'	=> 'holiday.photo.crop.post',
					'uses'  => 'HolidaysMasterController@postCropProfilePhoto'
				));
		Route::post('/dashboard/leaves/store', array(
				'as'	=> 'dashboard.leaves.store',
				'uses'  => 'LeavesController@leavestore'
			));
		Route::post('/dashboard/leaves/second/store', array(
				'as'	=> 'dashboard.leaves.second.store',
				'uses'  => 'LeavesController@leavesecondstore'
			));
		Route::post('/dashboard/leaves/third/store', array(
				'as'	=> 'dashboard.leaves.third.store',
				'uses'  => 'LeavesController@leavethirdstore'
			));
		Route::post('/user/checkin', array(
				'as'	=> 'user.checkin',
				'uses'  => 'TimeloggerController@checkIn'
			));
		Route::resource('assets','AssetsController',['except'=>['delete','show']]);
		Route::post('assets/delete','AssetsController@delete')->name('assets.delete');
		Route::resource('assets/master','AssetsMasterController',['except'=>['delete','show']]);
		Route::post('assets/master/delete','AssetsMasterController@delete')->name('assets.master.delete');			
		//end rushik
		Route::group(array('middleware'=> ['acl']),function(){
				

				Route::resource('/user','TimeloggerController',['except'=>['destroy']]);
				Route::resource('/leaves','LeavesController',['except'=>['show']]);
				Route::resource('/holiday','HolidaysController',['except'=>['destroy']]);
				

				Route::get('holiday/delete/{id}',[
					'as'=> 'holidays.delete',
					'uses'=>'HolidaysController@destroy'
				]);

				Route::get('leaves/{user_id}/create',array(
					'as' => 'user.leaves.create',
					'uses' => 'LeavesController@userCreate'
				));

				Route::get('leaves/{user_id}/store',array(
					'as' => 'user.leaves.store',
					'uses' => 'LeavesController@userStore'
				));

				Route::get('quiz-delete/{quiz_id}',array(
					'as' => 'quiz.delete',
					'uses' => 'QuizController@delete'
				));

				Route::get('quiz-send/{quiz_id}',array(
					'as' => 'quiz.send',
					'uses' => 'QuizController@send'
				));

				Route::resource('quiz','QuizController',['except'=>['destroy','show']]);
				
				
				Route::get('logs',[
					'as' => 'logs.index',
					'uses' => 'LogController@index'
				]);
				Route::resource('userpermission','UserPermissionController');

				Route::get('profile',[
					'as' =>'show.user',
					'uses' =>'ShowController@profile'
				]);

				Route::get('logs/{id}',[
					'as'=>'show.logs',
					'uses'=>'ShowController@log'
				]);

				Route::get('leaves/{id}',[
					'as'=>'show.leaves',
					'uses'=>'ShowController@leave'
				]);

				Route::get('Leave/{id}/approve',[
					'as' => 'leaves.approve',
					'uses' => 'LeavesController@approveLeave'
				]);

				Route::post('Leave/{id}/Approve',[
					'as'  => 'sendmail.approve',
					'uses'  =>'LeavesController@approvemail']);

				Route::post('Leave/{id}/Reject',[
					'as'  => 'sendmail.reject',
					'uses'  =>'LeavesController@rejectemail'
				]);

				// Route::post('leaves/delete',[
				// 	'as'   => 'leaves.destroy',
				// 	'uses' => 'LeavesController@destroy'
				// ]);

				Route::get('profile/upload-photo', array(
					'as'	=> 'profile.upload.get',
					'uses'	=> 'ShowController@getUploadProfilePhoto'
				));

				Route::post('profile/upload-photo', array(
					'as'	=> 'profile.upload.post',
					'uses'	=> 'ShowController@postUploadProfilePhoto'
				));

				Route::get('/profile/crop-photo', array(
					'as'	=> 'profile.photo.crop.get',
					'uses'  => 'ShowController@getCropProfilePhoto'
				));

				Route::post('/profile/crop-photo', array(
					'as'	=> 'profile.photo.crop.post',
					'uses'  => 'ShowController@postCropProfilePhoto'
				));
				Route::get('profile/edit',[
					'as'=>'show.edit',
					'uses'=>'ShowController@edit'
				]);
				Route::post('update',[
					'as'=>'show.update',
					'uses'=>'ShowController@update'
				]);
			});
	});

	Route::controllers([
			'password' => 'Auth\PasswordController',
	]);
});
